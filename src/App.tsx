import * as React from "react";
import { observer } from "mobx-react";
import { BrowserRouter, Route } from "react-router-dom";
// import UserStore from "./stores/UserStore";
import ConfigureRoutes from "./ConfigureRoutes";
import { SaliErrorBoundary, SaliSearchNavi, SaliAppBanner, SaliMainNavi, SaliFooter } from "./common/ui";
import SaliNavigation from "./common/ui/SaliNavigation";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

interface IAppState {
    finishedAuthenticating: boolean;
    networkError: boolean;
}

@observer
class App extends React.Component<{}, IAppState> {
    constructor(props: {}) {
        super(props);
        this.state = {
            finishedAuthenticating: true,
            networkError: false
        };
    }
    
    componentWillMount() {
        // UserStore
        //     .getCurrentUser()
        //     .then(() => this.setState({ finishedAuthenticating: true }))
        //     .catch(error => {
        //         Logger.error(error);
        //         this.setState({ finishedAuthenticating: true, networkError: true });
        //     });
        // this.setState({ finishedAuthenticating: true, networkError: true });
    }

    render() {
        if (!this.state.finishedAuthenticating) {
            return <div />;
        }

        // Had to add this wrapper to address MobX issue and react router
        // blocking. See: https://github.com/ReactTraining/react-router/blob/master/packages/react-router/docs/guides/blocked-updates.md
        // Issue becomes apparent when nested routes are added as children to
        // <SaliMainNavi /> withRouter expects props=> RouteComponentProps<any>
        const SailMainNaviWithRouter = () => (
            <Route
                render={routeProps =>
                    <SaliMainNavi {...routeProps} menuconfig={SaliNavigation.NAVIGATIONAL_LAYOUT_LINKS}>
                        <div id="Main">
                            <ToastContainer />
                            <SaliErrorBoundary hasError={this.state.networkError}>
                                <ConfigureRoutes />
                            </SaliErrorBoundary>
                        </div>
                    </SaliMainNavi>
                }
            />
        );

        return (
            <BrowserRouter>
                <div id="Sali-App" className="App">
                    <div id="Sali-Content" >
                        <a href="#main-content" className="visually-hidden visible-when-focused bypass-block-link">Skip Navigation</a>
                        <SaliSearchNavi />
                        <SaliAppBanner {...SaliNavigation.APP_BANNER_BRANDING} />
                        <SailMainNaviWithRouter />
                    </div>
                    <SaliFooter />
                </div>
            </BrowserRouter>
        );
    }
}

export default App;
