type EnvironmentName = undefined | "govnet";

var nameVal: EnvironmentName = undefined;

if(window.location.hostname.toLowerCase().indexOf("lands.resnet.qg") > -1) {
    nameVal = "govnet";
}

export const name: EnvironmentName = nameVal;