export const manageSecurity = "ManageSecurity";

export const createProjectMetadata = "CreateProjectMetadata";
export const viewProjectMetadata = "ViewProjectMetadata";
export const editProjectMetadata = "EditProjectMetadata";
export const deleteProjectMetadata = "DeleteProjectMetadata";

export const createRasterSeries = "CreateRasterSeries";
export const viewRasterSeries = "ViewRasterSeries";
export const editRasterSeries = "EditRasterSeries";
export const deleteRasterSeries = "DeleteRasterSeries";

export const createRasters = "CreateRasters";
export const viewRasters = "ViewRasters";
export const editRasters = "EditRasters";
export const deleteRasters = "DeleteRasters";
export const editClosedRasters = "EditClosedRasters";

export const createVectorSeries = "CreateVectorSeries";
export const viewVectorSeries = "ViewVectorSeries";
export const editVectorSeries = "EditVectorSeries";
export const deleteVectorSeries = "DeleteVectorSeries";

export const createDatasets = "CreateDatasets";
export const viewDatasets = "ViewDatasets";
export const editDatasets = "EditDatasets";
export const approveOrDenyDatasets = "ApproveOrDenyDatasets";
export const cancelDatasetRequests = "CancelDatasetRequests";

export const createLookups = "CreateLookups";
export const viewLookups = "ViewLookups";
export const editLookups = "EditLookups";
export const deleteLookups = "DeleteLookups";

export const manageQSpatialDefaults = "ManageQSpatialDefaults";