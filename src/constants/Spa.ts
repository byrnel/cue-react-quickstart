var baseUrlVal: string;
if(process.env.REACT_APP_SPA_BASEURL != null) {
    baseUrlVal = process.env.REACT_APP_SPA_BASEURL!;
} else if (window.location.hostname.indexOf("localhost") > -1) {
    baseUrlVal = window.location.protocol + "//" + window.location.hostname + ":3000";
} else {
    baseUrlVal = window.location.protocol + "//" + window.location.hostname;
}

export const baseUrl = baseUrlVal;