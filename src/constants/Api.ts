var baseUrlVal;
if(process.env.REACT_APP_API_BASEURL != null) {
    baseUrlVal = process.env.REACT_APP_API_BASEURL!;
} else if (window.location.hostname.indexOf("localhost") > -1) {
    baseUrlVal = window.location.protocol + "//" + window.location.hostname + ":4001/api";
} else {
    baseUrlVal = window.location.protocol + "//" + window.location.hostname + "/api";
}

export const baseUrl = baseUrlVal;