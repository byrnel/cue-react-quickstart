import * as React from "react";
import { Switch, Route, Redirect, RouteComponentProps } from "react-router-dom";
import { SaliConfigurePrintArea } from "./common/ui/";
import { Container, Segment } from "semantic-ui-react";
import { UserStore } from "./stores/";
import { NotFoundComponent } from "./features/errors/NotFoundComponent";
import SaliNavigation, { ITabGroupProps, INaviRoutes, INaviGroupRoutes, INaviRouteGroups, INaviRouteConfig } from "./common/ui/SaliNavigation";

// All routes are now centralised and maintained handled within this class
interface ConfigureRoutesProps {
  StandardRoutes: INaviRouteConfig[];
  TabbedRoutes: INaviRouteGroups;
}

interface IBaseProps {
  index: number;
}

export interface IRouteProps extends IBaseProps {
  route: INaviRouteConfig;
}

export interface ITabbedRouteProps extends IBaseProps {
  troute: INaviGroupRoutes;
  combined_links?: Array<ITabGroupProps>;
}

interface INaviTabbedRoutes {
  grouped: INaviRoutes[];
}

export class ConfigureRoutes extends React.Component<{}, {}> {

  // COMBINED if true, then all links will be displayed in the side navigation within the SaliTabControl otherwise components and links will be displayed separately 
  routes: ConfigureRoutesProps = { StandardRoutes: [], TabbedRoutes: { COMBINED: false, GROUPS: [] } };
  tabbedGroupRoutes: INaviGroupRoutes[] = [];
  allLinks: Array<ITabGroupProps>;

  constructor(props: {}) {
    super(props);
    this.routes.StandardRoutes = SaliNavigation.loadStandardRoutes();
    this.routes.TabbedRoutes = SaliNavigation.loadTabbedRoutes(this.routes.TabbedRoutes.COMBINED);
    this.tabbedGroupRoutes = this.routes.TabbedRoutes.GROUPS; // We need to reverse the order of routes so that they match in react router
  }

  renderMethod(config: IRouteProps, props: RouteComponentProps<any>) {
     if (config.route.COMPONENT.name === "Contact") {
      return (
        <SaliConfigurePrintArea displayLastModifiedDate={config.route.DISPLAYLASTUPDATED} showprint={config.route.INCLUDE_PRINT_AREA} crums={config.route.BREADCRUMS} {...props} lastModified={config.route.LASTUPDATED} Component={() => <config.route.COMPONENT mailTo="soils@qld.gov.au" {...props} />} />
      );
    } else {
      if (config.route.INCLUDE_PRINT_AREA) {
        return (
          <SaliConfigurePrintArea displayLastModifiedDate={config.route.DISPLAYLASTUPDATED} showprint={config.route.INCLUDE_PRINT_AREA} crums={config.route.BREADCRUMS} {...props} lastModified={config.route.LASTUPDATED} Component={() => <config.route.COMPONENT  {...props} />} />
        );
      } else {
        return (
          <config.route.COMPONENT {...props} />
        );
      }
    }
  }

  renderSecureRoute(config: IRouteProps) {
    return (
      <Route
        key={config.index}
        path={config.route.PATH}
        exact={config.route.EXACT}
        render={props =>
          UserStore.isAuthenticated ? (
            config.route.AUTHORISED() ? (
              this.renderMethod(config, props)
            )
              : (
                <Redirect
                  to={{
                    pathname: SaliNavigation.LINKS_REGISTERED.UNAUTHRORISED_PAGE.LINK.GET_LINK(),
                    state: { from: props.location }
                  }}
                />
              )
          )
            :
            (
              <Redirect
                to={{
                  pathname: SaliNavigation.LINKS_REGISTERED.SIGNIN.LINK.GET_LINK(),
                  state: { from: props.location }
                }}
              />
            )
        }
      />
    );
  }

  renderRoute(config: IRouteProps) {
    if (config.route.SECURE) {
      return (
        this.renderSecureRoute(config)
      );
    } else {
      return (
        <Route
          key={config.index}
          path={config.route.PATH}
          exact={config.route.EXACT}
          render={(props) => this.renderMethod(config, props)}
        />
      );
    }
  }

  renderTabbedGroupRouteItem(tr: ITabbedRouteProps) {
    const componentprops = { links: tr.combined_links, children: this.loadTabbedRoutes({ grouped: tr.troute.ROUTES }) };
    return (
        <Route
          path={tr.troute.BASE_PATH}
          exact={tr.troute.EXACT}
          render={routeprops =>
            <tr.troute.TAB_COMPONENT {...routeprops} {...componentprops} />
          }
        />
      );
  }

  loadTabbedRoutes(rts: INaviTabbedRoutes) {
    const routes = rts.grouped.map((item, index) => {
      return (this.renderRoute({ index: index, route: item.ROUTE }));
    }
    );
    return (
      routes
    );
  }

  renderTabbedRoutes(tr: ITabbedRouteProps) {
    const componentprops = { links: tr.troute.LINKS , children: this.loadTabbedRoutes({ grouped: tr.troute.ROUTES }) };
    return (
      <Route
        path={tr.troute.BASE_PATH}
        exact={tr.troute.EXACT}
        render={routeprops =>
          <tr.troute.TAB_COMPONENT {...routeprops} {...componentprops} />
        }
      />
    );
  }

  DisplayRoutes(routing: ConfigureRoutesProps) {
    return (
      <Switch>
        <Route exact={true} path={SaliNavigation.LINKS_REGISTERED.UNAUTHRORISED_PAGE.LINK.GET_ROUTE()} component={SaliNavigation.LINKS_REGISTERED.UNAUTHRORISED_PAGE.ROUTE.COMPONENT} />
        {routing.StandardRoutes.map((route, index) => {
          return (this.renderRoute({ index, route }));
        }
        )
        }
        {routing.TabbedRoutes && routing.TabbedRoutes.GROUPS.map((troute, index) => {
          return (this.renderTabbedRoutes({ index, troute }));
        }
        )
        }
        <Route component={NotFoundComponent} />
      </Switch>
    );
  }

  render() {
    return (
      <Container fluid={true} >
        <Segment padded={false} secondary={true}>
          {this.DisplayRoutes(this.routes)}
        </Segment>
      </Container>
    );
  }
}

const configureRoutes = ConfigureRoutes;
export default configureRoutes;
