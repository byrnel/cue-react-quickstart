import { observable } from "mobx";
import { ApiService, Logger } from "../../services/";
import { UserStore } from "../../stores/";
import SignInModel from "./SignInModel";
import * as Toasts from "../../common/Toasts";

class SignInStore {
    @observable credentials: SignInModel = new SignInModel();

    // Misc / UI concerns
    @observable isSigningIn: boolean = false;
    @observable isSigningOut: boolean = false;
    @observable signInError?: string;

    signIn(simulate?: boolean) {
        UserStore.user = undefined;
        this.isSigningIn = true;
        this.signInError = undefined;

        if(simulate) {
            UserStore.user = UserStore.dummyLogin();
            UserStore.isAuthenticated = true;
            return Promise.resolve(UserStore.user);
        } else {
            return ApiService
            .postIncludeResponse("/authentication/signin", {
                userName: this.credentials.userName,
                password: this.credentials.password // TOFO: Lawrence - Is this required anylonger?
            })
            .then(response => {
                this.isSigningIn = false;

                if(response.ok) {
                    this.signInError = undefined;
                    this.credentials.password = ""; // Clear password so it's not pre-filled when the user logs out.
                    return UserStore.getCurrentUser();
                }
                
                if(response.status === 400 && response.data != null) {
                    this.signInError = response.data.error;
                    return Promise.resolve(undefined);
                }

                return Promise.reject(undefined);
            })
            .catch(error => {
                Logger.error(error);
                this.isSigningIn = false;
                Toasts.networkError();
                throw error;
            });
        }
    }

    signOut(simulate?: boolean) {
        this.isSigningOut = true;
        if(simulate) {
            UserStore.isAuthenticated = false;
            UserStore.user = undefined;
            this.isSigningOut = false;
            return Promise.resolve();
        } else {
        return ApiService
            .postIncludeResponse("/authentication/signout", {})
            .then((response) => {
                this.isSigningOut = false;
                if(response.ok) {
                    UserStore.isAuthenticated = false;
                    UserStore.user = undefined;
                    return Promise.resolve();
                }
                return Promise.reject(undefined);
            })
            .catch(error => {
                Logger.error(error);
                this.isSigningOut = false;
                Toasts.networkError();
                throw error;
            });
        }
    }
}

export default new SignInStore();