import * as React from "react";
import { observer } from "mobx-react";
import { Redirect, RouteComponentProps } from "react-router";
import { Icon, Form, Button, Grid, Message } from "semantic-ui-react";
import { BindConfig } from "../../common/";
import SignInStore from "./SignInStore";
import "./sign-in.css";
import * as Environment from "../../constants/Environment";

interface ISignInComponentState {
    userName: string;
    password: string;
    redirectToReferrer: boolean;
    messageVisible: boolean;
}

@observer
export default class SignInComponent extends React.Component<RouteComponentProps<any>, ISignInComponentState> {
    constructor(props: RouteComponentProps<any>) {
        super(props);

        this.state = {
            userName: "",
            password: "",
            redirectToReferrer: false,
            messageVisible: true
        };

        this.handleSignIn = this.handleSignIn.bind(this);
        this.handleDismiss = this.handleDismiss.bind(this);
    }

    handleSignIn(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        SignInStore.signIn(true)
            .then((user) => user != null && this.setState({ redirectToReferrer: true }));
    }

    handleDismiss = () => {
        this.setState({ messageVisible: false });
    }

    render() {
        const { from } = this.props.location.state || "/";
        const { redirectToReferrer, messageVisible } = this.state;
        const bind = BindConfig({ context: SignInStore.credentials });

        if (Environment.name === "govnet") {
            return (
                <div className="js-sign-in-component">
                    <div className="ui two column centered grid">
                        <div className="column">
                            <Message
                                icon={true}
                                negative={true}
                                onDismiss={this.handleDismiss}
                            >
                                <Icon name="info" />
                                <Message.Content>
                                    <Message.Header>Access to this website is restricted</Message.Header>
                                    You are not authorised to access this website. Please contact an administrator.
                                </Message.Content>
                            </Message>
                        </div>
                    </div>
                </div>
            );
        }

        // Taken from non-react semantic-ui examples
        return (
            <div className="sign-in-page-container js-sign-in-component">

                {redirectToReferrer && (<Redirect to={from || "/"} />)}

                <div className="sign-in-page">
                    <Grid verticalAlign="middle" centered={true}>
                        <Grid.Row>
                            {from && messageVisible && (
                                <Grid.Column style={{ maxWidth: "800px" }}>
                                    <Message
                                        icon={true}
                                        negative={true}
                                        onDismiss={this.handleDismiss}
                                    >
                                        <Icon name="info" />
                                        <Message.Content>
                                            <Message.Header>Access to <strong>{from.pathname}</strong> is restricted</Message.Header>
                                            You must log in to view this page.
                                    </Message.Content>
                                    </Message>
                                </Grid.Column>
                            )}
                        </Grid.Row>
                    </Grid>
                    <Grid verticalAlign="middle" centered={true} className="sign-in-component">
                        <Grid.Row>
                            <Grid.Column style={{ maxWidth: "450px" }}>
                                <div id="cloud-wrap">
                                    <div className="x0"><div className="sign-in-saucer" /></div>
                                    <div className="x1"><div className="sign-in-cloud" /></div>
                                    <div className="x2"><div className="sign-in-cloud" /></div>
                                    <div className="x3"><div className="sign-in-cloud" /></div>
                                    <div className="x4"><div className="sign-in-cloud" /></div>
                                    <div className="x5"><div className="sign-in-cloud" /></div>
                                </div>
                                <Form>
                                    <div className="ui stacked segment">
                                        <Form.Input disabled={SignInStore.isSigningIn} autoFocus={true} {...bind("userName")} placeholder="Username" icon="user" iconPosition="left" className="js-user-name-input" />
                                        {/*<Form.Input {...bind("password")} placeholder="Password" icon="lock" iconPosition="left" type="password" className="js-password-input" />*/}
                                        <Button color="blue" fluid={true} onClick={this.handleSignIn} loading={SignInStore.isSigningIn} className="js-sign-in-button">Sign in</Button>
                                    </div>
                                    {SignInStore.signInError && <Message negative={true}>{SignInStore.signInError}</Message>}
                                </Form>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </div>
            </div>
        );
    }
}