import { observable } from "mobx";

export default class SignInModel {
    @observable userName: string = "";
    @observable password: string = "";
}