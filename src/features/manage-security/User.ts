import { observable } from "mobx";
import * as A from "../../services/A";
import Group from "./Group";
import Activity from "./Activity";

export default class User {
    @observable id?: number;
    @observable userName: string = "";
    @observable fullName: string = "";
    @observable windowsAccountName: string = "";
    @observable windowsSid: string = "";
    @observable groups: Group[] = [];
    @observable authorizedActivities: Activity[] = [];

    static fromJson(json: any) {
        const user = new User();
        user.id = A.nullableNumber(json.id);
        user.userName = A.string(json.userName);
        user.fullName = A.string(json.fullName);
        user.windowsAccountName = A.string(json.windowsAccountName);
        user.windowsSid = A.string(json.windowsSid);
        user.groups = (A.nullableList(json.groups) || []).map(Group.fromJson);
        user.authorizedActivities = (A.nullableList(json.authorizedActivities) || []).map(Activity.fromJson);
        return user;
    }

    clone = () => User.fromJson(JSON.parse(JSON.stringify(this)));
}