import * as React from "react";
import * as R from "ramda";
import { observer } from "mobx-react";
import { Modal, Button, Form, Header } from "semantic-ui-react";
import { BindConfig } from "../../../common";
import Group from "../Group";
import Activity from "../Activity";
import ErrorList from "../../../common/ErrorList";

interface IGroupEditorProps {
    group?: Group;
    activityOptions?: Activity[];
    isLoading: boolean;
    errors?: IErrors;
    onCancel: () => void;
    onCompleteEditing: (editedGroup: Group) => void;
}

@observer
export default class GroupEditor extends React.Component<IGroupEditorProps, {}> {
    /**
     * Adds or removes the specified activity from the edited groups's list of
     * authorized activities depending on whether the checkbox corresponding to
     * the activity was checked or unchecked.
     */
    handleChangeActivity = (activity: Activity, e: React.FormEvent<HTMLInputElement>, data: any) => {
        const { group } = this.props;
        if(group == null) {
            return;
        }

        group.authorizedActivities =
            data.checked
            ? R.union([activity], group.authorizedActivities)
            : group.authorizedActivities.filter(x => x.name !== activity.name);

        this.forceUpdate();
    }

    render() {
        const {
            group, onCancel, onCompleteEditing, isLoading, activityOptions,
            errors
         } = this.props;

        if(isLoading
            || group == null
            || activityOptions == null) {
            return <Modal dimmer={true} open={true} />;
        }

        const bind = BindConfig({
            context: group,
            errors: errors
        });

        return (
            <Modal dimmer={true} open={true}>
                <Modal.Header>{group.id != null ? "Edit Group: " + group.name : "New Group"}</Modal.Header>
                <Modal.Content>
                    <Modal.Description>
                        <ErrorList errors={errors} />
                        <Form>
                            <Form.Group widths="equal">
                                <Form.Input
                                    {...bind("name")}
                                    label="Name" 
                                    className="js-name-input"
                                />
                            </Form.Group>

                            <Header>Authorised Activities</Header>
                            {activityOptions.map(activity => (
                                <Form.Checkbox
                                    checked={R.any(x => x.name === activity.name, group!.authorizedActivities)}
                                    onChange={this.handleChangeActivity.bind(null, activity)}
                                    label={activity.name} 
                                />
                            ))}
                        </Form>
                    </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                <Button color="black" onClick={onCancel}>
                    Cancel
                </Button>
                <Button
                    positive={true}
                    icon="checkmark"
                    labelPosition="right"
                    content="Save"
                    onClick={onCompleteEditing.bind(null, group)}
                />
                </Modal.Actions>
            </Modal>
        );
    }
}