import { observable, action } from "mobx";
import { ApiService, Logger } from "../../../services/";
import * as A from "../../../services/A";
import Group from "../Group";
import Activity from "../Activity";
import { ISaliModalProps } from "../../../common/SaliModal";
import * as Toasts from "../../../common/Toasts";

class ManageGroupsStore {
    // Groups list
    @observable groups?: Group[];
    @observable isLoadingGroups: boolean = false;
    @observable networkErrorLoadGroups: boolean = false;    

    // Group deletion
    @observable maybeDeleteGroup?: Group;
    @observable isDeletingGroup: boolean = false;
    @observable networkErrorDeleteGroup: boolean = false;

    // Group editing
    @observable isEditingGroup: boolean = false;
    @observable maybeEditGroup?: Group;
    @observable isLoadingEditGroup: boolean = false;
    @observable isSavingEditGroup: boolean = false;
    @observable networkErrorEditGroup: boolean = false;
    @observable editGroupErrors?: IErrors;
    @observable activityOptions?: Activity[];

    // Misc UI stuff
    @observable modalOptions?: ISaliModalProps;

    @action reset = () => {
        this.groups = undefined;
        this.isLoadingGroups = false;
        this.networkErrorLoadGroups = false;

        this.maybeDeleteGroup = undefined;
        this.isDeletingGroup = false;
        this.networkErrorDeleteGroup = false;

        this.isEditingGroup = false;
        this.maybeEditGroup = undefined;
        this.isLoadingEditGroup = false;
        this.isSavingEditGroup = false;
        this.networkErrorEditGroup = false;
        this.editGroupErrors = undefined;
        this.activityOptions = undefined;

        this.modalOptions = undefined;
    }

    /**
     * Lists all groups in the system. Does not paginate because there
     * probably won't ever be many groups.
     */
    @action listGroups = () => {
        this.isLoadingGroups = true;
        this.networkErrorLoadGroups = false;
        return ApiService
            .get("/security/groups")
            .then(action((data: any) => {
                this.groups = A.list(data).map(Group.fromJson);
                this.isLoadingGroups = false;
            }))
            .catch(action((error) => {
                Logger.error(error);
                this.isLoadingGroups = false;
                this.networkErrorLoadGroups = true;
                Toasts.networkError();
            }));
    }

    /**
     * Prompts the administrator whether they really want to delete this group.
     */
    @action promptDeleteGroup = (group: Group) => {
        this.maybeDeleteGroup = group;
        this.openDeleteModal();
    }

    /**
     * Actually deletes the group that the administrator has confirmed they
     * wish to delete.
     */
    @action confirmDeleteGroup = () => {
        if(this.maybeDeleteGroup == null) {
            return Promise.reject(undefined);
        }

        this.isDeletingGroup = true;
        this.networkErrorDeleteGroup = false;
        return ApiService
            .delete("/security/groups/" + this.maybeDeleteGroup.id)
            .then(action(() => {
                this.isDeletingGroup = false;
                this.maybeDeleteGroup = undefined;

                // Refresh the page in case we changed our own activities
                // so the nav bar can reload with the right features
                // enabled.
                window.location.reload();
            }))
            .catch(action((e) => {
                Logger.error(e);
                this.isDeletingGroup = false;
                this.networkErrorDeleteGroup = true;
                Toasts.networkError();
                throw e;
            }));
    }

    /**
     * If the administrator has been prompted to delete a group this allows them
     * to cancel the intent to delete the group.
     */
    @action cancelDeleteGroup = () => {
        this.maybeDeleteGroup = undefined;
        this.isDeletingGroup = false;
        this.networkErrorDeleteGroup = false;
    }

    /**
     * Gets a fully populated group model and allows the administrator to edit
     * it. This can be used to change the group's authorizations.
     */
    @action beginEditGroup = (group?: Group) => {
        this.isLoadingEditGroup = true;
        this.networkErrorEditGroup = false;

        // Get all activities
        return ApiService
            .get("/security/lookups")
            .then(action((lookupData: any) => {
                this.activityOptions = A.list(lookupData.activities).map(Activity.fromJson);
                this.isLoadingEditGroup = false;

                if(group != null) {
                    // Re-fetch the group to get the full model
                    return ApiService
                        .get("/security/groups/" + group.id)
                        .then(action((data: any) => {
                            this.isEditingGroup = true;
                            this.maybeEditGroup = Group.fromJson(data);
                            this.isLoadingEditGroup = false;
                        }))
                        .catch(action((e) => {
                            Logger.error(e);
                            this.isLoadingEditGroup = false;
                            this.networkErrorEditGroup = true;
                            throw e;
                        }));
                } else {
                    // We're editing a new group - don't need to fetch it.
                    this.isEditingGroup = true;
                    this.maybeEditGroup = new Group();
                    this.isLoadingEditGroup = false;
                    return Promise.resolve();
                }
            }))
            .catch(action((e) => {
                Logger.error(e);
                this.isLoadingEditGroup = false;
                this.networkErrorEditGroup = true;
                Toasts.networkError();
            }));
    }

    /**
     * Saves the group that is currently being edited.
     */
    @action completeEditGroup = (editedGroup: Group) => {
        this.isSavingEditGroup = true;
        this.networkErrorEditGroup = false;
        this.editGroupErrors = undefined;

        var request = editedGroup.id != null
            ? ApiService.post("/security/groups/" + editedGroup.id, editedGroup)
            : ApiService.put("/security/groups", editedGroup);

        return request
            .then(action((errors: IErrors) => {
                this.isSavingEditGroup = false;

                if(Object.keys(errors).length > 0) {
                    // We have errors
                    this.editGroupErrors = errors;
                } else {
                    // We don't have errors
                    this.isEditingGroup = false;
                    this.maybeDeleteGroup = undefined;

                    // Refresh the page in case we changed our own activities
                    // so the nav bar can reload with the right features
                    // enabled.
                    window.location.reload();
                }
            }))
            .catch(action((e) => {
                Logger.error(e);
                this.isSavingEditGroup = false;
                this.networkErrorEditGroup = true;
                Toasts.networkError();
            }));
    }

    /**
     * Throws away any unsaved edits to the group that is currently being edited.
     */
    @action cancelEditGroup = () => {
        this.isEditingGroup = false;
        this.maybeEditGroup = undefined;
        this.isLoadingEditGroup = false;
        this.networkErrorEditGroup = false;
        this.isSavingEditGroup = false;
        this.editGroupErrors = undefined;
    }

    /**
     * Shows a popup dialog
     */
    openModal = (options: ISaliModalProps) => this.modalOptions = options;
    
    /**
     * If a popup dialog is open this will close it.
     */
    closeModal = () => this.modalOptions = undefined;
    
    openDeleteModal = () => {
        if(this.maybeDeleteGroup == null) {
            return;
        }

        this.openModal({
            header: "Delete Group?",
            content: "Are you sure you want to delete the " + this.maybeDeleteGroup.name + " group? Any users which are assigned to this group may lose some access rights.",
            icon: "trash",
            negativeText: "Cancel",
            positiveText: "Delete",
            positiveColor: "red",
            onPositiveClick: () => this.confirmDeleteGroup()
                .then(() => this.closeModal()),
            onClose: this.closeModal
        });
    }
}

export default new ManageGroupsStore();