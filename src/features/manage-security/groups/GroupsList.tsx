import * as React from "react";
import { observer } from "mobx-react";
import { Container, Grid, ButtonGroup, Table, Button, Message, Loader } from "semantic-ui-react";
import { SaliContent } from "../../../common/sali-page";
import { TableCellValue, SaliModal } from "../../../common/";
import ManageGroupsStore from "./ManageGroupsStore";
import Group from "../Group";
import GroupEditor from "./GroupEditor";

@observer
export default class GroupsList extends React.Component<{}, {}> {

    componentDidMount() {
        ManageGroupsStore.reset();
        ManageGroupsStore.listGroups();
    }

    renderGroupsList(groups: Group[]) {
        const { promptDeleteGroup, beginEditGroup } = ManageGroupsStore;

        return (
            <Table>
                <Table.Header>
                    <Table.HeaderCell>Group Name</Table.HeaderCell>
                    <Table.HeaderCell />
                </Table.Header>
                <Table.Body>
                    {groups.map(group => (
                        <Table.Row key={group.id} className="js-group" >
                            <TableCellValue value={group.name} />
                            <Table.Cell>
                                <ButtonGroup floated="right">
                                    <Button
                                        color="red"
                                        size="small"
                                        floated="right"
                                        content="Delete"
                                        onClick={promptDeleteGroup.bind(null, group)}
                                    />
                                    <Button
                                        color="blue"
                                        size="small"
                                        floated="right"
                                        content="Edit"
                                        onClick={beginEditGroup.bind(null, group)}
                                    />
                                </ButtonGroup>
                            </Table.Cell>
                        </Table.Row>
                    )
                    )}
                </Table.Body>
            </Table>
        );
    }

    renderNoGroupsList() {
        return (
            <Message>
                <Message.Header>
                    No groups
                </Message.Header>
                <p>
                    There are no groups yet. Create a group to make it easier to assign actions to users.
                </p>
            </Message>
        );
    }

    render() {
        const {
            isLoadingGroups, groups, isEditingGroup, isLoadingEditGroup,
            maybeEditGroup, beginEditGroup, cancelEditGroup, completeEditGroup,
            modalOptions, activityOptions, editGroupErrors, networkErrorLoadGroups
        } = ManageGroupsStore;

        if (isLoadingGroups) {
            return (
                <SaliContent>
                    <Loader active={true} inline="centered" />
                </SaliContent>
            );
        }

        return (

            <Container fluid={true} className="js-groups-list-component">
                <Grid stackable={true}>
                    <Grid.Row>
                        <Grid.Column>
                            <Message
                                icon="group"
                                header="Groups"
                                attached={true}
                            />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <Button.Group floated="right">
                                <Button
                                    color="green"
                                    alt="Add Group"
                                    onClick={() => beginEditGroup()}
                                    loading={isLoadingEditGroup}
                                    disabled={isLoadingEditGroup}
                                    icon="add"
                                    content="Add Group"
                                    className="js-create-user-button"
                                />
                            </Button.Group>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            {!networkErrorLoadGroups && (
                                groups != null &&
                                    groups.length > 0
                                    ? this.renderGroupsList(groups)
                                    : this.renderNoGroupsList()
                            )}

                            {isEditingGroup &&
                                <GroupEditor
                                    group={maybeEditGroup}
                                    errors={editGroupErrors}
                                    activityOptions={activityOptions}
                                    isLoading={isLoadingEditGroup}
                                    onCancel={cancelEditGroup}
                                    onCompleteEditing={completeEditGroup}
                                />}
                            {modalOptions && <SaliModal {...modalOptions} />}
                        </Grid.Column>
                    </Grid.Row>
                </Grid >
            </Container>
        );
    }
}