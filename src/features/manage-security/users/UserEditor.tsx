import * as React from "react";
import * as R from "ramda";
import { observer } from "mobx-react";
import { Modal, Button, Form, Header } from "semantic-ui-react";
import User from "../User";
import { BindConfig } from "../../../common";
import Group from "../Group";
import Activity from "../Activity";
import ErrorList from "../../../common/ErrorList";

interface IUserEditorProps {
    user?: User;
    errors?: IErrors;
    groupOptions?: Group[];
    activityOptions?: Activity[];
    isLoading: boolean;
    onCancel: () => void;
    onCompleteEditing: (editedUser: User) => void;
}

@observer
export default class UserEditor extends React.Component<IUserEditorProps, {}> {
    /**
     * Adds or removes the specified group from the edited user's list of
     * selected groups depending on whether the checkbox corresponding to the
     * group was checked or unchecked.
     */
    handleChangeGroup = (group: Group, e: React.FormEvent<HTMLInputElement>, data: any) => {
        const { user } = this.props;

        if(user == null) {
            return;
        }

        user.groups =
            data.checked
            ? R.union([group], user.groups)
            : user.groups.filter(x => x.id !== group.id);

        this.forceUpdate();
    }

    /**
     * Adds or removes the specified activity from the edited user's list of
     * authorized groups depending on whether the checkbox corresponding to the
     * group was checked or unchecked.
     */
    handleChangeActivity = (activity: Activity, e: React.FormEvent<HTMLInputElement>, data: any) => {
        const { user } = this.props;
        if(user == null) {
            return;
        }

        user.authorizedActivities =
            data.checked
            ? R.union([activity], user.authorizedActivities)
            : user.authorizedActivities.filter(x => x.name !== activity.name);

        this.forceUpdate();
    }

    render() {
        const {
            user, onCancel, onCompleteEditing, isLoading, groupOptions,
            activityOptions, errors
         } = this.props;

        if(isLoading
            || user == null
            || groupOptions == null
            || activityOptions == null) {
            return <Modal dimmer={true} open={true}/>;
        }

        const bind = BindConfig({
            context: user,
            errors: errors
        });

        return (
            <Modal dimmer={true} open={true}>
                <Modal.Header>{user.id != null ? "Edit " + user.fullName : "New User"}</Modal.Header>
                <Modal.Content>
                    <Modal.Description>
                        <ErrorList errors={errors} />
                        <Form>
                            <Form.Group widths="equal">
                                <Form.Input
                                    {...bind("windowsAccountName")}
                                    label="Windows Account Name"
                                    className="js-windows-account-name-input"
                                />
                            </Form.Group>
                            <Form.Group widths="equal">
                                <Form.Input
                                    {...bind("userName")}
                                    label="User Name" 
                                    className="js-user-name-input"
                                />
                                <Form.Input
                                    {...bind("fullName")}
                                    label="Full Name" 
                                    className="js-full-name-input"
                                />
                            </Form.Group>

                            <Header>Groups</Header>
                            {groupOptions.map(group => (
                                <Form.Checkbox
                                    checked={R.any(x => x.id === group.id, user!.groups)}
                                    onChange={this.handleChangeGroup.bind(null, group)}
                                    label={group.name} 
                                />
                            ))}

                            <Header>Authorised Activities</Header>
                            {activityOptions.map(activity => (
                                <Form.Checkbox
                                    checked={R.any(x => x.name === activity.name, user!.authorizedActivities)}
                                    onChange={this.handleChangeActivity.bind(null, activity)}
                                    label={activity.name} 
                                />
                            ))}
                        </Form>
                    </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                <Button color="black" onClick={onCancel}>
                    Cancel
                </Button>
                <Button
                    positive={true}
                    icon="checkmark"
                    labelPosition="right"
                    content="Save"
                    onClick={onCompleteEditing.bind(null, user)}
                />
                </Modal.Actions>
            </Modal>
        );
    }
}