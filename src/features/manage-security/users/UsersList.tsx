import * as React from "react";
import { observer } from "mobx-react";
import { Container, ButtonGroup, Form, Grid, Button, Table, Message } from "semantic-ui-react";
import { SaliSection } from "../../../common/sali-page";
import { BindConfig, Paginator, TableCellValue, SaliModal } from "../../../common/";
import ManageUsersStore from "./ManageUsersStore";
import User from "../User";
import UserEditor from "./UserEditor";

@observer
export default class UsersList extends React.Component<{}, {}> {

    componentDidMount() {
        ManageUsersStore.reset();
        ManageUsersStore.searchUsers();
        // ManageGroupsStore.reset();
        // ManageGroupsStore.listGroups();
    }

    /**
     * When a user search has been performed and at least one user was found,
     * this will render a list of users and provide links for the administrator
     * to edit or delete each users.
     */
    renderUsersTable(userSearchResults: IPaginatedResult<User>) {
        const { promptDeleteUser, beginEditUser } = ManageUsersStore;

        return (
            <Table>
                <Table.Header>
                    <Table.HeaderCell>User Name</Table.HeaderCell>
                    <Table.HeaderCell>Full Name</Table.HeaderCell>
                    <Table.HeaderCell />
                </Table.Header>
                <Table.Body>
                    {userSearchResults.entities.map(user => (
                        <Table.Row key={user.id} className="js-user-search-results">
                            <TableCellValue value={user.userName} />
                            <TableCellValue value={user.fullName} />
                            <Table.Cell>
                                <ButtonGroup floated="right">
                                    <Button
                                        color="red"
                                        content="Delete"
                                        onClick={promptDeleteUser.bind(null, user)}
                                    />
                                    <Button
                                        color="blue"
                                        content="Edit"
                                        onClick={beginEditUser.bind(null, user)}
                                    />
                                </ButtonGroup>
                            </Table.Cell>
                        </Table.Row>
                    )
                    )}
                </Table.Body>
            </Table>
        );
    }

    renderNoUsersList() {
        return (
            <Message>
                <Message.Header>
                    No users found
                </Message.Header>
                <p>
                    No users could be found using those search options. Please revise your query and try again.
                </p>
            </Message>
        );
    }

    render() {
        const {
            isLoadingUsers, searchUsers, clearSearchOptions, userSearchResults,
            isEditingUser, isLoadingEditUser, maybeEditUser, beginEditUser,
            cancelEditUser, completeEditUser, modalOptions, groupOptions,
            activityOptions, editUserErrors
        } = ManageUsersStore;

        const bind = BindConfig({
            context: ManageUsersStore,
            errors: editUserErrors
        });

        return (
            <Container fluid={true} className="js-users-list-component">
                <Grid stackable={true}>
                    <Grid.Row>
                        <Grid.Column>
                            <Message
                                icon="add user"
                                header="Users"
                                attached={true}
                            />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <Button.Group floated="right">
                                <Button
                                    color="green"
                                    alt="Add User"
                                    onClick={() => beginEditUser()}
                                    loading={isLoadingEditUser}
                                    disabled={isLoadingEditUser}
                                    icon="add"
                                    content="Add User"
                                    className="js-create-user-button"
                                />
                            </Button.Group>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <Form>
                                <SaliSection header="Search">
                                    <Grid columns="3" stackable={true} verticalAlign="bottom">
                                        <Grid.Row>
                                            <Grid.Column><Form.Input {...bind("userName")} label="Username" /></Grid.Column>
                                            <Grid.Column><Form.Input {...bind("fullName")} label="Full Name" /></Grid.Column>
                                            <Grid.Column>
                                                <ButtonGroup floated="right">
                                                    <Button
                                                        id="clear"
                                                        content="Clear"
                                                        onClick={clearSearchOptions}
                                                    />
                                                    <Button
                                                        id="search"
                                                        color="yellow"
                                                        content="Search"
                                                        onClick={() => searchUsers()}
                                                        icon="search"
                                                        className="js-user-search-button"
                                                        loading={isLoadingUsers}
                                                    />
                                                </ButtonGroup>
                                            </Grid.Column>
                                        </Grid.Row>
                                    </Grid>
                                </SaliSection>
                            </Form>

                            {
                                userSearchResults != null && (userSearchResults.entities.length > 0
                                    ? this.renderUsersTable(userSearchResults)
                                    : this.renderNoUsersList())
                            }

                            {userSearchResults != null && (
                                <Table>
                                    <Table.Footer>
                                        <Table.Row>
                                            <Table.HeaderCell colSpan={4}>
                                                <Paginator
                                                    currentPage={userSearchResults.paginationOptions.page}
                                                    pages={userSearchResults.paginationOptions.pages}
                                                    onNavigate={searchUsers}
                                                />
                                            </Table.HeaderCell>
                                        </Table.Row>
                                    </Table.Footer>
                                </Table>
                            )}

                            {isEditingUser &&
                                <UserEditor
                                    user={maybeEditUser}
                                    errors={editUserErrors}
                                    groupOptions={groupOptions}
                                    activityOptions={activityOptions}
                                    isLoading={isLoadingEditUser}
                                    onCancel={cancelEditUser}
                                    onCompleteEditing={completeEditUser}
                                />}
                            {modalOptions && <SaliModal {...modalOptions} />}
                        </Grid.Column>
                    </Grid.Row>
                </Grid >
            </Container>
        );
    }
}