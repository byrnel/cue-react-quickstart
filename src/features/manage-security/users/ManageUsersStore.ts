import { observable, action } from "mobx";
import { ApiService, Logger } from "../../../services/";
import * as A from "../../../services/A";
import User from "../User";
import Group from "../Group";
import Activity from "../Activity";
import { ISaliModalProps } from "../../../common/SaliModal";
import { UserStore } from "../../../stores";
import * as Toasts from "../../../common/Toasts";

class ManageUsersStore {
    // User search options
    @observable userName: string = "";
    @observable fullName: string = "";

    // User search results
    @observable userSearchResults?: IPaginatedResult<User>;
    @observable isLoadingUsers: boolean = false;
    @observable networkErrorLoadUsers: boolean = false;    

    // User deletion
    @observable maybeDeleteUser?: User;
    @observable isDeletingUser: boolean = false;
    @observable networkErrorDeleteUser: boolean = false;

    // User editing
    @observable isEditingUser: boolean;
    @observable maybeEditUser?: User;
    @observable isLoadingEditUser: boolean = false;
    @observable isSavingEditUser: boolean = false;
    @observable networkErrorEditUser: boolean = false;
    @observable editUserErrors?: IErrors;
    @observable groupOptions?: Group[];
    @observable activityOptions?: Activity[];

    // Finding external users from external directory (LDAP / CorpAuth)
    @observable externalUsersSearchResults?: string[];
    @observable isLoadingExternalUsers: boolean = false;
    @observable networkErrorExternalUsers: boolean = false;

    // Misc UI stuff
    @observable modalOptions?: ISaliModalProps;

    @action reset = () => {
        this.userName = "";
        this.fullName = "";

        this.userSearchResults = undefined;
        this.isLoadingUsers = false;
        this.networkErrorLoadUsers = false;

        this.maybeDeleteUser = undefined;
        this.isDeletingUser = false;
        this.networkErrorDeleteUser = false;

        this.isEditingUser = false;
        this.maybeEditUser = undefined;
        this.isLoadingEditUser = false;
        this.isSavingEditUser = false;
        this.networkErrorEditUser = false;
        this.editUserErrors = undefined;
        this.groupOptions = undefined;
        this.activityOptions = undefined;

        this.externalUsersSearchResults = undefined;
        this.isLoadingExternalUsers = false;
        this.networkErrorExternalUsers = false;
    }

    /**
     * Searches for and presents users that match the given search critera.
     * If a page number is specified users for that page will be returned,
     * otherwise the first page will be returned.
     */
    @action searchUsers = (page?: number) => {
        this.isLoadingUsers = true;
        this.networkErrorLoadUsers = false;
        const paginationParams = this.userSearchResults != null
            ? { ...this.userSearchResults.paginationOptions, page: page || 1 }
            : {};
        return ApiService
            .post("/security/users/search", {
                userName: this.userName,
                fullName: this.fullName,
                ...paginationParams
            })
            .then(action((data: any) => {
                this.userSearchResults = {
                    entities:  A.list(data.entities).map(User.fromJson),
                    paginationOptions: {
                        page: A.number(data.paginationOptions.page),
                        pageSize: A.number(data.paginationOptions.pageSize),
                        pages: A.number(data.paginationOptions.pages),
                        sortProperty: A.string(data.paginationOptions.sortProperty),
                        sortDirection: data.paginationOptions.sortDirection === "descending"
                            ? "descending"
                            : "ascending",
                    }
                };
                this.isLoadingUsers = false;
            }))
            .catch(action((e) => {
                Logger.error(e);
                this.isLoadingUsers = false;
                this.networkErrorLoadUsers = false;
                Toasts.networkError();
            }));
    }

    @action clearSearchOptions = () => {
        this.userName = "";
        this.fullName = "";
    }

    /**
     * Prompts the administrator whether they really want to delete this user.
     */
    @action promptDeleteUser = (user: User) => {
        this.maybeDeleteUser = user;
        this.openDeleteModal();
    }

    /**
     * Actually deletes the user that the administrator has confirmed they
     * wish to delete.
     */
    @action confirmDeleteUser = () => {
        if(this.maybeDeleteUser == null) {
            return Promise.reject(undefined);
        }

        this.isDeletingUser = true;
        this.networkErrorDeleteUser = false;
        return ApiService
            .delete("/security/users/" + this.maybeDeleteUser.id)
            .then(action(() => {
                this.isDeletingUser = false;
                this.maybeDeleteUser = undefined;
                this.closeModal();
                this.searchUsers(this.userSearchResults!.paginationOptions.page);
            }))
            .catch(action((e) => {
                Logger.error(e);
                this.isDeletingUser = false;
                this.networkErrorDeleteUser = true;
                Toasts.networkError();
                throw e;
            }));
    }

    /**
     * If the administrator has been prompted to delete a user this allows them
     * to cancel the intent to delete the user.
     */
    @action cancelDeleteUser = () => {
        this.maybeDeleteUser = undefined;
        this.isDeletingUser = false;
        this.networkErrorDeleteUser = false;
    }

    /**
     * Gets a fully populated user model and allows the administrator to edit
     * it. This can be used to change the user's groups and authorizations.
     */
    @action beginEditUser = (user?: User) => {
        this.isLoadingEditUser = true;
        this.networkErrorEditUser = false;

        // Get all groups and activities
        return ApiService
            .get("/security/lookups")
            .then(action((lookupData: any) => {
                this.groupOptions = A.list(lookupData.groups).map(Group.fromJson);
                this.activityOptions = A.list(lookupData.activities).map(Activity.fromJson);

                if(user != null) {
                    // Re-fetch the user being edited to get the full user model
                    return ApiService
                        .get("/security/users/" + user.id)
                        .then(action((data: any) => {
                            this.isEditingUser = true;
                            this.maybeEditUser = User.fromJson(data);
                            this.isLoadingEditUser = false;
                        }))
                        .catch(action((e) => {
                            Logger.error(e);
                            this.isLoadingEditUser = false;
                            this.networkErrorEditUser = true;
                            throw e;
                        }));
                } else {
                    // We're editing a new user - don't need to fetch it.
                    this.isEditingUser = true;
                    this.maybeEditUser = new User();
                    this.isLoadingEditUser = false;
                    return Promise.resolve();
                }
            }))
            .catch(action((e) => {
                Logger.error(e);
                this.isLoadingEditUser = false;
                this.networkErrorEditUser = true;
                Toasts.networkError();
            }));
    }

    /**
     * Saves the user that is currently being edited (or created).
     */
    @action completeEditUser = (editedUser: User) => {
        this.isSavingEditUser = true;
        this.networkErrorEditUser = false;
        this.editUserErrors = undefined;

        var request = editedUser.id != null
            ? ApiService.post("/security/users/" + editedUser.id, editedUser)
            : ApiService.put("/security/users", editedUser);

        return request
            .then(action((errors: IErrors) => {
                this.isSavingEditUser = false;

                if(Object.keys(errors).length > 0) {
                    // We have errors
                    this.editUserErrors = errors;
                } else {
                    this.isEditingUser = false;
                    this.maybeDeleteUser = undefined;
                    
                    if(editedUser.id === UserStore.user!.id) {
                        // Refresh the page in case we changed our own activities
                        // so the nav bar can reload with the right features
                        // enabled.
                        window.location.reload();
                    } else {
                        // Reload users list
                        if(this.userSearchResults != null) {
                            this.searchUsers(this.userSearchResults.paginationOptions.page);
                        }
                    }
                }
            }))
            .catch(action((e) => {
                Logger.error(e);
                this.isSavingEditUser = false;
                this.networkErrorEditUser = true;                
                Toasts.networkError();
            }));
    }

    /**
     * Throws away any unsaved edits to the user that is currently being edited.
     */
    @action cancelEditUser = () => {
        this.isEditingUser = false;
        this.maybeEditUser = undefined;
        this.isLoadingEditUser = false;
        this.networkErrorEditUser = false;
        this.isSavingEditUser = false;
        this.editUserErrors = undefined;
    }

    /**
     * Queries an external resources (LDAP) for users which match the specified
     * query.
     */
    @action searchExternalUser = (query: string) => {
        this.isLoadingExternalUsers = true;
        this.networkErrorExternalUsers = false;
        return ApiService
            .get("/security/users/external/" + query)
            .then(action((data: any) => {
                this.externalUsersSearchResults = A.list(data).map(A.string);
                this.isLoadingExternalUsers = false;
            }))
            .catch(action((e) => {
                Logger.error(e);
                this.isLoadingExternalUsers = false;
                this.networkErrorExternalUsers = true;                
                Toasts.networkError();
            }));
    }

    /**
     * Shows a popup dialog
     */
    openModal = (options: ISaliModalProps) => this.modalOptions = options;
    
    /**
     * If a popup dialog is open this will close it.
     */
    closeModal = () => this.modalOptions = undefined;
    
    openDeleteModal = () => {
        if(this.maybeDeleteUser == null) {
            return;
        }

        this.openModal({
            header: "Delete User?",
            content: "Are you sure you want to delete " + this.maybeDeleteUser.fullName + "?",
            icon: "trash",
            negativeText: "Cancel",
            positiveText: "Delete",
            positiveColor: "red",
            onPositiveClick: () => this.confirmDeleteUser()
                .then(() => this.closeModal()),
            onClose: this.closeModal
        });
    }
}

export default new ManageUsersStore();