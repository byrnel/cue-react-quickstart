import * as React from "react";
import { observer } from "mobx-react";
import { Menu } from "semantic-ui-react";
import ManageUsersStore from "./users/ManageUsersStore";
import ManageGroupsStore from "./groups/ManageGroupsStore";
import UsersList from "./users/UsersList";
import GroupsList from "./groups/GroupsList";
import { SaliContainer, SaliHeader } from "../../common/sali-page";
import SaliMenu from "../../common/sali-page/SaliMenu";

interface ITab {
    id: string;
    name: string;
    icon: string;
    component: JSX.Element;
}

interface IManageSecurityComponentState {
    currentTab: ITab;
    tabs: ITab[];
}

@observer
export default class ManageSecurityComponent extends React.Component<{}, IManageSecurityComponentState> {
    constructor(props: {}) {
        super(props);

        const tabs = [
            { id: "users", name: "Users", icon: "users", component: <UsersList /> },
            { id: "groups", name: "Groups", icon: "key", component: <GroupsList /> },
        ];
        this.state = { currentTab: tabs[0], tabs: tabs };
    }

    componentDidMount() {
        ManageUsersStore.reset();
        ManageUsersStore.searchUsers();
        ManageGroupsStore.reset();
        ManageGroupsStore.listGroups();
    }

    render() {
        const { currentTab, tabs } = this.state;

        return (
            <SaliContainer>
                <SaliHeader
                    icon="lock"
                    header="Manage Security"
                />
                <SaliMenu>
                    {tabs.map(tab => (
                        <Menu.Item 
                            id={tab.id}
                            icon={tab.icon} 
                            name={tab.name}
                            active={tab === currentTab} 
                            onClick={() => this.setState({ currentTab: tab })}
                            className={"js-" + tab.id + "users-tab"}
                        />
                    ))}
                </SaliMenu>
                {currentTab.component}
            </SaliContainer>
        );
    }
}