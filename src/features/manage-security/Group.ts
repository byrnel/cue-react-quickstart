import { observable } from "mobx";
import * as A from "../../services/A";
import Activity from "./Activity";

export default class Group {
    @observable id?: number;
    @observable name: string = "";
    @observable authorizedActivities: Activity[] = [];

    static fromJson(json: any) {
        const group = new Group();
        group.id = A.nullableNumber(json.id);
        group.name = A.string(json.name);
        group.authorizedActivities = (A.nullableList(json.authorizedActivities) || []).map(Activity.fromJson);
        return group;
    }

    clone = () => Group.fromJson(JSON.parse(JSON.stringify(this)));
}