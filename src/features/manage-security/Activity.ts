import { observable } from "mobx";
import * as A from "../../services/A";

export default class Activity {
    @observable name: string = "";

    static fromJson(json: any) {
        const activity = new Activity();
        activity.name = A.string(json.name);
        return activity;
    }
}