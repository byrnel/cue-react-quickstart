import * as React from "react";
import { RouteComponentProps } from "react-router";
import { Container, Message } from "semantic-ui-react";

export class NotFoundComponent extends React.Component<RouteComponentProps<any>, {}> {
    constructor(props: RouteComponentProps<any>) {
        super(props);
    }

    handleDismiss = () => {
        // Just redirect to root
        this.props.history.push("/");
    }

    render() {
        return (
            <Container>
                <Message
                    size="big"
                    negative={true}
                    icon="info"
                    onDismiss={this.handleDismiss}
                    header="Navigation Error"
                    content={this.props.location.pathname}
                />
            </Container>
        );
    }
}