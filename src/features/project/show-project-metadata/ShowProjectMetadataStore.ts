import { observable } from "mobx";
import { ApiService, Logger } from "../../../services";
import ShowProjectMetadataModel from "./ShowProjectMetadataModel";

class ShowProjectMetadataSetStore {
    
    @observable projectMetadataSet: ShowProjectMetadataModel;

    // UI stuff
    @observable isBusy: boolean;
    @observable networkErrorOnLoad: boolean;

    constructor() {
        this.reset();
    }

    reset() {
        this.projectMetadataSet = new ShowProjectMetadataModel();
        this.isBusy = false;
        this.networkErrorOnLoad = false;
    }

    fetchProjectMetadataSet(id: number) {
        this.isBusy = true;
        this.networkErrorOnLoad = false;
        
        ApiService
            .get("/ProjectMetadata/Show/" + id)
            .then((result: any) => {
                if(result == null || !result.id) {
                    throw new Error("Result was invalid");
                }
                this.isBusy = false;
                this.projectMetadataSet = this.getProjectMetadataSetModelFromJson(result);
            })
            .catch(error => {
                Logger.error(error);
                this.isBusy = false;
                this.networkErrorOnLoad = true;
            });
    }

    getProjectMetadataSetModelFromJson(json: any) {
        let model = new ShowProjectMetadataModel();
        model.fillFromJson(json);
        return model;
    }
}

export default new ShowProjectMetadataSetStore();