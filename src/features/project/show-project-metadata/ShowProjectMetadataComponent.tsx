import * as React from "react";
import { observer } from "mobx-react";
import ShowProjectMetadataStore from "./ShowProjectMetadataStore";
import DeleteProjectMetadataStore from "../delete-project-metadata/DeleteProjectMetadataStore";
import { RouteComponentProps } from "react-router";
import { Button, Container, Segment, Message, Header, Grid, Icon } from "semantic-ui-react";
import { SaliModal } from "../../../common";
import LabelValuePair from "../../../common/LabelValuePair";
import NetworkError from "../../../common/NetworkError";
import UserStore from "../../../stores/UserStore";
import * as Activities from "../../../constants/Activities";
import SaliNavigation from "../../../common/ui/SaliNavigation";

@observer
export default class ShowProjectMetadataSetComponent extends React.Component<RouteComponentProps<any>, {}> {
    constructor(props: any) {
        super(props);
        this.handleEdit = this.handleEdit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    componentDidMount() { 
        ShowProjectMetadataStore.fetchProjectMetadataSet(this.props.match.params.projectMetadataId);
    }

    tempMaybe(val: any, prop?: string): any {
        if (val && prop) { return val[prop]; }
        if (val) { return val; }
        return null;
    }

    handleDelete = (projectMetadataId: number, e: React.MouseEvent<HTMLButtonElement>) => {
        e.preventDefault();
        DeleteProjectMetadataStore.openDeleteDialogue(() => {
            DeleteProjectMetadataStore
                .delete(projectMetadataId)
                .then(success => success && this.props.history.push(SaliNavigation.LINKS_REGISTERED.HOME.LINK.GET_LINK()));
        });
    }

    handleEdit = (projectMetadataId: number, e: React.MouseEvent<HTMLButtonElement>) => {
        e.preventDefault();
        this.props.history.push(SaliNavigation.LINKS_REGISTERED.HOME.LINK.GET_LINK() + "/" + projectMetadataId);
    }

    renderContent = () => {
        const { projectMetadataSet } = ShowProjectMetadataStore;

        if (ShowProjectMetadataStore.isBusy) {
            return (
                <Header as="h2" icon={true} textAlign="center">
                    <Icon name="circle notched" circular={true} loading={true} />
                    <Header.Subheader>
                        Loading project metadata set...
                    </Header.Subheader>
                </Header>
            );
        }

        return (
            <Grid divided="vertically">
                <Grid.Row columns={3}>
                    <Grid.Column>
                        <LabelValuePair
                            label={"NE Bounds - (Latitude, Longitude)"}
                            value={projectMetadataSet.neLatitude + ", " + projectMetadataSet.neLongitude}
                        />
                        <LabelValuePair
                            label={"SW Bounds - (Latitude, Longitude)"}
                            value={projectMetadataSet.swLatitude + ", " + projectMetadataSet.swLongitude}
                        />
                        <LabelValuePair
                            label={"Scale"}
                            value={projectMetadataSet.scale}
                        />
                        <LabelValuePair
                            label={"Survey Type"}
                            value={projectMetadataSet.surveyType}
                        />
                        <LabelValuePair
                            label={"Project Manager"}
                            value={projectMetadataSet.projectManager}
                        />
                        <LabelValuePair
                            label={"Project Information Manager"}
                            value={projectMetadataSet.informationManager}
                        />
                    </Grid.Column>
                    <Grid.Column>
                        <LabelValuePair
                            label={"Project Start Date"}
                            value={projectMetadataSet.projectStartDate}
                        />
                        <LabelValuePair
                            label={"Project End Date"}
                            value={projectMetadataSet.projectEndDate}
                        />
                        <LabelValuePair
                            label={"Metadata Date Created"}
                            value={projectMetadataSet.metadataCreatedDate}
                        />
                        <LabelValuePair
                            label={"Metadata Date Updated"}
                            value={projectMetadataSet.metadataUpdatedDate}
                        />
                        <LabelValuePair
                            label={"Project Overview"}
                            value={projectMetadataSet.projectOverview}
                        />
                        <LabelValuePair
                            label={"Comments"}
                            value={projectMetadataSet.comments}
                        />
                    </Grid.Column>

                    <Grid.Column>
                        {/* <List> */}
                            <LabelValuePair
                                label={"Organisation"}
                                value={projectMetadataSet.organisation}
                            />
                            <LabelValuePair
                                label={"Metadata Organisation"}
                                value={projectMetadataSet.metadataName}
                            />
                            <LabelValuePair
                                label={"Metadata Contact Position"}
                                value={projectMetadataSet.metadataContactPosition}
                            />
                            <LabelValuePair
                                label={"Email"}
                                value={projectMetadataSet.email}
                            />
                            <LabelValuePair
                                label={"Telephone number"}
                                value={projectMetadataSet.telephone}
                            />
                            <LabelValuePair
                                label={"Custodian Organisation"}
                                value={projectMetadataSet.custodianName}
                            />
                            <LabelValuePair
                                label={"Custodian Contact Position"}
                                value={projectMetadataSet.custodianContactPosition}
                            />
                        {/* </List> */}

                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }

    render() {
        const { projectMetadataSet, networkErrorOnLoad } = ShowProjectMetadataStore;

        if(networkErrorOnLoad) {
            return <NetworkError />;
        }

        return (
            <Container fluid={true} className="js-show-project-metadata-component">
                <Message
                    icon="tasks"
                    header={"View Project Metadata " + this.tempMaybe(projectMetadataSet.saliProject, "code")}
                    attached={true}
                />

                <Segment attached={true}>

                    <div style={{paddingLeft: "24px"}}>
                        {/* Header */}
                        <Header as="h2" style={{marginTop: "24px"}}>
                            Project Metadata Set
                            <Header.Subheader>
                                {this.tempMaybe(projectMetadataSet.saliProject, "code")}
                            </Header.Subheader>
                        </Header>

                        <div style={{paddingLeft: "24px", paddingRight: "200px"}}>
                            {/* Content */}
                            {this.renderContent()}
                        </div>

                    </div>
                </Segment>
                <Segment secondary={true} attached="bottom" clearing={true}>
                    <Button.Group floated="right">
                        {UserStore.isAuthorized(Activities.deleteProjectMetadata) && (
                            <Button
                                color="red"
                                content="Delete"
                                onClick={this.handleDelete.bind(null, projectMetadataSet.id)}
                            />
                        )}
                        {UserStore.isAuthorized(Activities.editProjectMetadata) && (
                            <Button
                                color="blue"
                                content="Edit"
                                onClick={this.handleEdit.bind(null, projectMetadataSet.id)}
                            />
                        )}
                    </Button.Group>
                </Segment>
                <SaliModal
                    {...SaliModal.config([
                        { propertyName: "modalOptions", context: DeleteProjectMetadataStore }
                    ])}
                />
            </Container>
        );
    }
}