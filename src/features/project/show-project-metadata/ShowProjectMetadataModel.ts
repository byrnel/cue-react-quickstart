import { observable } from "mobx";
import * as moment from "moment";
import { Project } from "../../../domain";

export default class ShowProjectMetadataSetModel {
    @observable id?: number;
    @observable saliProject?: Project;
    @observable projectStartDate?: string;
    @observable projectEndDate?: string;
    @observable neLatitude?: number;
    @observable neLongitude?: number;
    @observable swLatitude?: number;
    @observable swLongitude?: number;
    @observable scale?: number;
    @observable surveyType?: string;
    @observable projectManager?: string;
    @observable informationManager?: string;
    @observable projectOverview?: string;
    @observable comments?: string;
    @observable metadataCreatedDate?: string;
    @observable metadataUpdatedDate?: string;

    @observable organisation: string;
    @observable metadataName?: string;
    @observable metadataContactPosition?: string;
    @observable email?: string;
    @observable telephone?: string;
    @observable custodianName?: string;
    @observable custodianContactPosition?: string;

    reset() {
        // todo: ^
        // sort this out with a `?` so we can reset it or just leave it out

        this.id = undefined;
        this.saliProject = undefined;
        this.projectStartDate = undefined;
        this.projectEndDate = undefined;
        this.neLatitude = undefined;
        this.swLatitude = undefined;
        this.swLongitude = undefined;
        this.scale = undefined;
        this.surveyType = "";
        this.projectManager = "";
        this.informationManager = "";
        this.projectOverview = "";
        this.comments = "";
        this.metadataCreatedDate = undefined;
        this.metadataUpdatedDate = undefined;
        this.organisation = "";
        this.metadataName = "";
        this.metadataContactPosition = "";
        this.email = "";
        this.telephone = "";
        this.custodianName= "";
        this.custodianContactPosition = "";
    }

    fillFromJson(json: any) {
        this.reset();
        this.id = json.id;
        this.saliProject = json.project;
        this.projectStartDate = moment.utc(json.projectStartDate).format("MMMM Do YYYY");
        this.projectEndDate = json.projectEndDate ? moment.utc(json.projectEndDate).format("MMMM Do YYYY") : "In progress";
        this.neLatitude = json.neLatitude;
        this.neLongitude = json.neLongitude;
        this.swLatitude = json.swLatitude;
        this.swLongitude = json.swLongitude;
        this.scale = json.scale;
        this.surveyType = json.surveyType;
        this.projectManager = json.projectManager;
        this.informationManager = json.informationManager;
        this.projectOverview = json.projectOverview;
        this.comments = json.comments;
        this.metadataCreatedDate = moment(json.metadataCreatedDate).format("MMMM Do YYYY");
        this.metadataUpdatedDate = moment(json.metadataUpdatedDate).format("MMMM Do YYYY");

        this.organisation = json.organisationName;
        this.metadataName = json.metadataName;
        this.metadataContactPosition = json.metadataPosition;
        this.email = json.email;
        this.telephone = json.phone;
        this.custodianName = json.custodianName;
        this.custodianContactPosition = json.custodianPosition;

    }
}