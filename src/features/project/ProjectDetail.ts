import { Moment } from "moment";

class Boundary {
    neLatitude: number;
    neLongitude: number;
    swLatitude: number;
    swLongitude: number;
}

export default class ProjectDetail {
    name: string;
    code: string;
    startDate: Moment;
    finishDate: Moment;
    boundary: Boundary;
}