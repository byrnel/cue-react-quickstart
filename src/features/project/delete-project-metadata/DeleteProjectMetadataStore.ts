import { observable } from "mobx";
import { ISaliModalProps } from "../../../common/SaliModal";
import { ApiService, Logger } from "../../../services/";
import * as Toasts from "../../../common/Toasts";

class DeleteProjectMetadataStore {
    @observable isDeleting: boolean = false;
    @observable deletingProjectMetadataId?: number;

    // Modal
    @observable modalOptions?: ISaliModalProps;
    @observable comment?: string;
    @observable errors?: IErrors;

    reset() {
        this.isDeleting = false;
        this.deletingProjectMetadataId = undefined;
    }

    delete(projectMetadataid: number): Promise<boolean> {
        this.isDeleting = true;
        this.deletingProjectMetadataId = projectMetadataid;

        return ApiService
            .post("/projectMetadata/delete/" + projectMetadataid)
            .then((result) => {
                if(result == null) {
                    throw new Error("Result was null");
                }

                this.isDeleting = false;
                this.deletingProjectMetadataId = undefined;
                return true;
            })
            .catch(error => {
                Logger.error(error);
                this.isDeleting = false;
                this.deletingProjectMetadataId = undefined;
                Toasts.networkError();
                return false;
            });
    }

    openModal = (options: ISaliModalProps) => {
        this.modalOptions = options;
    }

    closeModal = () => {
        this.modalOptions = undefined;
    }

    openDeleteDialogue = (executeCommand: () => void) => {
        this.openModal({
            header: "Delete Project Metadata?",
            positiveText: "Delete",
            positiveColor: "red",
            negativeText: "Cancel",
            onPositiveClick: () => {
                executeCommand();
                this.closeModal();
            },
            onClose: this.closeModal
        });
    }
}

export default new DeleteProjectMetadataStore;