import { observable } from "mobx";
import { Moment } from "moment";
import { SurveyType, Project, Contact} from "../../domain/";
import Lookup from "../../domain/Lookup";

export default class ProjectMetadata {
    @observable id: number;
    @observable saliProject: Project;
    @observable projectStartDate: Moment;
    @observable projectEndDate?: Moment;
    @observable neLatitude: number;
    @observable neLongitude: number;
    @observable swLatitude: number;
    @observable swLongitude: number;
    @observable scale?: number;
    @observable surveyType: SurveyType | null;

    @observable projectManager: Contact;
    @observable informationManager: Contact;
    @observable organisation: Lookup;

    @observable projectOverview: string;
    @observable comments: string | null;
    @observable metadataCreatedDate: Moment;
    @observable metadataUpdatedDate: Moment;
}