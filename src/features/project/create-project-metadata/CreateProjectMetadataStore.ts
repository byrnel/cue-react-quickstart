import { observable } from "mobx";
import ProjectMetadata from "../ProjectMetadata";
import { Project, Contact } from "../../../domain";
import { ApiService, Logger } from "../../../services";
import ProjectDetail from "../ProjectDetail";
import * as moment from "moment";
import SurveyType from "../../../domain/SurveyType";
import * as SaliRole from "../../../constants/SaliRole";
import Lookup from "../../../domain/Lookup";
import * as Toasts from "../../../common/Toasts";

class CreateProjectMetadataStore {
    @observable projectMetadata: ProjectMetadata = new ProjectMetadata();
    @observable projectOptions: Array<Project> = [];
    @observable contactOptions: Array<Contact> = [];
    @observable projectManagerOptions: Array<Contact> = [];
    @observable infoManagerOptions: Array<Contact> = [];
    @observable organisationOptions: Array<Lookup> = [];
    @observable surveyTypeOptions: Array<SurveyType> = [];

    // Misc / UI concerns
    @observable isLoading: boolean = false;
    @observable isSaving: boolean = false;
    @observable networkErrorOnLoad: boolean = false;
    @observable errors?: IErrors;

    // Upload progress
    @observable isUploading: boolean = true;
    @observable projectMetadataId: number | null = null;

    reset() {
        this.projectMetadata = new ProjectMetadata();
        this.isLoading = false;
        this.isSaving = false;
        this.networkErrorOnLoad = false;
        this.errors = undefined;
        this.projectMetadataId = null;
        this.projectOptions = [];
        this.contactOptions = [];
        this.surveyTypeOptions = [];
        this.projectManagerOptions = [];
        this.infoManagerOptions = [];
        this.organisationOptions = [];
    }

    submitMetadata(): Promise<number | undefined> {
        const pm = this.projectMetadata;
        const model = {
            SALIProjectCode: pm.saliProject != null ? pm.saliProject.code : null,
            SALIProjectName: pm.saliProject != null ? pm.saliProject.name : null,
            NELatitude: pm.neLatitude,
            NELongitude: pm.neLongitude,
            SWLatitude: pm.swLatitude,
            SWLongitude: pm.swLongitude,
            Scale: pm.scale,
            SurveyType: pm.surveyType != null ? pm.surveyType.code : null,
            ProjectManager: pm.projectManager != null ? pm.projectManager.userId : null,
            ProjectInformationManager: pm.informationManager != null ? pm.informationManager.userId : null,
            Organisation: pm.organisation != null ? pm.organisation.code : "",
            ProjectOverview: pm.projectOverview,
            Comments: pm.comments,
            SALIProjectStartDate: pm.projectStartDate,
            SALIProjectFinishDate: pm.projectEndDate
        };

        this.errors = undefined;
        this.isSaving = true;

        return ApiService
            .post("/ProjectMetadata", model)
            .then((result:any) => {
                this.isSaving = false;

                if(result.id) {
                    return result.id;
                } else {
                    this.errors = <IErrors> result;
                    return null;
                }
            })
            .catch(error => {
                Logger.error(error);
                this.isSaving = false;
                Toasts.networkError();
                return null;
            });
    }

    getLookupData() {
        this.isLoading = true;
        this.networkErrorOnLoad = false;
        return ApiService
            .get("/ProjectMetadata/lookupdata")
            .then((data: any) => {
                this.projectOptions = data.projects;
                this.contactOptions = data.contacts.map(Contact.mapJsonToContact);
                this.surveyTypeOptions = data.surveyTypes.map(SurveyType.fromJson);
                this.organisationOptions = data.organisations.map(Lookup.fromJson);
                this.projectMetadata.metadataCreatedDate = moment();
                this.projectMetadata.metadataUpdatedDate = moment();
                this.isLoading = false;
                this.networkErrorOnLoad = false;
            })
            .catch(error => {
                Logger.error(error);
                this.isLoading = false;
                this.networkErrorOnLoad = true;
            });
    }

    getProjectDetails(projectCode: string) {
        let code = projectCode != null ? { projectCode: projectCode } : undefined;
        return ApiService
            .get("/ProjectMetadata/getProjectDetails", code)
            .then((data: ProjectDetail) => {
                this.projectMetadata.projectStartDate = moment(data.startDate);
                this.projectMetadata.projectEndDate = data.finishDate ? moment(data.finishDate) : undefined;
                this.projectMetadata.neLatitude = data.boundary.neLatitude;
                this.projectMetadata.neLongitude = data.boundary.neLongitude;
                this.projectMetadata.swLatitude = data.boundary.swLatitude;
                this.projectMetadata.swLongitude = data.boundary.swLongitude;
                this.isLoading = false;
            })
            .catch(error => {
                Logger.error(error);
                this.isLoading = false;
                Toasts.networkError();
            });
    }

    filterContactList() {
        this.projectManagerOptions = this.projectMetadata.saliProject.code != null
            ? this.contactOptions.filter((x: Contact) => x.projectCode.localeCompare(this.projectMetadata.saliProject.code) === 0 &&
                                                         !x.endDate &&
                                                         x.role === SaliRole.projectManagerRole)
            : [];
        this.infoManagerOptions = this.projectMetadata.saliProject.code != null
            ? this.contactOptions.filter((x: Contact) => x.projectCode.localeCompare(this.projectMetadata.saliProject.code) === 0 &&
                                                         !x.endDate &&
                                                         x.role === SaliRole.infoManagerRole &&
                                                         x.userId !== SaliRole.saliId)
            : [];
    }
}

export default new CreateProjectMetadataStore();