import * as React from "react";
import { observer } from "mobx-react";
import { RouteComponentProps } from "react-router";
import { Container, Button, Form, Segment, Header, Dimmer, Loader, Grid } from "semantic-ui-react";
import { DateInput, BindConfig, EfficientSelectInput, ErrorList } from "../../../common";
import { CreateProjectMetadataStore } from "./";
import { Project, Contact, SurveyType } from "../../../domain";
import SaliNavigation from "../../../common/ui/SaliNavigation";
import Lookup from "../../../domain/Lookup";
import { SaliHeader } from "../../../common/sali-page";
// import NetworkError from "../../../common/NetworkError";

@observer
export default class CreateProjectMetadataComponent extends React.Component<RouteComponentProps<{}>, {}> {
    constructor(props: any) {
        super(props);
        this.handleSubmitAndRedirect = this.handleSubmitAndRedirect.bind(this);
    }

    componentDidMount() {
        CreateProjectMetadataStore.reset();
        CreateProjectMetadataStore.getLookupData();
    }

    handleSubmitAndRedirect = (e: React.MouseEvent<HTMLButtonElement>) => {
        e.preventDefault();
        CreateProjectMetadataStore
            .submitMetadata()
            .then((resultId?: number) => {
                if(resultId != null) {
                    this.routeToShowMetadata(resultId);
                }
            });
    }

    routeToShowMetadata = (id: number) => {
        this.props.history.push(SaliNavigation.LINKS_REGISTERED.MANAGE.BASE.GET_LINK() + "/" + id);
    }

    renderLoader() {
        return (
            <Dimmer active={true} inverted={true}>
                <Loader active={true} inline="centered">Loading</Loader>
            </Dimmer>
        );
    }

    render() {
        const { projectMetadata } = CreateProjectMetadataStore;

        // if (CreateProjectMetadataStore.networkErrorOnLoad) {
        //     return <NetworkError />;
        // }

        // if (CreateProjectMetadataStore.isLoading) {
        //     return this.renderLoader();
        // }

        // This function fixes warnings in the browser that were caused when properties in the store get set to "undefined"
        // and then bound to Form Inputs.
        // The warning gets thrown when the input gets populated with a truthy string, and the component
        // is changed from uncontrolled to controlled (bad juju in React).
        const bind = (context: string) => {
            let binding = BindConfig({
                context: projectMetadata,
                errors: CreateProjectMetadataStore.errors
            })(context);
            binding.value = binding.value != null ? binding.value : "";
            return binding;
        };

        // This section fixes warnings thrown by the dropdown elements after 'bind' was changed
        const bindList = BindConfig({
            context: projectMetadata,
            errors: CreateProjectMetadataStore.errors
        });

        const projectOptionsFunc = (projects: Array<Project>) => projects.map(project =>
            ({ text: project.code, value: project.code, "data-entity": project })
        );

        const surveyTypeOptionsFunc = (surveyTypes: Array<SurveyType>) => surveyTypes.map(surveyType =>
            ({ text: surveyType.name, value: surveyType.code, "data-entity": surveyType })
        );

        const lookupOptionsFunc = (lookups: Array<Lookup>) => lookups.map(lookup =>
            ({ text: lookup.name, value: lookup.code, "data-entity": lookup })
        );

        const contactOptionsFunc = (contactsArray: Array<Contact>) => contactsArray.map((contact, index) =>
            ({
                text: contact.firstName + " " + contact.lastName,
                value: contact.userId,
                "data-entity": contact,
                key: index
            })
        );

        return (
            <Container fluid={true} className="js-create-project-component">
                <Form>
                    <SaliHeader
                        header="Add Project Metadata"
                        icon="cube"
                        attached={true}
                        isLoading={CreateProjectMetadataStore.isLoading}
                    />

                    {CreateProjectMetadataStore.errors && (
                        <Segment attached={true}>
                            <Grid.Row>
                                <Grid.Column>
                                    <ErrorList errors={CreateProjectMetadataStore.errors} />
                                </Grid.Column>
                            </Grid.Row>
                        </Segment>
                    )}

                    <Segment attached={true}>
                        <Header as="h2">
                                <Header.Subheader>
                                Metadata for a Project level spatial data grouping
                                </Header.Subheader>
                        </Header>
                    </Segment>
                    <Segment secondary={true} attached={true}>
                        <Grid stackable={true} columns="equal" divided={true}>
                            <Grid.Row columns="equal">
                                <Grid.Column width="10">
                                    <Segment attached={true}>

                                        <EfficientSelectInput
                                            {...bindList("saliProject", () => {
                                                CreateProjectMetadataStore.filterContactList();
                                                CreateProjectMetadataStore.getProjectDetails(projectMetadata.saliProject.code);
                                            })}
                                            label="SALI Project"
                                            entities={CreateProjectMetadataStore.projectOptions}
                                            optionsFunc={projectOptionsFunc}
                                            required={true}
                                            withBlankOption={false}
                                            className="js-project-input"
                                        />
                                        <Form.Input {...bind("neLatitude")} required={true}  readOnly={true} label="North East Boundary - Latitude" />
                                        <Form.Input {...bind("neLongitude")} required={true} readOnly={true} label="North East Boundary - Longitude" />
                                        <Form.Input {...bind("swLatitude")} required={true} readOnly={true} label="South West Boundary - Latitude" />
                                        <Form.Input {...bind("swLongitude")} required={true} readOnly={true} label="South West Boundary - Longitude" />
                                        <Form.Input
                                            required={true}
                                            type="tel"
                                            value={projectMetadata.scale || ""}
                                            onChange={(e, data) => {
                                                if (data.value != null && data.value.match(/^\d{0,15}$/)) {
                                                    projectMetadata.scale = parseInt(data.value, 10);
                                                } else if (data.value === "") {
                                                    projectMetadata.scale = undefined;
                                                }
                                            }}
                                            label="Scale"
                                            className="js-scale-input"
                                        />
                                        <EfficientSelectInput
                                            {...bindList("surveyType")}
                                            required={false}
                                            entities={CreateProjectMetadataStore.surveyTypeOptions}
                                            optionsFunc={surveyTypeOptionsFunc}
                                            disabled={CreateProjectMetadataStore.surveyTypeOptions.length === 0}
                                            label="Survey Type"
                                            className="js-survey-type-input"
                                        />
                                        <EfficientSelectInput
                                            {...bindList("projectManager")}
                                            required={true}
                                            entities={CreateProjectMetadataStore.projectManagerOptions}
                                            optionsFunc={contactOptionsFunc}
                                            disabled={CreateProjectMetadataStore.projectManagerOptions.length === 0}
                                            label="Project Manager"
                                            className="js-project-manager-input"
                                        />
                                        <EfficientSelectInput
                                            {...bindList("informationManager")}
                                            required={true}
                                            entities={CreateProjectMetadataStore.infoManagerOptions}
                                            optionsFunc={contactOptionsFunc}
                                            disabled={CreateProjectMetadataStore.infoManagerOptions.length === 0}
                                            label="Project Information Manager"
                                            className="js-project-information-manager-input"
                                        />
                                        <EfficientSelectInput
                                            {...bindList("organisation")}
                                            required={true}
                                            entities={CreateProjectMetadataStore.organisationOptions}
                                            optionsFunc={lookupOptionsFunc}
                                            disabled={CreateProjectMetadataStore.organisationOptions.length === 0}
                                            label="Organisation"
                                            className="js-organisation-input"
                                        />
                                        <Form.TextArea {...bind("projectOverview")} required={true} label="Project Overview" className="js-project-overview-input" />
                                        <Form.TextArea {...bind("comments")} label="Comments" className="js-comments-input" />
                                    </Segment>
                                </Grid.Column>
                                <Grid.Column>
                                    <Segment attached={true}>
                                        <DateInput disabled={true} required={true} label="Project Start Date" {...bind("projectStartDate")} />
                                        <DateInput disabled={true} label="Project End Date" {...bind("projectEndDate")} />
                                        <DateInput disabled={true} required={true} label="Metadata Date Created" {...bind("metadataCreatedDate")} />
                                        <DateInput disabled={true} required={true} label="Metadata Date Updated" {...bind("metadataUpdatedDate")} />
                                    </Segment>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Segment>

                    <Segment secondary={true} attached="bottom" clearing={true}>
                        <Button.Group floated="right">
                            <Button
                                icon="send outline"
                                color="blue"
                                content="Create"
                                disabled={CreateProjectMetadataStore.isSaving}
                                loading={CreateProjectMetadataStore.isSaving}
                                onClick={this.handleSubmitAndRedirect}
                                className="js-project-create-button"
                            />
                        </Button.Group>
                    </Segment>
                </Form>
            </Container>
        );
    }
}
