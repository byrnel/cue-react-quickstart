import { observable } from "mobx";
import * as moment from "moment";
import { Contact, SurveyType } from "../../../domain";
import Lookup from "../../../domain/Lookup";

export default class EditProjectMetadataModel {
    @observable id: number;
    @observable saliProject: string;
    @observable projectStartDate: moment.Moment;
    @observable projectEndDate?: moment.Moment;
    @observable neLatitude: number;
    @observable neLongitude: number;
    @observable swLatitude: number;
    @observable swLongitude: number;
    @observable scale?: number;
    @observable surveyType?: SurveyType;

    @observable projectManager: Contact;
    @observable informationManager: Contact;
    @observable organisation: Lookup;

    @observable projectOverview: string;
    @observable comments: string | null;
    @observable metadataCreatedDate: moment.Moment;
    @observable metadataUpdatedDate: moment.Moment;

    static fromJson(json: any) {
        const x = new EditProjectMetadataModel();
        x.id = json.id;
        x.saliProject = json.saliProject.code;
        x.projectStartDate = moment.utc(json.startDate);
        x.projectEndDate = json.finishDate ? moment.utc(json.finishDate) : undefined;
        x.neLatitude = json.boundary.neLatitude;
        x.neLongitude = json.boundary.neLongitude;
        x.swLatitude = json.boundary.swLatitude;
        x.swLongitude = json.boundary.swLongitude;
        x.scale = json.scale;
        x.surveyType = json.surveyType ? SurveyType.fromJson(json.surveyType) : undefined;
        x.projectManager = json.projectManager;
        x.informationManager = json.projectInformationManager;
        x.organisation = json.organisation;
        x.projectOverview = json.projectOverview;
        x.comments = json.comments;
        x.metadataCreatedDate = moment(json.metadataCreatedDate);
        x.metadataUpdatedDate = moment(json.metadataUpdatedDate);
        return x;
    }
}