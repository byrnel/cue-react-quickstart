import { observable } from "mobx";
import { Contact, Project, SurveyType, Lookup } from "../../../domain";
import EditProjectMetadataModel from "./EditProjectMetadataModel";
import { ApiService, Logger } from "../../../services";
import * as SaliRole from "../../../constants/SaliRole";
import * as moment from "moment";
import * as Toasts from "../../../common/Toasts";

class EditProjectMetadataStore {
    @observable projectMetadata: EditProjectMetadataModel = new EditProjectMetadataModel();
    @observable projectOptions: Array<Project> = [];
    @observable contactOptions: Array<Contact> = [];
    @observable projectManagerOptions: Array<Contact> = [];
    @observable infoManagerOptions: Array<Contact> = [];
    @observable surveyTypeOptions: Array<SurveyType> = [];
    @observable organisationOptions: Array<Lookup> = [];

    // Misc / UI concerns
    @observable isLoading: boolean = false;
    @observable isSaving: boolean = false;
    @observable networkErrorOnLoad: boolean = false;
    @observable errors?: IErrors;

    // Upload progress
    @observable isUploading: boolean = true;

    @observable projectMetadataId: number | null = null;

    reset() {
        this.projectMetadata = new EditProjectMetadataModel();
        this.isLoading = false;
        this.isSaving = false;
        this.networkErrorOnLoad = false;
        this.errors = undefined;
        this.projectMetadataId = null;
        this.projectOptions = [];
        this.contactOptions = [];
        this.surveyTypeOptions= [];
        this.projectManagerOptions = [];
        this.infoManagerOptions = [];
        this.organisationOptions = [];
    }

    submitMetadata(): Promise<number | undefined> {
        const pm = this.projectMetadata;
        const model = {
            Id: pm.id,
            SALIProjectCode: pm.saliProject != null ? pm.saliProject : null,
            NELatitude: pm.neLatitude,
            NELongitude: pm.neLongitude,
            SWLatitude: pm.swLatitude,
            SWLongitude: pm.swLongitude,
            Scale: pm.scale,
            SurveyType: pm.surveyType != null ? pm.surveyType.code : null,
            ProjectManager: pm.projectManager != null ? pm.projectManager.userId : null,
            ProjectInformationManager: pm.informationManager != null ? pm.informationManager.userId : null,
            Organisation: pm.organisation != null ? pm.organisation.code : "",
            ProjectOverview: pm.projectOverview,
            Comments: pm.comments,
            SALIProjectStartDate: pm.projectStartDate,
            SALIProjectFinishDate: pm.projectEndDate
        };

        this.errors = undefined;
        this.isSaving = true;

        return ApiService
            .post("/ProjectMetadata/edit", model)
            .then((result:any) => {
                this.isSaving = false;

                if(result.id) {
                    return result.id;
                } else {
                    this.errors = <IErrors> result;
                    return null;
                }
            })
            .catch(error => {
                Logger.error(error);
                this.isSaving = false;
                Toasts.networkError();
            });
    }

    getProjectMetadata(projectMetadataId: number) {
        this.isLoading = true;
        return ApiService
            .get("/ProjectMetadata/edit/" + projectMetadataId)
            .then((data: any) => {
                this.projectMetadata = EditProjectMetadataModel.fromJson(data.projectMetadata);
                this.contactOptions = data.lookups.contacts.map(this.mapJsonToContact);
                this.surveyTypeOptions = data.lookups.surveyTypes.map(SurveyType.fromJson);
                this.organisationOptions = data.lookups.organisations.map(Lookup.fromJson);
                this.projectMetadata.metadataUpdatedDate = moment();
                this.isLoading = false;
                this.networkErrorOnLoad = false;
            })
            .then(() => {
                this.filterContactList();
            })
            .catch(error => {
                Logger.error(error);
                this.isLoading = false;
                this.networkErrorOnLoad = true;
            });
    }

    mapJsonToContact = (x: any): Contact => ({
        projectCode: x.saliProjectCode,
        userId: x.userId,
        role: x.role,
        endDate: x.endDate != null ? x.endDate : null,
        firstName: x.firstName,
        lastName: x.lastName
    })

    filterContactList() {
        this.projectManagerOptions = this.projectMetadata.saliProject != null
            ? this.contactOptions.filter((x: Contact) => x.projectCode.localeCompare(this.projectMetadata.saliProject) === 0 &&
                                                         !x.endDate &&
                                                         x.role === SaliRole.projectManagerRole)
            : [];
        this.infoManagerOptions = this.projectMetadata.saliProject != null
            ? this.contactOptions.filter((x: Contact) => x.projectCode.localeCompare(this.projectMetadata.saliProject) === 0 &&
                                                         !x.endDate &&
                                                         x.role === SaliRole.infoManagerRole &&
                                                         x.userId !== SaliRole.saliId)
            : [];
    }
}

export default new EditProjectMetadataStore();