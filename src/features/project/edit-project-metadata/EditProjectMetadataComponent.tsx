import * as React from "react";
import { RouteComponentProps } from "react-router";
import { observer } from "mobx-react";
import { Container, Button, Dimmer, Form, Header, Loader, Message, Segment, Grid, Portal } from "semantic-ui-react";
import EditProjectMetadataStore from "./EditProjectMetadataStore";
import { BindConfig, DateInput, EfficientSelectInput, ErrorList } from "../../../common";
import { Contact, SurveyType, Lookup } from "../../../domain";
import SaliNavigation from "../../../common/ui/SaliNavigation";
import NetworkError from "../../../common/NetworkError";

interface IEditProjectMetadataState {
    portalOpen: boolean;
}

@observer
export default class EditProjectMetadataComponent extends React.Component<RouteComponentProps<any>, IEditProjectMetadataState> {
    constructor(props: RouteComponentProps<any>) {
        super(props);
        this.state = {
            portalOpen: false
        };
        this.handleSubmitAndRedirect = this.handleSubmitAndRedirect.bind(this);
    }

    componentDidMount() {
        EditProjectMetadataStore.reset();
        EditProjectMetadataStore.getProjectMetadata(this.props.match.params.projectMetadataId);
    }

    handleSubmitAndRedirect = (e: React.MouseEvent<HTMLButtonElement>) => {
        e.preventDefault();
        EditProjectMetadataStore
            .submitMetadata()
            .then((result: number) => {
                this.setState({
                    portalOpen: true
                });
                setTimeout(this.handlePortalClose, 2000);
                if (EditProjectMetadataStore.errors === undefined) {
                    this.routeToShowMetadata(result);
                }
            });
    }

    handlePortalClose = () => {
        this.setState({
            portalOpen: false
        });
    }

    routeToShowMetadata = (id: number) => {
        this.props.history.push(SaliNavigation.LINKS_REGISTERED.MANAGE.LINKS.SECURITY.GROUPS.GET_LINK() + "/" + id);
    }

    renderLoader() {
        return (
            <Dimmer active={true} inverted={true}>
                <Loader active={true} inline="centered">Loading</Loader>
            </Dimmer>
        );
    }

    render() {
        const { projectMetadata, errors } = EditProjectMetadataStore;

        if (EditProjectMetadataStore.networkErrorOnLoad) {
            return <NetworkError />;
        }

        if (EditProjectMetadataStore.isLoading) {
            return this.renderLoader();
        }

        const bind = (context: string) => {
            let binding = BindConfig({
                context: projectMetadata,
                errors: errors
            })(context);
            binding.value = binding.value != null ? binding.value : "";
            return binding;
        };

        // This section fixes warnings thrown by the dropdown elements after 'bind' was changed
        const bindList = BindConfig({
            context: projectMetadata,
            errors: EditProjectMetadataStore.errors
        });

        const contactOptionsFunc = (contactsArray: Array<Contact>) => contactsArray.map((contact, index) =>
            ({
                text: contact.firstName + " " + contact.lastName,
                value: contact.userId,
                "data-entity": contact,
                key: index
            })
        );

        const surveyTypeOptionsFunc = (surveyTypes: Array<SurveyType>) => surveyTypes.map((surveyType, index) =>
            ({ text: surveyType.name, value: surveyType.code, "data-entity": surveyType, key: index })
        );

        const lookupsOptionsFunc = (lookups: Array<Lookup>) => lookups.map(lookup =>
            ({ text: lookup.name, value: lookup.code, "data-entity": lookup })
        );

        const {
            portalOpen
        } = this.state;
        const hasErrors = EditProjectMetadataStore.errors !== undefined;

        return (
            <Container fluid={true}>
                <Form>
                    <Message
                        icon="browser"
                        header="Edit Project Metadata"
                        attached={true}
                    />
                    {EditProjectMetadataStore.errors && (
                        <Segment attached={true}>
                            <Grid.Row>
                                <Grid.Column>
                                    <ErrorList errors={EditProjectMetadataStore.errors} />
                                </Grid.Column>
                            </Grid.Row>
                        </Segment>
                    )}

                    <Segment attached={true}>
                        <Header as="h2">
                                <Header.Subheader>
                                Metadata for a Project level spatial data grouping
                                </Header.Subheader>
                        </Header>
                    </Segment>
                    <Segment secondary={true} attached={true}>
                        <Grid stackable={true} columns="equal" divided={true}>
                            <Grid.Row columns="equal">
                                <Grid.Column width="10">
                                    <Segment attached={true}>
                                        <Form.Input {...bind("saliProject")} readOnly={true} label="SALI Project" />
                                        <Form.Input {...bind("neLatitude")} required={true} readOnly={true} label="North East Boundary - Latitude" />
                                        <Form.Input {...bind("neLongitude")} required={true} readOnly={true} label="North East Boundary - Longitude" />
                                        <Form.Input {...bind("swLatitude")} required={true} readOnly={true} label="South West Boundary - Latitude" />
                                        <Form.Input {...bind("swLongitude")} required={true} readOnly={true} label="South West Boundary - Longitude" />
                                        <Form.Input
                                            required={true}
                                            value={projectMetadata.scale || ""}
                                            onChange={(e, data) => {
                                                if (data.value != null && data.value.match(/^\d{0,15}$/)) {
                                                    projectMetadata.scale = parseInt(data.value, 10);
                                                } else if (data.value === "") {
                                                    projectMetadata.scale = undefined;
                                                }
                                            }}
                                            label="Scale"
                                        />
                                        <EfficientSelectInput
                                            {...bindList("surveyType")}
                                            required={false}
                                            entities={EditProjectMetadataStore.surveyTypeOptions}
                                            optionsFunc={surveyTypeOptionsFunc}
                                            disabled={EditProjectMetadataStore.surveyTypeOptions.length === 0}
                                            label="Survey Type"
                                        />

                                        {/* TODO: turn these to dropdowns or custom Contact pickers */}
                                        <EfficientSelectInput
                                            {...bindList("projectManager")}
                                            required={true}
                                            entities={EditProjectMetadataStore.projectManagerOptions}
                                            optionsFunc={contactOptionsFunc}
                                            disabled={EditProjectMetadataStore.projectManagerOptions.length === 0}
                                            label="Project Manager"
                                        />
                                        <EfficientSelectInput
                                            {...bindList("informationManager")}
                                            required={true}
                                            entities={EditProjectMetadataStore.infoManagerOptions}
                                            optionsFunc={contactOptionsFunc}
                                            disabled={EditProjectMetadataStore.infoManagerOptions.length === 0}
                                            label="Project Information Manager"
                                        />
                                        <EfficientSelectInput
                                            {...bindList("organisation")}
                                            required={true}
                                            entities={EditProjectMetadataStore.organisationOptions}
                                            optionsFunc={lookupsOptionsFunc}
                                            disabled={EditProjectMetadataStore.organisationOptions.length === 0}
                                            label="Organisation"
                                        />
                                        <Form.TextArea {...bind("projectOverview")} required={true} label="Project Overview" />
                                        <Form.TextArea {...bind("comments")} label="Comments" />
                                    </Segment>
                                </Grid.Column>
                                <Grid.Column>
                                    <Segment attached={true}>
                                        <DateInput disabled={true} required={true} label="Project Start Date" {...bind("projectStartDate")} />
                                        <DateInput disabled={true} label="Project End Date" {...bind("projectEndDate")} />
                                        <DateInput disabled={true} required={true} label="Metadata Date Created" {...bind("metadataCreatedDate")} />
                                        <DateInput disabled={true} required={true} label="Metadata Date Updated" {...bind("metadataUpdatedDate")} />
                                    </Segment>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Segment>

                    <Segment secondary={true} attached="bottom" clearing={true}>
                        <Button.Group floated="right">
                            <Button
                                icon="disk outline"
                                color="green"
                                content="Save"
                                disabled={EditProjectMetadataStore.isSaving}
                                loading={EditProjectMetadataStore.isSaving}
                                onClick={this.handleSubmitAndRedirect}
                            />
                        </Button.Group>
                    </Segment>
                </Form>
                <Portal
                    open={portalOpen}
                    onClose={this.handlePortalClose}
                >
                    <Segment style={{ top: "5%", right: "5%", position: "fixed", zIndex: 1000 }}>
                        <Message
                            icon={hasErrors ? "x" : "check"}
                            content={hasErrors ? "Something went wrong" : "Project Metadata Created"}
                            color={hasErrors ? "red" : "green"}
                        />
                    </Segment>
                </Portal>
            </Container>
        );
    }
}