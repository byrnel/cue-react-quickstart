import * as React from "react";
import * as ReactDOM from "react-dom";
import App from "./App";
import "./index.css";
import registerServiceWorker from "./registerServiceWorker";
require("./common/ui/cui.css"); // Consistent UI Settings
require("./common/ui/sui.css"); // Semantic UI enhancements.
import { Logger } from "./services";

window.onerror = (msg: string, url: string, lineNo: number, columnNo: number, error: any) => {
  Logger.error(error);
  return false;
};

ReactDOM.render(
  <App />,
  document.getElementById("root") as HTMLElement
);
registerServiceWorker();
