import * as A from "../services/A";

export default class Lookup {
    id: number;
    code: string;
    name: string;

    static fromJson(json: any) {
        const lookup = new Lookup();
        lookup.id = A.number(json.id);
        lookup.name = A.string(json.name);
        lookup.code = A.string(json.code);
        return lookup;
    }
}