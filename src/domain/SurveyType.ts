import * as A from "../services/A";

export default class SurveyType {
    name: string;
    code: string;

    static fromJson(json: any) {
        const surveyType = new SurveyType();
        surveyType.name = A.string(json.meaning);
        surveyType.code = A.string(json.lowValue);
        return surveyType;
    }
}