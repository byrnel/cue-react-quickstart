import { observable } from "mobx";
import { GuidService } from "../services/";

export default class File {
    
    @observable baseResourceId: number;
    @observable guid: string = GuidService.newGuid();
    @observable serverGuid: string;
    @observable name: string;
    @observable size: number;
    @observable type: string;
}