import * as A from "../services/A";

export default class Series {
    id: number;
    title: string;
    saliProject: string;

    static fromJson(json: any) {
        const series = new Series();
        series.id = A.number(json.id);
        series.title = A.string(json.title);
        series.saliProject = A.string(json.saliProject);

        return series;
    }
}