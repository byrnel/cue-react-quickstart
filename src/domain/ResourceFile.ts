import { observable } from "mobx";
import { GuidService } from "../services/";

/**
 * Wraps a File and gives it a guid so we can safely use it
 * in a list with React keys.
 */
export default class ReportFile implements IStageableFile, IFileModel {
    @observable file: File;
    @observable guid: string = GuidService.newGuid();
    @observable serverGuid?: string;

    constructor(file: File) {
        this.file = file;
    }

    get name() {
        return this.file.name;
    }
}