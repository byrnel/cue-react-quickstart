import Type from "./Type";
import * as ResourceType from "./ResourceType";

export default class Types {
    typesArray = new Array<Type>();

    constructor() {
        ResourceType.resourceTypeList.map((value: string) => {
            this.typesArray.push(new Type(value, value.toLocaleLowerCase()));
        });
    }

}
