import * as A from "../services/A";

export default class Tag {
    id: number;
    name: string;

    static fromJson(json: any) {
        const tag = new Tag();
        tag.id = A.number(json.id);
        tag.name = A.string(json.name);
        return tag;
    }
}