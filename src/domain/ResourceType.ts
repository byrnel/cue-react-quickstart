export type ResourceType = "Journal Articles" | "Report" | "Factsheet" | "Map" | "Image" | "Soil Test Instructions" | "Word Document" | "Spreadsheet";
export type ParentResourceType = "Journal Articles" | "Report" | "Factsheet" | "Map";

export const resourceTypeList: string[] = ["Journal Articles", "Report", "Factsheet", "Map", "Image", "Soil Test Instructions", "Spreadsheet"];
export const allResourceTypes: ResourceType[] = ["Journal Articles", "Report", "Factsheet", "Map", "Image", "Soil Test Instructions", "Spreadsheet"];
export const allParentResourceTypes: ParentResourceType[] = ["Journal Articles", "Report", "Factsheet", "Map"];