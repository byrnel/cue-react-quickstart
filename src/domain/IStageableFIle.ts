interface IStageableFile {
    file: File;
    guid: string;
    
    /**
     * If the file has been staged (uploaded to the server into a temporary store),
     * this should be set to a guid which the server provided back to the client.
     * This guid can be submitted with subsequent requests to reference the file
     * data that have been stored on the server.
     */
    serverGuid?: string;
}