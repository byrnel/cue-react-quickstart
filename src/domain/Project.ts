import * as A from "../services/A";

export default class Project {
    name: string;
    code: string;

    static fromJson(json: any) {
        const project = new Project();
        project.name = A.string(json.name);
        project.code = A.string(json.code);
        return project;
    }
}