export default class User {
    id: number;
    userName: string;
    activities: string[];
}