import { observable } from "mobx";
import { GuidService } from "../services/";

export default class MapFile implements IStageableFile, IFileModel {
    @observable file: File;
    @observable guid: string = GuidService.newGuid();
    @observable serverGuid?: string;

    constructor(file: File) {
        this.file = file;
    }

    get name() {
        return this.file.name;
    }
}