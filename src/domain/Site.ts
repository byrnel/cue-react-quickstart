import * as A from "../services/A";

export default class Site {
    id: number;
    saliProject: string;

    static fromJson(json: any) {
        const site = new Site();
        site.id = A.number(json.id);
        site.saliProject = A.string(json.saliProject);

        return site;
    }
}