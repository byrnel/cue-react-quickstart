import { Moment } from "moment";

export default class Contact {
    projectCode: string;
    userId: string;
    role: string;
    endDate?: Moment;
    firstName: string;
    lastName: string;

    static mapJsonToContact = (x: any): Contact => ({
        projectCode: x.saliProjectCode,
        userId: x.userId,
        role: x.role,
        endDate: x.endDate != null ? x.endDate : null,
        firstName: x.firstName,
        lastName: x.lastName
    })
    
}