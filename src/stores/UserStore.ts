import { observable, action } from "mobx";
import { ApiService, Logger } from "../services/";
import { User } from "../domain/";
import * as R from "ramda";

class UserStore {
    // The logged in user (if any)
    @observable user?: User;

    // Misc / UI concerns
    @observable isAuthenticating: boolean = false; // Indicates whether we are requesting the current user
    @observable networkError: boolean = false;
    @observable isAuthenticated: boolean = false; // Lawrence  - Indicates user authenticated and control menu , secure routes ect

    getCurrentUser() {
        this.isAuthenticating = true;

        return ApiService
            .get("/authentication/whoami")
            .then(action((user?: User) => {
                this.user = user;
                this.isAuthenticating = false;
                this.isAuthenticated = user != null;
                return user;
            }))
            .catch(action((e) => {
                Logger.error(e);
                this.networkError = true;
                this.isAuthenticating = false;
                this.isAuthenticated = false;
                throw e;
            }));
    }

    @observable userName(): string {
        return this.user ? this.user.userName : "";
    }

    /**
     * Checks if the user is authorized to perform the specified activity. If
     * multiple activities are specified, the user must be authorized to perform
     * all of the activities.
     */
    isAuthorized(...activities: string[]) {
        return this.user != null
            && activities.length > 0
            && R.all(activity => this.user!.activities.indexOf(activity) > -1, activities);
    }

    /**
     * Checks if the user is authorized to perform the specified activity. If
     * multiple activities are specified, the user must be authorized to perform
     * at least one of the activities.
     */
    isAuthorizedForAny(...activities: string[]) {
        return this.user != null
            && activities.length > 0
            && R.any(activity => this.user!.activities.indexOf(activity) > -1, activities);
    }

    @observable dummyLogin():User {
        this.user = new User();
        this.user.id = 1;
        this.user.userName = "admin";
        this.user.activities = ["CreateProjectMetadata", "ManageSecurity"];
        return this.user;
    }
}

export default new UserStore(); 