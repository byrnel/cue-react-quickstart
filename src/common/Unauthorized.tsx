import * as React from "react";

export default class Unauthorized extends React.Component<{}, {}> {
    render() {
        return (
            <p>You are not authorized to perform this activity.</p>
        );
    }
}