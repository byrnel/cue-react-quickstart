import * as React from "react";
import { Form } from "semantic-ui-react";

interface IFileInputProps {
    label: string;
    value?: File;
    onChange: (e: any, data: any) => void;
}

export default class FileInput extends React.Component<IFileInputProps, {}> {
    private fileInput: HTMLInputElement | null;

    constructor(props: IFileInputProps) {
        super(props);
        this.handleFilesSelected = this.handleFilesSelected.bind(this);
    }

    handleFilesSelected(e: React.ChangeEvent<HTMLInputElement>) {
        e.preventDefault();
        const files = this.fileInput != null? this.fileInput.files : null;

        if(files == null || files.length === 0) {
            return;
        }

        this.props.onChange(e, { value: files[0] });
    }

    render() {
        const { value } = this.props;

        return (
             <Form.Field>
                <label>{this.props.label}</label>
                <div className="ui action input">
                    <input type="text" disabled={true} value={value != null ? value.name : ""} />
                    <label className="ui icon button btn-file">
                        <i className="attachment file icon" />
                        <input 
                            type="file"
                            style={{display:"none"}}
                            ref={x => this.fileInput = x}
                            onChange={this.handleFilesSelected}
                        />
                    </label>
                </div>
            </Form.Field>
        );
    }
}