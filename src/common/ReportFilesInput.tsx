import * as React from "react";
import { ResourceFile } from "../domain";
import { FilesInput } from "./";

interface IReportFilesInput {
    // A collection of report files
    value: Array<ResourceFile>;
    onChange: (e: any, data: { value: Array<ResourceFile> }) => void;
    errors?: IErrors;
    error?: boolean;
    required?: boolean;
}

export default class ReportFilesInput extends React.Component<IReportFilesInput, {}> {
    constructor(props: IReportFilesInput) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e: any, data: any) {
        this.props.onChange(e, {
            value: data.value
        });
    }

    handleCreateReportFile(file: File): IFileModel {
        return new ResourceFile(file);
    }

    render() {
        const { value, errors } = this.props;

        return (
            <FilesInput
                onChange={this.handleChange}
                fileModelConstructor={this.handleCreateReportFile}
                errors={errors}
                label="Report Files"
                value={value}
                required={this.props.required}
            />
        );
    }
}