import * as R from "ramda";

/**
 * Specifically for the UrlCombo Component (see Bind for original)
 */
function UrlComboBind(
    context: any,
    mainProperty: string,
    urlProperty: string,
    onUrlChange?: (e: any, data: any) => void,
    onMainChange?: (e: any, data: any) => void,
    errors?: IErrors,
    errorProperty?: string) {
    if(!(mainProperty in context)) {
        throw "Bound property does not exist: " + mainProperty;
    }
    if(!(urlProperty in context)) {
        throw "Bound property does not exist: " + urlProperty;
    }

    const checkErrorProperty = (errorProperty != null ? errorProperty : mainProperty).toLowerCase();
    const hasErrors =
        context.guid != null &&
        R.path([context.guid, checkErrorProperty], errors) != null;

    const wrapperFunction = (value: string) => {
        return value;
    };
    
    return {
        mainValue: wrapperFunction(context[mainProperty]),
        urlValue: wrapperFunction(context[urlProperty]),
        onMainChange: (e: any, data: any) =>  {
            context[mainProperty] = data.value;
            context.changesMade = true;
            if(onMainChange != null) {
                onMainChange(e, data);
            }
        },
        onUrlChange: (e: any, data: any) =>  {
            context[urlProperty] = data.value;
            context.changesMade = true;
            if(onUrlChange != null) {
                onUrlChange(e, data);
            }
        },
        error: hasErrors
    };
}

export default UrlComboBind;