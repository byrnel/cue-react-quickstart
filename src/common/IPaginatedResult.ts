interface IPaginatedResult<TModel> {
    entities: Array<TModel>;
    paginationOptions: {
        page: number;
        pageSize: number;
        pages: number;
        sortProperty: string;
        sortDirection: "ascending" | "descending";
    };
}