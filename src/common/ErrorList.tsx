import * as React from "react";
import { Message } from "semantic-ui-react";

interface IErrorListProps {
    errors?: IErrors;
}

export default class ErrorList extends React.Component<IErrorListProps, {}> {
    render() {
        const { errors } = this.props;

        if(errors == null) {
            return null;
        }

        const flatErrors = new Array<string>();

        for(let guidKey of Object.keys(errors)) {
            for(let propertyKey of Object.keys(errors[guidKey])) {
                for(let validationError of errors[guidKey][propertyKey]) {
                    flatErrors.push(validationError.errorMessage);
                }
            }
        }
        
        return (
            <Message
                negative={true}
                header="There were some errors with your submission"
                list={flatErrors}
            />
        );
    }
}