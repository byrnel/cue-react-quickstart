import * as React from "react";

/**
 * Wraps a collection of inputs in a div with an "inputs" class so
 * the inputs can be joined together similar to Semantic UI's Button.Group
 * @param props 
 */
export default function InputGroup(props: any) {
    const otherProps = {...props};
    delete otherProps.children;
    return <div className="MODELd-ui inputs" {...otherProps}>{props.children}</div>;
}