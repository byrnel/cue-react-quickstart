import * as React from "react";
import { Modal, Button, Header, Icon, List, SemanticCOLORS, Checkbox, Message, SemanticICONS } from "semantic-ui-react";

export interface ISaliModalProps {
    forceHide?: boolean; // this is a hack, do not touch it <3
    header?: string;
    content?: string;
    list?: string[];
    color?: SemanticCOLORS;
    icon?: string;
    dimmer?: boolean;
    positiveText?: string;
    negativeText?: string;
    onPositiveClick?: () => void;
    onNegativeClick?: () => void;
    positiveColor?: SemanticCOLORS;
    negativeColor?: SemanticCOLORS;
    customContent?: JSX.Element;
    error?: boolean;
    onClose: () => void;
    checkboxText?: string;
    checkboxErrorText?: string;
}

export interface ISaliModalConfigProp {
    propertyName: string;
    context: object;
}

const generateList = (items: string[]) => {
    return (
        <List bulleted={true}>
            {items.map((value: string, index: number) => {
                return (
                    <List.Item key={index}>
                        {value}
                    </List.Item>
                );
            })}
        </List>
    );
};

interface ISaliModalState {
    isChecked: boolean;
    showCheckboxError: boolean;
}

export default class SaliModal extends React.Component<ISaliModalProps, ISaliModalState> {
    static config = (args: Array<ISaliModalConfigProp>): ISaliModalProps => {
        for (const configProp of args) {
            if (configProp.context[configProp.propertyName]) {
                return configProp.context[configProp.propertyName];
            }
        }
        return ({
            forceHide: true,
            onClose: () => {
                // Pray that you never end up here
            }
        });
    }

    constructor(props: ISaliModalProps) {
        super(props);
        this.state = {
            isChecked: this.props.checkboxText ? false : true,
            showCheckboxError: false
        };
        this.handleUnchecked = this.handleUnchecked.bind(this);
        this.toggle = this.toggle.bind(this);
    }

    handleUnchecked() {
        this.setState({
            showCheckboxError: true
        });
    }

    toggle() {
        this.setState({
            isChecked: !this.state.isChecked
        });
    }

    render() {
        const { forceHide, header, content, list, positiveText, negativeText, onPositiveClick, onNegativeClick, 
            negativeColor, onClose, dimmer, customContent, error, checkboxText, checkboxErrorText } = this.props;

        const color = error ? "red" : this.props.color;
        const buttonColour = error ? "black" : this.props.positiveColor;
        const icon: SemanticICONS = error ? "warning circle" : this.props.icon as SemanticICONS;

        return (
            <Modal
                dimmer={dimmer? dimmer : true}
                open={forceHide ? false : true}
                size="small"
                onClose={onClose}
                closeOnDimmerClick={true}
            >
                <Header>
                    {icon && <Icon name={icon} color={color}/>}
                    {header ? header : "SALI Modal Header"}
                </Header>

                <Modal.Content>
                    <Modal.Description>
                        <p>{content}</p>
                    </Modal.Description>
                    {customContent}
                    {list && generateList(list)}
                    {this.state.showCheckboxError && <Message color="red" content={checkboxErrorText || "Please check the box before continuing"}/>}
                    {checkboxText && <Checkbox label={checkboxText} checked={this.state.isChecked} onClick={this.toggle}/>}
                </Modal.Content>

                <Modal.Actions>
                    {negativeText && (
                        <Button
                            className="js-sali-modal-negative-button"
                            content={negativeText} 
                            color={negativeColor ? negativeColor : "black"}
                            onClick={onNegativeClick || onClose}
                        />
                    )}
                    <Button
                        className="js-sali-modal-positive-button"
                        color={buttonColour ? buttonColour : "green"}
                        content={positiveText ? positiveText : "Okay"}
                        onClick={this.state.isChecked? (onPositiveClick || onClose) : this.handleUnchecked}
                    />
                </Modal.Actions>
            </Modal>
        );
    }
}