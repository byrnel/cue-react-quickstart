import * as React from "react";
import { Form, Input, Button } from "semantic-ui-react";

export interface IUrlComboProps {
    mainLabel?: string;
    urlLabel?: string;
    urlBlock: string;
    mainValue?: string;
    urlValue?: string;
    onUrlChange?: (e: any, data: any) => void;
    onMainChange?: (e: any, data: any) => void;
    error?: boolean;
    mainRequired?: boolean;
    urlRequired?: boolean;
    className?: string;
    mainPlaceholder?: string;
    mainClassName?: string;
    urlClassName?: string;
}

interface IUrlComboState {
    urlChanged: boolean;
}

export default class UrlCombo extends React.Component<IUrlComboProps, IUrlComboState> {
    constructor(props: IUrlComboProps) {
        super(props);
        this.state = { urlChanged: false };
        this.init();
    }

    componentDidMount() {
        this.init();
    }

    componentWillReceiveProps(nextProps: IUrlComboProps) {
        this.init();
    }

    init = () => {
        const existing = this.stripUrlString(this.props.urlValue);
        var urlChanged = false;
        
        if (existing !== "" 
            && existing !== this.slugify(this.props.mainValue? this.props.mainValue : "-1")) {
            urlChanged = true;
        }

        this.setState({urlChanged: urlChanged});
    }

    handleUrlChange = (e: any, data: any) => {
        const value = this.slugify(data.value);
        var realChange = true;

        if (data.value === "") {
            realChange = false;
        }
        this.setState({urlChanged: realChange});
        this.updateUrlProp(e, {value});
    }

    handleMainChange = (e: any, data: any) => {
        const value = this.slugify(data.value);
        
        if (!this.state.urlChanged) {
            this.updateUrlProp(e, {value});
        }

        if (this.props.onMainChange) {
            this.props.onMainChange(e, data);    
        }
    }

    handleRefreshClick = (e: any, data: any) => {
        const value = this.slugify(this.props.mainValue ? this.props.mainValue : "");
        this.setState({urlChanged: false});
        this.updateUrlProp(e, {value});
    }

    // from https://gist.github.com/mathewbyrne/1280286
    slugify = (value: string) => {
      return value.toString().toLowerCase()
        .replace(/\s+/g, "-")           // Replace spaces with -
        .replace(/[^\w\-]+/g, "")       // Remove all non-word chars
        .replace(/\-\-+/g, "-")         // Replace multiple - with single -
        .replace(/^-+/, "");            // Trim - from start of text
    }

    stripUrlString(fullUrl: string | undefined) {
        return fullUrl ? fullUrl.split("/").slice(-1)[0] : "";
    }

    updateUrlProp(e: any, data: any) {
        if (this.props.onUrlChange) {
            this.props.onUrlChange(e, {value: this.props.urlBlock + data.value});
        }
    }

    render() {
        const { 
            mainLabel, 
            urlLabel, 
            urlBlock, 
            mainValue, 
            urlValue, 
            error,
            mainRequired, 
            urlRequired, 
            className, 
            mainPlaceholder,
            mainClassName,
            urlClassName
         } = this.props;
        const frontUrl = this.stripUrlString(urlValue);

        return (
            <div className={className}>
                <Form.Input
                    id="main-input"
                    label={mainLabel} 
                    placeholder={mainPlaceholder}
                    value={mainValue}
                    onChange={this.handleMainChange}
                    error={error}
                    required={mainRequired}
                    className={mainClassName}
                />
                <Form.Field
                    error={error}
                    required={urlRequired}
                >
                    <label>{urlLabel}</label>

                    <div style={{marginBottom: "16px"}}>
                    <Input
                        id="url-input"
                        style={{width: "91%", marginRight: "4px"}}
                        size="mini" 
                        label={urlBlock}
                        value={frontUrl}
                        onChange={this.handleUrlChange}
                        className={urlClassName}
                    />

                    <Button
                        size="tiny" 
                        icon="undo"
                        onClick={this.handleRefreshClick}
                    />
                </div>
                </Form.Field>
            </div>
        );
    }
}