import * as React from "react";
import { configure, mount, ReactWrapper } from "enzyme";
import * as Adapter from "enzyme-adapter-react-15";
import Select, { ISelectOption, ISelectProps } from "./Select";

configure({ adapter: new Adapter() });

class Person {
    id: number;
    name: string;
    age: number;

    constructor(id: number, name: string, age: number) {
        this.id = id;
        this.name = name;
        this.age = age;
    }
}

const options = [
    new Person(100, "Musk", 44),
    new Person(101, "Gates", 55),
    new Person(102, "Jobs", 66)
];

const optionsFunc = (person: Person): ISelectOption => ({ text:person.name, value:person.id.toString()});

const selectItemAt = (wrapper: ReactWrapper<any, any>, index: number) => {
     // Open the dropdown
     wrapper.find("div[role='combobox']").simulate("focus");
     wrapper.update();
 
     // Select the option
     wrapper.find(".item").at(index).simulate("click");
     wrapper.update();
};

it("renders without crashing", () => {
    mount(
        <Select
            options={options}
            optionsFunc={optionsFunc}
            onChange={(e, data) => { ""; }}
            blurOnClick={false}
        />
    );
});

it("renders without crashing when there are no options", () => {
    mount(
        <Select
            options={[]}
            optionsFunc={optionsFunc}
            onChange={(e, data) => { ""; }}
            blurOnClick={false}
        />
    );
});

it("supports selecting a value", () => {
    let newValue: Person | undefined = undefined;

    const wrapper = mount(
        <Select
            options={options}
            optionsFunc={optionsFunc}
            onChange={(e, data) => { newValue = data.value; }}
            blurOnClick={false}
        />
    );

    // Select the second option
    selectItemAt(wrapper, 1);

    // Gates should have been chosen
    expect(newValue).toBe(options[1]);
});

it("supports changing the selected value", () => {
    let newValue: Person | undefined = undefined;

    const wrapper = mount<ISelectProps>(
        <Select
            options={options}
            optionsFunc={optionsFunc}
            onChange={(e, data) => { newValue = data.value; }}
            blurOnClick={false}
        />
    );

    // Select the second option
    selectItemAt(wrapper, 1);
    
    // Gates should have been chosen
    expect(newValue).toBe(options[1]);
    wrapper.setProps({ value: options[1] });

    // Now select the first option
    selectItemAt(wrapper, 0);

    // And Musk should have been chosen
    expect(newValue).toBe(options[0]);
});

it("supports clearing the value", () => {
    let newValue: Person | undefined = undefined;

    const wrapper = mount(
        <Select
            withBlankOption={true}
            options={options}
            optionsFunc={optionsFunc}
            onChange={(e, data) => { newValue = data.value; }}
            blurOnClick={false}
        />
    );

    // Select the second option (accounting for the first option being a blank
    // option)
    selectItemAt(wrapper, 2);
    
    // Gates should have been chosen
    expect(newValue).toBe(options[1]);
    wrapper.setProps({ value: options[1] });

    // Now select the blank option
    selectItemAt(wrapper, 0);

    // And undefined should have been chosen
    expect(newValue).toBeUndefined();
});

it("supports new values being added to the options", () => {
    let newValue: Person | undefined = undefined;

    let mutatingOptions = [...options];
    
    const wrapper = mount(
        <Select
            value={mutatingOptions[1]} // Select Gates first
            withBlankOption={true}
            options={mutatingOptions}
            optionsFunc={optionsFunc}
            onChange={(e, data) => { newValue = data.value; }}
            blurOnClick={false}
        />
    );

    // Add Hugh Herr as a new option
    const herr = new Person(104, "Herr", 33);
    mutatingOptions.push(herr);

    // Force a re-render
    wrapper.update();

    // The selected value shouldn't have changed
    expect(newValue).toBeUndefined();

    // We should be able to choose Herr
    selectItemAt(wrapper, 4);
    expect(newValue).toBe(herr);
});

it("supports an entirely new options list", () => {
    let newValue: Person | undefined = undefined;
    
    const wrapper = mount(
        <Select
            value={options[1]} // Select Gates first
            withBlankOption={true}
            options={options}
            optionsFunc={optionsFunc}
            onChange={(e, data) => { newValue = data.value; }}
            blurOnClick={false}
        />
    );

    // Replace the options list with a new one
    const newOptions = [
        new Person(105, "Snowden", 29)
    ];
    wrapper.setProps({ options: newOptions });
    wrapper.render();

    // The selected value shouldn't have changed
    expect(newValue).toBeUndefined();

    // We should be able to choose Snowden
    selectItemAt(wrapper, 1);
    expect(newValue).toBe(newOptions[0]);
});