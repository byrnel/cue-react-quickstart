import * as React from "react";
import { configure, mount, ReactWrapper } from "enzyme";
import * as Adapter from "enzyme-adapter-react-15";
import DropList, { IDropListOption } from "./DropList";

configure({ adapter: new Adapter() });

class Item {
    id: number;
    text: string;

    constructor(id: number, text: string) {
        this.id = id;
        this.text = text;
    }
}

const entities = [
    new Item(100, "ABC"),
    new Item(101, "IJK"),
    new Item(102, "XYZ"),
    new Item(103, "ABC"),
    new Item(104, "IJK"),
    new Item(105, "XYZ"),
    new Item(106, "ABC"),
    new Item(107, "IJK"),
    new Item(108, "XYZ")
];

const optionsFunc = (item: Item): IDropListOption => {
        return {text: item.text, value: item.id};
};

const cliccItem = (wrapper: ReactWrapper<any, any>, index: number) => {
    // Open the dropdown
    wrapper.find("div[role='combobox']").simulate("focus");
    wrapper.update();

    // Clicc
    const child = wrapper.find("div[role='listbox']").children().at(index);
    child.simulate("click");
};

const deletItemAt = (wrapper: ReactWrapper<any, any>, index: number) => {
    // Clicc remove item
    const list = wrapper.find("div[role='list']");
    const child = list.childAt(index);
    // console.log(child.debug());
    const button = child.find("button");
    // console.log(button.debug());
    button.simulate("click");
};

it ("renders without crashing", () => {
    mount(
        <DropList
            options={entities}
            optionsFunc={optionsFunc}
        />
    );
});

it ("renders without crashing with no options", () => {
    mount(
        <DropList
            options={[]}
            optionsFunc={optionsFunc}
        />
    );
});

it ("can have an item selected", () => {
    let value: number[] = [];
    const wrapper = mount(
        <DropList
            value={[]}
            options={entities}
            optionsFunc={optionsFunc}
            onChange={(e, data) => {
                value=data.value;
            }}
        />
    );
    // List is empty
    expect(wrapper.find("div[role='list']").children()).toHaveLength(0);
    // Select value
    cliccItem(wrapper, 2);
    // Value was selected
    expect(value).toHaveLength(1);
    expect(value[0]).toBe(entities[2].id);
    // Value was added to the list
    expect(wrapper.find("div[role='list']").children()).toHaveLength(1);
});

// We have issues with these tests in that: a second click cannot be performed on the combobox item due
// to problems with refocussing after the first click
xit ("can have multiple items selected", () => {
    let value: number[] = [];
    const wrapper = mount(
        <DropList
            value={value}
            options={entities}
            optionsFunc={optionsFunc}
            onChange={(e, data) => {
                value=data.value;
            }}
        />
    );

    // List is empty
    expect(wrapper.find("div[role='list']").children()).toHaveLength(0);

    // Focus
    wrapper.find("div[role='combobox']").simulate("focus");
    wrapper.update();

    // console.log(wrapper.find("div[role='listbox']").children().debug());

    // Clicc
    wrapper.find("div[role='listbox']").children().at(0).simulate("focus", { preventDefault() {"<div>";}});
    wrapper.update();

    // Unfocus
    wrapper.find("div[role='combobox']").simulate("change", {data: {value: {text: "yeet"}}});
    wrapper.update();

    // focus on something else.
    wrapper.find("div#boi").simulate("click");
    wrapper.update();

    // Focus
    wrapper.find("div[role='combobox']").simulate("focus");
    wrapper.update();

    // console.log(wrapper.find("div[role='listbox']").children().debug());
});

it ("can have an item removed", () => {
    let value: number[] = [];
    const wrapper = mount(
        <DropList
            value={value}
            options={entities}
            optionsFunc={optionsFunc}
            onChange={(e, data) => {
                value=data.value;
            }}
        />
    );

    // Select value
    cliccItem(wrapper, 2);
    // Click delete
    deletItemAt(wrapper, 0);
    // Value was removed
    expect(value).toHaveLength(0);
    // Value was removed from list
    expect(wrapper.find("div[role='list']").children()).toHaveLength(0);
});

// xit ("can have multiple items removed", () => {

// });

// xit ("can have the same item added then removed then added again", () => {
    
// });