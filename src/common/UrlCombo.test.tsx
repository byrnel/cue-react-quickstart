import * as React from "react";
import { configure, mount, ReactWrapper } from "enzyme";
import * as Adapter from "enzyme-adapter-react-15";
import UrlCombo from "./UrlCombo";

configure({ adapter: new Adapter() });

const simulateKeyPresses = (input: ReactWrapper<any, any>, text: string) => {
    input.simulate("change", {target: {value: text}});
};

const inputTitle = (wrapper: ReactWrapper<any, any>, text: string) => {
    // focus title input
    const input = wrapper.find("input#main-input");
    input.simulate("focus");
    wrapper.update();

    // type text
    simulateKeyPresses(input, text);
    wrapper.update();

    // unfocus title input
    input.simulate("blur");
    wrapper.update();
};

const inputUrl = (wrapper: ReactWrapper<any, any>, text: string) => {
    // focus url input
    const input = wrapper.find("input#url-input");
    input.simulate("focus");
    wrapper.update();

    // type text
    simulateKeyPresses(input, text);
    wrapper.update();

    // unfocus url input
    input.simulate("blur");
    wrapper.update();
};

const resetUrl = (wrapper: ReactWrapper<any, any>) => {
    // click url reset button
    wrapper.find("button").simulate("click");
    wrapper.update();
};

const slugify = (value: string) => {
    return value.toString().toLowerCase()
      .replace(/\s+/g, "-")           // Replace spaces with -
      .replace(/[^\w\-]+/g, "")       // Remove all non-word chars
      .replace(/\-\-+/g, "-")         // Replace multiple - with single -
      .replace(/^-+/, "");            // Trim - from start of text
};

const urlBlock = "http://www.ayylmao.com/";

it ("renders without crashing", () => {
    mount(
        <UrlCombo
            urlBlock={urlBlock}
        />
    );
});

it ("can store basic url", () => {
    var title: string = "";
    var url: string = "";
    const wrapper = mount(
        <UrlCombo
            mainValue={title}
            onMainChange={(e: any, data: any) => {
                title=data.value;
            }}
            urlValue={url}
            onUrlChange={(e: any, data: any) => {
                url=data.value;
            }}
            urlBlock={urlBlock}
        />
    );
    wrapper.update();
    // Check values
    expect(title).toBe("");
    expect(url).toBe("");
    // Input title
    inputTitle(wrapper, "test");
    // Check values
    expect(title).toBe("test");
    expect(url).toBe(urlBlock + "test");
});

it ("can store complex url", () => {
    var title: string = "";
    var url: string = "";
    const wrapper = mount(
        <UrlCombo
            mainValue={title}
            onMainChange={(e: any, data: any) => {
                title=data.value;
            }}
            urlValue={url}
            onUrlChange={(e: any, data: any) => {
                url=data.value;
            }}
            urlBlock={urlBlock}
        />
    );
    wrapper.update();
    // Check values
    expect(title).toBe("");
    expect(url).toBe("");
    const textInput = "test so\\me wack/y sh-t ov!r h3re";
    // Input title
    inputTitle(wrapper, textInput);
    // Check values
    expect(title).toBe(textInput);
    expect(url).toBe(urlBlock + slugify(textInput));
});

it ("can handle custom url", () => {
    var title: string = "";
    var url: string = "";
    const wrapper = mount(
        <UrlCombo
            mainValue={title}
            onMainChange={(e: any, data: any) => {
                title=data.value;
            }}
            urlValue={url}
            onUrlChange={(e: any, data: any) => {
                url=data.value;
            }}
            urlBlock={urlBlock}
        />
    );
    wrapper.update();
    const textInput = "test";
    const urlInput = "custom url";
    // Input title
    inputTitle(wrapper, textInput);
    // Check values
    expect(title).toBe(textInput);
    expect(url).toBe(urlBlock + slugify(textInput));
    // Change URL
    inputUrl(wrapper, urlInput);
    // Check values
    expect(title).toBe(textInput);
    expect(url).toBe(urlBlock + slugify(urlInput));
});

it ("can reset url", () => {
    var title: string = "";
    var url: string = "";
    let wrapper = mount(
        <UrlCombo
            mainValue={title}
            onMainChange={(e: any, data: any) => {
                title=data.value;
            }}
            urlValue={url}
            onUrlChange={(e: any, data: any) => {
                url=data.value;
            }}
            urlBlock={urlBlock}
        />
    );
    wrapper.update();
    const textInput = "test";
    const urlInput = "custom url";
    // Input title
    inputTitle(wrapper, textInput);
    // Change URL
    inputUrl(wrapper, urlInput);
    // Check values
    expect(title).toBe(textInput);
    expect(url).toBe(urlBlock + slugify(urlInput));

    // Reset Url
    resetUrl(wrapper);
    // Check values
    expect(title).toBe(textInput);
    expect(url).toBe(urlBlock); // this does not append the actual 'title' value as it should because the prop is never updated in the actual DOM
});

it ("can resync url", () => {
    var title: string = "";
    var url: string = "";
    let wrapper = mount(
        <UrlCombo
            mainValue={title}
            onMainChange={(e: any, data: any) => {
                title=data.value;
            }}
            urlValue={url}
            onUrlChange={(e: any, data: any) => {
                url=data.value;
            }}
            urlBlock={urlBlock}
        />
    );
    wrapper.update();
    const textInput = "test";
    const textInputTwo = "yeet";
    // Input title
    inputTitle(wrapper, textInput);
    // Check values
    expect(title).toBe(textInput);
    expect(url).toBe(urlBlock + slugify(textInput));

    // Reset Url
    resetUrl(wrapper);
    // Check values
    expect(title).toBe(textInput);
    expect(url).toBe(urlBlock);
    
    // Update title
    inputTitle(wrapper, textInputTwo);
    // Check values
    expect(title).toBe(textInputTwo);
    expect(url).toBe(urlBlock + textInputTwo);

});