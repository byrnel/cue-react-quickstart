import * as React from "react";
import { Form, Button, Icon, Message, List } from "semantic-ui-react";
import { Select } from "./";

// todo jay - fix this boi so that he 'value' is he got dam string
export interface IDropListOption {
    text: string;
    value: any;
}

export interface IDropListProps {
    value?: Array<any>;
    label?: string;
    onChange?: (e: any, data: any) => void;
    options: Array<any>; // An array of rich entities (they are converted to select options using optionsFunc)
    optionsFunc: (e: any) => IDropListOption;
    error?: boolean;
    disabled?: boolean;
    withBlankOption?: boolean;
    required?: boolean;

    subLabel?: string;
    placeholder?: string;
    noResultsMessage?: string;
    message?: string;
    withButton?: boolean;
}

interface IDropListState {
    selectedItems: Array<IDropListOption>;
    actualOptions: Array<any>;
}

let selectedCategory: any;
let localOptions: IDropListOption[];
let simpleOptions: any[];

export default class DropList extends React.Component<IDropListProps, IDropListState> {
    constructor(props: IDropListProps) {
        super(props);

        localOptions = props.options.map((value: any) => props.optionsFunc(value));
        simpleOptions = [];
        localOptions.forEach((option: IDropListOption) => {
            simpleOptions.push(option.value);
        });
        this.state = {selectedItems: props.value ? this.filterValidSelections(props.value) : [], actualOptions: []};
        this.state = {selectedItems: this.state.selectedItems, actualOptions: this.getActualOptions()};
    }

    /**
     * Ensure that all selected values are valid with respect to the simpleOptions
     * given (i.e. the original absolute list of options)
     * @param value the potentially invalid selections
     */
    filterValidSelections = (value: Array<any>): Array<any> => {
        var validSelections: Array<any> = [];
        value.forEach((item: any) => {
            if (simpleOptions.indexOf(this.props.optionsFunc(item).value) !== -1) {
                validSelections.push(item);
            }
        });

        if (this.props.onChange !== undefined) {
            this.props.onChange(undefined, {value: validSelections});
        }

        return validSelections;
    }

    /**
     * Every time we come back, grab the const options and strip out
     * and values that are in 'value' so that the drop down only ever
     * shows [original - value] and value is the list of all selections
     * but we never actually modify the original value
     */
    getActualOptions() {
        var i: number = -1;
        var actualOptions = this.props.options.slice();
        this.state.selectedItems.forEach(function(element: any) {
            i = simpleOptions.indexOf(element);
            if (i !== -1) {
                actualOptions.splice(i, 1);
            }
        });
        return actualOptions;
    }

    handleAddItem = (e: any, data: any) => {
        if (selectedCategory === null 
            || selectedCategory === undefined) {
            return;
        }

        var selectedItems =  [selectedCategory,... this.state.selectedItems];
        this.setState(
            {selectedItems: selectedItems},
            () => {
                this.setState({actualOptions: this.getActualOptions()});
            }
        );

        if (this.props.value !== undefined && this.props.onChange !== undefined) {
            this.props.onChange(e, {value:[selectedCategory,... this.props.value]});
        }

        selectedCategory = undefined;
    }

    handleSelectionChange = (e: any, data: any) => {
        selectedCategory = this.props.optionsFunc(data.value).value;
        if (!this.props.withButton) {
            this.handleAddItem(e, data);
        }
    }

    handleRemoveItem = (e: any, data: any) => {
        this.state.selectedItems.splice(data.id, 1);
        this.setState(
            {selectedItems: this.state.selectedItems, actualOptions: this.getActualOptions()},
            () => {
                if (this.props.value !== undefined && this.props.onChange !== undefined) {
                    this.props.onChange(e, {value: this.state.selectedItems});
                }
            }
        );
    }

    // Render the added categories list
    renderCategoryList = () => {
        return (
            <List relaxed={true}>
                {this.state.selectedItems.map((item: any, index: number) => {
                    return (
                        <List.Item key={index}>
                            <List.Content verticalAlign="top">
                                <List.Header>
                                    <Button 
                                        id={index}
                                        onClick={this.handleRemoveItem} 
                                        icon="x" 
                                        color="red" 
                                        size="tiny" 
                                        style={{marginRight: "16px"}}
                                    />
                                    {item}
                                </List.Header>
                            </List.Content>
                        </List.Item>
                    );
                })}
            </List>
        );
    }

    render() {
        const { label, subLabel, placeholder, noResultsMessage, message, withButton } = this.props;
        const { actualOptions, selectedItems } = this.state;
        return (
            <Form.Field>
                <label>{label? label : "DropList"}</label>
                <div style={{marginBottom: "16px"}}>
                    <Select
                        selectOnBlur={false}
                        style={withButton? {width: "65%"} : {width: "100%"}}
                        // selection 
                        // scrolling
                        options={actualOptions}
                        optionsFunc={this.props.optionsFunc}
                        placeholder={placeholder? placeholder : "Select item..."}
                        noResultsMessage={noResultsMessage? noResultsMessage : "No results..."}
                        onChange={this.handleSelectionChange}
                    />

                    {withButton &&
                        <Button 
                            hidden={!withButton}
                            onClick={this.handleAddItem} 
                            animated="vertical" 
                            style={{width: "15%", margin: "auto 4px"}}
                        >
                        <Button.Content hidden={true}>
                            <Icon name="plus" />
                        </Button.Content>
                        <Button.Content visible={true}>Add</Button.Content>
                        </Button>}
                </div>
                
                <label>{selectedItems.length > 0? subLabel : ""}</label>
                <Message 
                    info={true}
                    compact={true}
                    hidden={selectedItems.length > 0}
                    content={message? message : "Add items using the above dropdown"}
                    style={{marginTop: 0}}
                />

                {this.renderCategoryList()}
            </Form.Field>
        );
    }
}