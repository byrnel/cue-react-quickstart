import * as React from "react";
import { Icon, Header } from "semantic-ui-react";

interface INetworkErrorProps {
    header?: string;
    message?: string;
}

/**
 * This can be used to display a fairly generic error dialog for when there is
 * a network error such as the Api being offline or the server failing to
 * handle a request properly.
 */
export default class NetworkError extends React.Component<INetworkErrorProps, {}> {
    render() {
        return (
            <div style={{width: "100%", height: "100%", paddingTop: "10%", paddingBottom: "10%", backgroundColor: "#EAEAEA", textAlign: "center"}}>
                <Icon name="cogs" size="massive" style={{color: "#DDD"}} />
                <Header size="large">{this.props.header || "Something went wrong..."}</Header>
                {this.props.message || "A network error occurred. Please wait a few moments then try again."}
            </div>
        );
    }
}