import * as React from "react";
import { Grid, Button, SemanticWIDTHS, SemanticFLOATS } from "semantic-ui-react";
require("./sali-page-style.css");

export interface ISaliButtonFooterProps {
    className?: string;
    fullWidth?: boolean;
    floated?: SemanticFLOATS;
}

export default class SaliButtonFooter extends React.Component<ISaliButtonFooterProps, {}> {
    constructor(props: ISaliButtonFooterProps) {
        super(props);
    }

    render() {
        const {
            children,
            className,
            floated
        } = this.props;

        const numColumns: number = React.Children.count(children);

        return (
            <Grid columns={2}>
                <Grid.Row style={{paddingLeft: 1, paddingRight: 1}}>
                    <Grid.Column floated={floated ? floated : "left"}>
                        <Button.Group 
                            attached="bottom" 
                            widths={numColumns as SemanticWIDTHS} 
                            className={"sali-button-footer " + className}
                        >
                            {children}
                        </Button.Group>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
}