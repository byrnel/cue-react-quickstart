import * as React from "react";
// import Constants from "./Constants";
import { Container, Message, Icon, SemanticICONS } from "semantic-ui-react";
// import { Link } from "react-router-dom";

export interface IBreadCrumbItem {
    href: any;
    content: string;
}

export interface ISaliHeaderProps {
    className?: string;
    icon: string;
    header?: string;
    breadcrumb?: Array<IBreadCrumbItem>;
    isLoading?: boolean;
    attached?: boolean | false;
}

interface ISaliHeaderState {
    contentWidth: number;
    scrolled?: boolean;
}

export default class SaliHeader extends React.Component<ISaliHeaderProps, ISaliHeaderState> {

    constructor(props: ISaliHeaderProps) {
        super(props);
        this.state = {
            scrolled: false,
            contentWidth: 0
        };
    }

    componentDidMount() {
        // window.addEventListener("resize", this.handleResize);
        // window.addEventListener("scroll", this.handleScroll);
    }

    componentWillUnmount() {
        // window.removeEventListener("resize", this.handleResize);
        // window.removeEventListener("scroll", this.handleScroll);
    }

    render() {

        const {
            icon,
            header,
            isLoading,
            attached
        } = this.props;

        let iconName: SemanticICONS = isLoading ? "circle notched" : icon as SemanticICONS;

        return (
            <Container fluid={true}>
                <Message icon={iconName} attached={attached}>
                    <Icon name={iconName} loading={isLoading} />
                    <Message.Content>
                        <Message.Header className="break-word">{header}</Message.Header>
                        {this.props.children}
                    </Message.Content>
                </Message>
            </Container>
        );
    }
}