import { CSSProperties } from "react";

class PublicationSetConstants {
    // Globals
    APP_NAME = "INFORMd";

    // Content Box nonsense
    CONTENT_BOX_PADDING = 20;
    NEVER_DO_THIS = 57; // Set this based on horizontal padding of semantic-ui-react"s context box

    // Size constants
    EXISTING_MENU_HEIGHT = 40; // margin to set so the fixed object appears to attach to the existing menu
    SALI_MENU_HEIGHT = 52;
    SALI_HEADER_HEIGHT = 128;
    SALI_HEADER_CONTENT_HEIGHT = 64;
    MENU_DROP_TRIGGER_HEIGHT = this.EXISTING_MENU_HEIGHT + this.SALI_HEADER_HEIGHT - this.CONTENT_BOX_PADDING - this.SALI_HEADER_CONTENT_HEIGHT; // how far do we scroll before triggering a fix
    HEADER_DROP_TRIGGER_HEIGHT = this.EXISTING_MENU_HEIGHT + (0.5*this.SALI_HEADER_HEIGHT - 0.5*this.SALI_HEADER_CONTENT_HEIGHT) - this.CONTENT_BOX_PADDING;
    MAX_WIDTH = 1020;
    MIN_WIDTH = 600;

    // Style constants
    HEADER_STYLE_DROPPED: CSSProperties;
    MENU_STYLE_DROPPED: CSSProperties;
    CONTENT_STYLE_DROPPED: CSSProperties;

    // Generate style objects
    constructor() {
        this.HEADER_STYLE_DROPPED = {
            // position: "fixed",
            // maxWidth: this.MAX_WIDTH,
            // minWidth: this.MIN_WIDTH
            wordWrap: "break-word"
            // top: this.EXISTING_MENU_HEIGHT + 0.5*this.SALI_HEADER_CONTENT_HEIGHT
        };
        this.MENU_STYLE_DROPPED = {
            position: "fixed",
            maxWidth: this.MAX_WIDTH,
            minWidth: this.MIN_WIDTH,
            top: this.EXISTING_MENU_HEIGHT + this.SALI_HEADER_CONTENT_HEIGHT
        };
        this.CONTENT_STYLE_DROPPED = {
            marginTop: this.EXISTING_MENU_HEIGHT + this.SALI_MENU_HEIGHT - 2*this.CONTENT_BOX_PADDING
        };
    }
}

export default new PublicationSetConstants();