import * as React from "react";
import Constants from "./Constants";
import { Menu } from "semantic-ui-react";
require("./sali-page-style.css");

export interface ISaliMenuProps {
    className?: string;
    style?: any;
    header?: string;
    isLoading?: boolean;
}

interface ISaliMenuState {
    contentWidth: number;
    scrolled?: boolean;
}

// const renderMenuGroup = (parentIndex: number) => {
//     return (
//         <Menu.Menu position={config.position ? config.position : "left"}>
//         <Menu.Item style={{padding: "0 0 0 0"}}> 
//             <Button.Group style={{margin: "0 0 0 0", padding: "0 0 0 0"}}>
//                 {config.buttons && config.buttons.map((button: any, index: number) => {
//                     return (
//                         <Button
//                             id={button.id || parentIndex + index} 
//                             style={{...Constants.MENU_BUTTON_STYLE,...button.style}}
//                             onClick={button.onClick} >

//                         </Button>
//                     );
//                 })}
//             </Button.Group>
//         </Menu.Item> 
//     </Menu.Menu>
//     );
// }

export default class SaliMenu extends React.Component<ISaliMenuProps, ISaliMenuState> {
    constructor(props: ISaliMenuProps) {
        super(props);

        this.state = {
            scrolled: false,
            contentWidth: 0
        };
    }

    componentDidMount() {
        // window.addEventListener("resize", this.handleResize);
        // window.addEventListener("scroll", this.handleScroll);
    }

    componentWillUnmount() {
        // window.removeEventListener("resize", this.handleResize);
        // window.removeEventListener("scroll", this.handleScroll);
    }

    render() {
        const {
            scrolled,
            contentWidth
        } = this.state;

        const {
            className,
            style,
            header
        } = this.props;

        const dynamicStyle = scrolled
            ? { ...Constants.MENU_STYLE_DROPPED, width: contentWidth }
            : {};

        return (
            <Menu
                // style={menuStyle} // todo: get this working with scroller
                style={{...style,...dynamicStyle}}
                tabular={true}
                className={"sali-menu " + className}
            >
                {header && 
                    <h1>{header}</h1>
                }
                {!header && this.props.children}
            </Menu>
        );
    }

    /**
     * Misc UI tweak
     */
    // private handleScroll = () => {
    //     const { scrolled } = this.state;
    //     const newIsScrolled = window.scrollY >= Constants.MENU_DROP_TRIGGER_HEIGHT;
    //     if(newIsScrolled !== scrolled) {
    //         this.setState({ scrolled: newIsScrolled });
    //     }
    // }

    /**
     * Misc UI tweak
     */
    // private handleResize = () => {
    //     this.setState({ contentWidth: window.innerWidth - Constants.NEVER_DO_THIS });
    //     this.handleScroll();
    // }
}