import * as React from "react";

export default class SaliTreeTRoot extends React.Component<{}, {}> {
    
    render() {
        return (
            <div className="root-piece-base">
                <div className="root-piece-t-left"/>
                <div className="root-piece-t-right"/>
            </div>
        );
    }
}