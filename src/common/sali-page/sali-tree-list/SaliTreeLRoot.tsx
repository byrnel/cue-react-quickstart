import * as React from "react";

export default class SaliTreeLRoot extends React.Component<{}, {}> {
    
    render() {
        return (
            <div className="root-piece-base">
                <div className="root-piece-l-top"/>
                <div className="root-piece-l-bottom"/>
            </div>
        );
    }
}