import * as React from "react";
import Constants from "./Constants";
require("./sali-page-style.css");

export interface ISaliContainerProps {
    className?: string;
    isLoading?: boolean;
    networkError?: boolean;
}

interface ISaliContainerState {
    scrolled?: boolean;
}

export default class SaliContainer extends React.Component<ISaliContainerProps, ISaliContainerState> {
    constructor(props: ISaliContainerProps) {
        super(props);

        this.state = {
            scrolled: false
        };
    }
    
    componentDidMount() {
        // window.addEventListener("scroll", this.handleScroll);
    }

    componentWillUnmount() {
        // window.removeEventListener("scroll", this.handleScroll);
    }

    render() {

        const {
            scrolled
        } = this.state;

        const {
            className,
            children
        } = this.props;

        const dynamicStyle = scrolled
            ? Constants.CONTENT_STYLE_DROPPED
            : {};

        return (
            <div
                className={"sali-content " + className}
                style={dynamicStyle} 
            >
                {children}
            </div>
        );
    }

    // private handleScroll = () => {
    //     const { scrolled } = this.state;
    //     const newIsScrolled = window.scrollY >= Constants.MENU_DROP_TRIGGER_HEIGHT;
    //     if(newIsScrolled !== scrolled) {
    //         this.setState({ scrolled: newIsScrolled });
    //     }
    // }
}