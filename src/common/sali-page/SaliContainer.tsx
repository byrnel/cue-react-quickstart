import * as React from "react";
import { SaliMenu } from "./";
require("./sali-page-style.css");

export interface ISaliContainerProps {
    className?: string;
}

interface ISaliContainerState {

}

export default class SaliContainer extends React.Component<ISaliContainerProps, ISaliContainerState> {

    hasMenu: boolean = false;
    
    constructor(props: ISaliContainerProps) {
        super(props);

        this.state = {
            scrolled: false,
            contentWidth: 0
        };

        this.hasMenu = this.containerHasMenuChild();
    }

    render() {
        const {
            className,
            children
        } = this.props;

        return (
            <div className={"ui fluid container " + className} >
                {children}
            </div>
        );
    }

    private containerHasMenuChild() {
        let hasMenu: boolean = false;
        React.Children.forEach(this.props.children, (child: React.ReactNode) => {
            if (child && ((child as JSX.Element).type === SaliMenu)) {
                hasMenu = true;
            }
        });
        return hasMenu;
    }
}