import * as React from "react";
require("./sali-page-style.css");

export interface ISaliSegmentProps {
    className?: string;
}

export default class SaliSegment extends React.Component<ISaliSegmentProps, {}> {
    constructor(props: ISaliSegmentProps) {
        super(props);
    }

    render() {
        const {
            children,
            className
        } = this.props;
        
        return (
            <div className={"sali-segment " + className}>
                {children}
            </div>
        );
    }
}