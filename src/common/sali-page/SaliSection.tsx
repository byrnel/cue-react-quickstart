import * as React from "react";
require("./sali-page-style.css");

export interface ISaliSectionProps {
    className?: string;
    header?: string;
}

export default class SaliSection extends React.Component<ISaliSectionProps, {}> {
    constructor(props: ISaliSectionProps) {
        super(props);
    }

    render() {
        const {
            children,
            className,
            header
        } = this.props;
        
        return (
            <div className={"sali-section " + className}>
                {header && <h1>{header}</h1>}
                <div className="sali-section-inner">
                    {children}
                </div>
            </div>
        );
    }
}