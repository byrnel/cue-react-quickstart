
import * as React from "react";
import { Breadcrumb } from "semantic-ui-react";
import { Link } from "react-router-dom";

export interface IBreadCrumbItem {
    href: any;
    name: string;
    active?: boolean;
}

export interface IBreadCrumProps {
    breadcrumbs: Array<IBreadCrumbItem>;
}

// TODO: Replace existing breadcrum within SALI Header with this
// React needs to be upgraded from 15 => 16 + to take advantage of
// ==> <React.Fragment>
// Using CSS to style breadcrum such as adding ">"
export function GenerateBreadCrumb(props: IBreadCrumProps) {
    return (
        <Breadcrumb id="breadcrumbs">
            <h2 className="print-version">You are here:&nbsp; </h2>
            {generateCrumContent(props)}
        </Breadcrumb>
    );
}

// Workaround to address adding multiple elments
function generateCrumContent(props: IBreadCrumProps) {
    let crums = [];
    for (let i = 0; i < props.breadcrumbs.length; i++) {
        if (i + 1 === props.breadcrumbs.length) {
            crums.push(
                <Breadcrumb.Section
                    to={props.breadcrumbs[i].href}
                    active={true}
                > {props.breadcrumbs[i].name}
                </Breadcrumb.Section>);
        } else {

            if (!props.breadcrumbs[i].active) {
                crums.push(
                    <Breadcrumb.Section
                        to={props.breadcrumbs[i].href}
                    > {props.breadcrumbs[i].name}
                    </Breadcrumb.Section>);
            } else {
                crums.push(
                    <Breadcrumb.Section
                        as={Link}
                        to={props.breadcrumbs[i].href}
                    >{props.breadcrumbs[i].name}
                    </Breadcrumb.Section>
                );
            }

            crums.push(<Breadcrumb.Divider icon="right angle" />
            );
        }
    }
    return crums;
}