import * as React from "react";

export default class SaliBypassBlock extends React.Component<{}, {}> {
    
    render() {
        
        return (
            <main id="main-content" className="bypass-block-target" tabIndex={-1}>
                {this.props.children}
            </main>
        );
    }
}
