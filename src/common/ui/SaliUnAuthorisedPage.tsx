import * as React from "react";
import { observer } from "mobx-react";
import { Container } from "semantic-ui-react";
import { Redirect, RouteComponentProps } from "react-router";
import SaliNavigation from "../../common/ui/SaliNavigation";
import NetworkError from "../../common/NetworkError";

interface ISaliUnAuthorisedPageState {
    messageVisible: boolean;
    dismissed: boolean;
}

@observer
export class SaliUnAuthorisedPage extends React.Component<RouteComponentProps<any>, ISaliUnAuthorisedPageState> {
    constructor(props: RouteComponentProps<any>) {
        super(props);

        this.state = {
            messageVisible: true,
            dismissed: false
        };

        this.handleDismiss = this.handleDismiss.bind(this);
    }

    handleDismiss = () => {
        this.setState({ dismissed: true });
    }

    render() {
        const { from } = this.props.location.state || "/";
        const { dismissed, messageVisible } = this.state;

        const header:string = `Access to ${from.pathname} is restricted`;
        
        return (
            <Container>
                {dismissed && (<Redirect to={SaliNavigation.LINKS_REGISTERED.HOME.LINK.GET_LINK()} />)}
                {from && messageVisible && (
                    <NetworkError header={header} message="You do not have the relevant permissions to perform this action."/>
                )}
            </Container>
        );
    }
}

export default SaliUnAuthorisedPage;