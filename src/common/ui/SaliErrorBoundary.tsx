
import * as React from "react";
import { Logger } from "../../services";
import NetworkError from "../../common/NetworkError";

export interface ISaliErrorBoundaryProps {
    hasError: boolean;
}

export interface ISaliErrorBoundaryState {
    hasError: boolean;
}

export class SaliErrorBoundary extends React.Component<ISaliErrorBoundaryProps, ISaliErrorBoundaryState> {
    error: string;
    info: string;

    constructor(props: ISaliErrorBoundaryProps) {
        super(props);
        this.state = { hasError: props.hasError };
    }

    componentDidCatch(error: any, info: any) {
        // Display fallback UI
        this.error = error;
        this.info = info;
        this.setState({ hasError: true });
        // You can also log the error to an error reporting service
        //   logError(error, info);
        Logger.error(error + ": " + info);
    }

    render() {
        if (this.state.hasError) {
            // You can render any custom fallback UI. 
            return <NetworkError header={this.error} message={this.info} />;
        }
        return this.props.children;
    }
}