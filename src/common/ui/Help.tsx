import * as React from "react";
import { Container, Grid, Message } from "semantic-ui-react";
import { SaliHelpLinks } from "./SaliHelpLinks";
import { NavLink } from "react-router-dom";

// const HELP_PATH_FILE_NAME:string = "../../assets/docs/sali_user_manual.pdf";

function requireSaliUserGuideHelp() {
    delete require.cache[require.resolve("../../assets/docs/sali_user_manual.pdf")]; // Variables did not work here. Needs further investigations
    return require("../../assets/docs/sali_user_manual.pdf");
}

export class Help extends React.Component<{}, {}> {

    render() {

        const CreateHelpLinks = () => (
            <ul>
                <li><b>{SaliHelpLinks.SEARCHING_NAVIGATION_OTIONS.TITLE}</b>
                    <ul>
                        {SaliHelpLinks.SEARCHING_NAVIGATION_OTIONS.LINKS.map((lk, index) => (
                            <li>
                                <a className="item" key={index} href={lk.href} rel={lk.rel} target={lk.target}>{lk.name}</a>
                            </li>
                        ))}
                    </ul>
                </li>
                <li><b>{SaliHelpLinks.SITE_FEATURES.TITLE}</b>
                    <ul>
                        {SaliHelpLinks.SITE_FEATURES.LINKS.map((lk, index) => (
                            <li>
                                <a className="item" key={index} href={lk.href} rel={lk.rel} target={lk.target}>{lk.name}</a>
                            </li>
                        ))}
                    </ul>
                </li>
            </ul>
        );
        
        const SaliHelpContent = () => (
            <Container textAlign="left" fluid={true}>
                <Grid stackable={true} stretched={true}>
                    <Grid.Row>
                        <Grid.Column>
                            <Message
                                icon="help circle"
                                header="Help"
                                attached={true}
                            />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <Container fluid={true}>
                                <p>To find out more about our website and its features please select from the links below or from the menu to the left. </p>
                                <NavLink to={requireSaliUserGuideHelp()} target="_blank" className="ui green button">View User Guide (PDF, 619 KB)</NavLink>
                                <br />
                            </Container>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <Container fluid={true}>
                                <CreateHelpLinks />
                            </Container>
                        </Grid.Column>
                    </Grid.Row>
                </Grid >
            </Container>
        );
      
        return (
            <SaliHelpContent />
        );
    }
}

export default Help;
