import * as React from "react";
import { CSSProperties } from "react";
import {
    MenuItemProps,
    DropdownProps,
    DropdownItemProps
} from "semantic-ui-react";
import { RouteComponentProps } from "react-router-dom";
import { UserStore } from "../../stores/";

// Model Application JSX Elements
import UsersList from "../../features/manage-security/users/UsersList";
import GroupsList from "../../features/manage-security/groups/GroupsList";
import SignInComponent from "../../features/sign-in/SignInComponent";
import CreateProjectMetadataComponent from "../../features/project/create-project-metadata/CreateProjectMetadataComponent";

import Home from "./Home";
import About from "./About";
import Contact from "./Contact";
import SiteMap from "./SiteMap";
import Help from "./Help";
import SaliUnAuthorisedPage from "../../common/ui/SaliUnAuthorisedPage";
import { SaliTabControl } from "../../common/ui";
import { IBreadCrumbItem } from "../../common/ui/SaliHelpers";
import * as moment from "moment";

// Site Mapping Structure 
export interface ISiteMapConfig {
    NAME: string;
    HREF: string;
    TREE: ISiteMapConfig[];
    DISABLED?: boolean | false; // Some links which are not valid are disabled . Such as Manage/Publications (there is no landing page)
}
import * as Activities from "../../constants/Activities";

export interface IAppBranding {
    NAME: string;
    ICON: string;
    APP_STYLE: CSSProperties; // Configure CSS for branding
    IMAGE: string;
}

type TargetType = "_blank" | "_self" | "_parent" | "_top";

export interface INaviLinkFooter {
    LINK: INaviGeneral;
    FOOTER_BRAND: string;
    LINKS: INaviGeneral[];
}

// Used for general links such as footer
// Search area
export interface INaviGeneral {
    NAME: string;
    HREF: string;
    TARGET: TargetType;
    LEGAL: boolean;
    ACCESS_KEY?: number;
    REL?: string;
    TAB_INDEX?: number;   
}

export interface INaviQldGov extends INaviGeneral {
    IMAGE: { 
        SRC: string; 
        ALT: string;
    };
}

export interface ISaliProduct extends INaviGeneral {
    DESC: string;
    DISABLED?: boolean;
    IMAGE: { 
        SRC: string; 
        ALT: string;
    };
}

export interface INaviLinkSearch {
    DISPLAY: boolean; 
    TARGET:string;
    ACTION: string;
    INPUTS: Array<INaviSearchInput>;
}

export interface INaviSearchInput {
    TYPE: string;
    NAME: string;
    VALUE:string;
}

export interface INaviTopLinks {
    SITEMAP: INaviComponentLink;
    CONTACT: INaviComponentLink;
    HELP: INaviComponentLink;
    LINKS: Array<INaviLinkGroups>;
    ROUTES: Array<INaviRoutes>;
}

export interface INaviLeftLinks {
    HOME: INaviComponentLink;
    ABOUT: INaviComponentLink;
    PROJECT: INaviDropDownConfig;
}

export interface INaviRightLinks {
    SIGNIN: INaviComponentLink;
    MANAGE: INaviDropDownConfig;
    activeItem?: string;
    handleOnClick?(e: React.MouseEvent<HTMLAnchorElement>, d: MenuItemProps): void;
}

export interface INaviLinkLayout {
    SEARCH_AREA: INaviTopLinks;
    LEFT: INaviLeftLinks;
    RIGHT: INaviRightLinks;
    // NOT_FOUND_ROUTE: React.ComponentType<any> | React.StatelessComponent<any>;
}

export interface INaviLinkGroups {
    GROUP_NAME: string; // Name for the top level group
    LINKS: Array<INavigationLinkConfig>;
}

export interface INaviBase {
    BASE_URL: string;
    // LINKS: Array<INavigationLinkConfig>;
    ROUTES: Array<INaviRoutes>;
    LINKS: Array<INaviLinkGroups>;
    ICON?: string;
    activeItem?: string;
    handleOnClick?(e: React.MouseEvent<HTMLAnchorElement>, d: MenuItemProps): void;
}

export interface INaviDropDownConfig extends INaviBase {
    DROP_DOWN_NAME: string;
    dropDownOnClick?(e: React.KeyboardEvent<HTMLElement>, data: DropdownProps): void;
    dropDownOnItemClick?(e: React.MouseEvent<HTMLDivElement>, data: DropdownItemProps): void; 
}

export interface INaviGeneralLinkConfig extends INaviBase {
    NAME: string;
}

export interface INaviDropDownGroupConfig extends INaviDropDownConfig {
    SUB_MENUS: Array<INaviDropDownConfig>;
}

// This interface determines the relevant properties
// and configuration settings for a link. Explanation
// of the settings are below
export interface INaviLinkModel {
    NAME: string; // This is the link name
    HREF: string; // i.e /product/create
    SITEMAP?: boolean | false; // If this link is to show up in site map or not.
    SHOW_BREADCRUMS?: boolean | false; // If the link is to show up in breadcrums or not
    /**
     * If this is not-null, the user must be permitted to carry out at least one
     * of the activities in this list, otherwise the navigation link will not be
     * shown to the user.
     */
    ACTIVITIES?: string[]; // Used to determine, roles , permissions required to access this link. 
    ENFORCE_BASE_LINK?: boolean | false;
    ICON?: string; // The icon to display for this link
    JS_LINK_TAG?: string; // i.e used to configure test links such as js-create-raster-link ect. 
    ROUTER_PATH?: string | undefined;
    DISABLED_LINK?: boolean | false; // Used for Sitemap to disable a parent link when user has not access .
    TAB_INDEX?: number; // Unique Id for the Tab index on the page. Auto generated by generateTabIndexes(data: INaviLink)
    AUTHORISED(): boolean; // A callback function that determines the user's permission to access a link and route
}

// Addition configuration for the base link model
export interface INavigationLinkConfig extends INaviLinkModel {
    BREADCRUMS: IBreadCrumbItem[]; // Breadcrums for links on the navigation branch
    activeItem?: string; // Used for the state of the link menu item
    GET_ROUTE(): string; // Callback function to determine the route for the component
    GET_LINK(base?: string): string; // Callback function to determine the link for the component
    handleOnClick?(e: React.MouseEvent<HTMLAnchorElement>, data: MenuItemProps): void; // Handle click event on menu component
}

export interface INaviComponentLinkConfig extends INavigationLinkConfig {
    ROUTE: INaviRouteConfig;
}

// New Configuration
export interface INaviComponentLink {
    TAB_LAYOUT: boolean;
    LINK: INavigationLinkConfig;
    ROUTE: INaviRouteConfig;
}

export interface INaviRoutes {
    ROUTE: INaviRouteConfig;
}

export interface INaviRouteGroups {
    COMBINED: boolean; // if true, then all links will be displayed in the side navigation within the SaliTabControl otherwise components and links will be displayed individually  
    // SIGNOUT: React.ComponentClass<{}>;
    GROUPS: INaviGroupRoutes[];
}

export interface INaviGroupRoutes {
    BASE_PATH: string;
    TAB_COMPONENT: React.ComponentType<any> | React.ComponentType<RouteComponentProps<any>> | React.StatelessComponent<any>;
    // LINKS: INavigationLinkConfig[];
    LINKS: INaviLinkGroups[];
    ROUTES: INaviRoutes[];
    EXACT?: boolean;
    INCLUDE_ALL_LINKS?: boolean | false;
    GROUPNAME?: string | undefined;
}

export interface ITabGroupProps {
    groupname: string;
    links: INaviLinkGroups[];
    base_link: string;
}

// Main LINK_? interface configuration 
export interface INaviBaseLinks {
    BASE: INavigationLinkConfig;
    LINKS: {
    };
}

export interface INaviLinkModelSecurity {
    USERS: INavigationLinkConfig;
    GROUPS: INavigationLinkConfig;
    GROUPNAME?: string;
}

export interface INaviLinkModelManage extends INaviBaseLinks {
    LINKS: {
        SECURITY: INaviLinkModelSecurity;
    };
    ROUTES: Array<INaviRoutes>;
}

export interface INavigationHomeLinks extends INaviBaseLinks {
    LINKS: {
        // ABOUT: INavigationLinkConfig,
        // CONTACT: INavigationLinkConfig,
        // SITEMAP: INavigationLinkConfig,
        // HELP: INavigationLinkConfig,
    };
}

export interface INaviLinkModelProject extends INaviBaseLinks {
    LINKS: {
        ADD: INavigationLinkConfig;
    };
    ROUTES: Array<INaviRoutes>;
}

export interface INaviLinkModelUsers {
    CREATE: INavigationLinkConfig;
    SEARCH: INavigationLinkConfig;
    EDIT: INavigationLinkConfig;
    SHOW: INavigationLinkConfig;
}

// Main route configuration 
export interface INaviRouteConfig {
    COMPONENT: React.ComponentType<any> | React.ComponentType<RouteComponentProps<any>> | React.StatelessComponent<any>;
    BREADCRUMS: IBreadCrumbItem[]; // We pass the breadcrums dynamically when we load the routes in ConfigureRoutes.jsx
    INCLUDE_PRINT_AREA: boolean; // Include print area wrapper component
    PATH?: string | undefined; // Route path
    LASTUPDATED: moment.Moment; // This is the date that the component was last updated. Default's to current datetime/ can be overrided as required
    EXACT?: boolean; // Path is exact or not
    SECURE?: boolean; // require's the user to login to access this component
    DISPLAYLASTUPDATED?: boolean; // whether or not to show the last updated date for a component or not
    AUTHORISED(): boolean; // A callback function this gets assigned the same function call as defined on INaviComponentLink
}

export interface ISNaviLinkGroupsCombinedConfig {
    GROUP_NAME: string;
    BASE_LINK: string;
    LC: INaviDropDownConfig; // Link Config
}

// Link register is for registering links used throughout the site
export interface INaviLinksRegister {
    QLDGOV: INaviQldGov;
    SEARCH: INaviLinkSearch;
    SIGNIN: INaviComponentLink;
    SITE_MAP: INaviComponentLink;
    CONTACT: INaviComponentLink;
    HELP: INaviComponentLink;
    HOME: INaviComponentLink;
    ABOUT: INaviComponentLink;
    PROJECT: INaviLinkModelProject;
    MANAGE: INaviLinkModelManage;
    FOOTER: INaviLinkFooter;
    UNAUTHRORISED_PAGE: INaviComponentLink;
}

export interface INaviLink {
    HOME: INavigationHomeLinks;
    ABOUT: INavigationLinkConfig;
    PROJECT: INaviLinkModelProject;
    CONTACT: INavigationLinkConfig;
    SITEMAP: INavigationLinkConfig;
    HELP: INavigationLinkConfig;
    MANAGE: INaviLinkModelManage;
    SIGNIN: INaviComponentLink;
}

type LinkComponentType = INaviLink | INaviComponentLinkConfig | INaviDropDownConfig;
type DataShape = INaviDropDownConfig | INaviComponentLink | INavigationLinkConfig | INaviLinkModelSecurity | INaviBaseLinks | INavigationHomeLinks | INavigationLinkConfig | INaviGeneral | INaviLinkModelProject;

export interface IBreadCrums {
    showprint: boolean | false;
    crums: Array<IBreadCrumbItem> | undefined;
    print(): void;
}

// Function used to generate the breadcrums
function generateBreadCrums(root: INavigationLinkConfig, branch: INavigationLinkConfig[]): Array<IBreadCrumbItem> {
    let crums: Array<IBreadCrumbItem> = new Array<IBreadCrumbItem>();
    if (branch.length !== 0) {
        branch.filter(x => x.SHOW_BREADCRUMS).map((li, index) => {
            crums.push({
                href: li.GET_LINK(),
                name: li.NAME,
                active: li.DISABLED_LINK ? false : true
            });
        });

        if (crums.length !== 0) {
            crums.unshift({
                href: root.GET_LINK(),
                name: root.NAME,
                active: root.DISABLED_LINK ? false : true
            });
        }
    }

    return (
        crums
    );
}

// function roles(activities?: string[]): string[] {
//     var activitiesInSection = activities || [];
//     return (activitiesInSection);
// }

function generateConfigLink(linkConfig: INaviLinkModel, baseHref?: string): INavigationLinkConfig {
    let configLink: INavigationLinkConfig = {
        NAME: linkConfig.NAME,
        HREF: linkConfig.HREF,
        ROUTER_PATH: linkConfig.ROUTER_PATH ? linkConfig.ROUTER_PATH : linkConfig.HREF,
        SITEMAP: linkConfig.SITEMAP,
        ICON: linkConfig.ICON,
        ACTIVITIES: linkConfig.ACTIVITIES,
        SHOW_BREADCRUMS: linkConfig.SHOW_BREADCRUMS,
        ENFORCE_BASE_LINK: linkConfig.ENFORCE_BASE_LINK,
        JS_LINK_TAG: linkConfig.JS_LINK_TAG,
        DISABLED_LINK: linkConfig.DISABLED_LINK,
        AUTHORISED: linkConfig.AUTHORISED,
        TAB_INDEX: 0,
        GET_ROUTE: function () {
            let routeIt = this.ROUTER_PATH ? this.ROUTER_PATH : this.HREF;
            return (
                this.ENFORCE_BASE_LINK ? baseHref + routeIt : routeIt
            );
        },
        GET_LINK: function () {
            return (
                this.ENFORCE_BASE_LINK ? baseHref + this.HREF : this.HREF
            );
        },
        BREADCRUMS: []
    };
    return configLink;
}

function generateConfigRoute(baseconfig: INaviRouteConfig): INaviRouteConfig {
    let configRoute: INaviRouteConfig = {
        ...baseconfig
    };
    return configRoute;
}

function generateComponentLink(tabLayout: boolean, linkConfig: INaviLinkModel, routeConfig: INaviRouteConfig): INaviComponentLink {

    let componentLink: INaviComponentLink = {
        TAB_LAYOUT: tabLayout,
        LINK: generateConfigLink(linkConfig),
        ROUTE: generateConfigRoute(routeConfig)
    };

    componentLink.ROUTE.PATH = componentLink.LINK.GET_ROUTE();
    componentLink.ROUTE.AUTHORISED = componentLink.LINK.AUTHORISED;
    return componentLink;
}

function checkAuthorised(userstate: boolean): boolean {
    return userstate;
}

function generateLinkModelManage(LINK_HOME: INaviComponentLink): INaviLinkModelManage {

    let baseConfig = { NAME: "Manage", AUTHORISED: () => checkAuthorised(UserStore.isAuthorized(Activities.manageSecurity)), HREF: "/manage", ICON: "mail", DISABLED_LINK: true, SHOW_BREADCRUMS: true, SITEMAP: true, ACTIVITIES: [Activities.manageSecurity] };
    let baseLink = generateConfigLink(baseConfig);
    let LINK_MANAGE: INaviLinkModelManage = {
        BASE: baseLink,
        LINKS: {
            SECURITY: {
                GROUPNAME: "Security",
                USERS: generateConfigLink({ NAME: "Users", AUTHORISED: () => checkAuthorised(UserStore.isAuthorized(Activities.manageSecurity)), HREF: "/security/users", ICON: "add user", SITEMAP: true, ENFORCE_BASE_LINK: true, SHOW_BREADCRUMS: true, ACTIVITIES: [Activities.manageSecurity] }, baseConfig.HREF),
                GROUPS: generateConfigLink({ NAME: "Groups", AUTHORISED: () => checkAuthorised(UserStore.isAuthorized(Activities.manageSecurity)), HREF: "/security/groups", ICON: "group", SITEMAP: true, ENFORCE_BASE_LINK: true, SHOW_BREADCRUMS: true, ACTIVITIES: [Activities.manageSecurity] }, baseConfig.HREF)
            }
        },
        ROUTES: []
    };

    LINK_MANAGE.BASE.BREADCRUMS = generateBreadCrums(LINK_MANAGE.BASE, []);
    LINK_MANAGE.LINKS.SECURITY.USERS.BREADCRUMS = generateBreadCrums(LINK_HOME.LINK, [LINK_MANAGE.BASE, LINK_MANAGE.LINKS.SECURITY.USERS]);
    LINK_MANAGE.LINKS.SECURITY.GROUPS.BREADCRUMS = generateBreadCrums(LINK_HOME.LINK, [LINK_MANAGE.BASE, LINK_MANAGE.LINKS.SECURITY.GROUPS]);

    LINK_MANAGE.ROUTES = [
        {
            ROUTE: generateConfigRoute({ LASTUPDATED: moment(), PATH: LINK_MANAGE.LINKS.SECURITY.USERS.GET_ROUTE(), COMPONENT: UsersList, INCLUDE_PRINT_AREA: true, SECURE: true, EXACT: true, BREADCRUMS: LINK_MANAGE.LINKS.SECURITY.USERS.BREADCRUMS, AUTHORISED: () => LINK_MANAGE.LINKS.SECURITY.USERS.AUTHORISED() })
        },
        {
            ROUTE: generateConfigRoute({ LASTUPDATED: moment(), PATH: LINK_MANAGE.LINKS.SECURITY.GROUPS.GET_ROUTE(), COMPONENT: GroupsList, INCLUDE_PRINT_AREA: true, SECURE: true, EXACT: true, BREADCRUMS: LINK_MANAGE.LINKS.SECURITY.GROUPS.BREADCRUMS, AUTHORISED: () => LINK_MANAGE.LINKS.SECURITY.GROUPS.AUTHORISED() })
        }
    ];

    return LINK_MANAGE;
}

function generateLinkProject(LINK_HOME: INaviComponentLink): INaviLinkModelProject {

    let baseConfig = { NAME: "Project", AUTHORISED: () => checkAuthorised(true), HREF: "/project", ICON: "leanpub", SITEMAP: true, DISABLED_LINK: true, SHOW_BREADCRUMS: true, ACTIVITIES: [Activities.createProjectMetadata] };
    let baseLink = generateConfigLink(baseConfig);

    let LINK_PROJECT: INaviLinkModelProject = {
        BASE: baseLink,
        LINKS: {
            ADD: generateConfigLink({ NAME: "Add Project Metadata", AUTHORISED: () => checkAuthorised(UserStore.isAuthorized(Activities.createProjectMetadata)), JS_LINK_TAG: "js-create-project-link",HREF: "/metadata/add", ICON: "cube", SITEMAP: true, ENFORCE_BASE_LINK: true, SHOW_BREADCRUMS: true, ACTIVITIES: [Activities.createProjectMetadata] }, baseConfig.HREF)
        },
        ROUTES: []
    };

    LINK_PROJECT.BASE.BREADCRUMS = generateBreadCrums(LINK_PROJECT.BASE, []);
    LINK_PROJECT.LINKS.ADD.BREADCRUMS = generateBreadCrums(LINK_HOME.LINK, [LINK_PROJECT.BASE, LINK_PROJECT.LINKS.ADD]);

    LINK_PROJECT.ROUTES = [
        {
            ROUTE: generateConfigRoute({ LASTUPDATED: moment(), PATH: LINK_PROJECT.LINKS.ADD.GET_ROUTE(), COMPONENT: CreateProjectMetadataComponent, INCLUDE_PRINT_AREA: true, SECURE: true, EXACT: true, BREADCRUMS: LINK_PROJECT.LINKS.ADD.BREADCRUMS, AUTHORISED: () => LINK_PROJECT.LINKS.ADD.AUTHORISED() })
        }
    ];

    return LINK_PROJECT;
}

class SaliNavigation {

    tabIndexCounter: number = 1; // Start at 1
    TypeCheck: LinkComponentType;

    ORGANISATION = "Department of Environment and Science";
    APP_NAME = "CUE QuickStart";
    APP_ICON = "icon globe";

    PRODUCTS: ISaliProduct[] = [
                    { NAME: "MODELd",LEGAL: false,DISABLED: true, HREF: "https://modeld.lands.resnet.qg", IMAGE: {ALT: "MODELd", SRC: require("../../assets/img/sali/products/model-square.png")}, TARGET: "_self", DESC: "MODELd provides the ability to store, manage and publish soil spatial information." },
                    { NAME: "INFORMd",LEGAL: false, HREF: "https://informd.lands.resnet.qg", IMAGE: {ALT: "INFORMd", SRC: require("../../assets/img/sali/products/informed-square.png")}, TARGET: "_blank", DESC: "INFORMd is the repository for knowledge products generated by soil activities." },
                    { NAME: "SERVEd - Soil Chemistry",LEGAL: false, HREF: "https://soil-chem.information.qld.gov.au",  IMAGE: {ALT: "SERVEd - Soil Chemistry", SRC: require("../../assets/img/sali/products/served-soilchemistry-square.png")}, TARGET: "_blank", DESC: "SERVEd - Soil Chemistry API enables soils chemistry data to be more easily available to the public (self-service)" },
                    { NAME: "SERVEd - Land Suitability",LEGAL: false, HREF: "https://land-suit.information.qld.gov.au",  IMAGE: {ALT: "SERVEd - Land Suitability", SRC: require("../../assets/img/sali/products/served-landsuitability-square.png")}, TARGET: "_blank", DESC: "SERVEd - Land Suitability API enables land suitability data to be more easily available to the public (self-service)" }
    ];

    NAVIGATIONAL_LINKS: INaviLink;
    NAVIGATIONAL_LAYOUT_LINKS: INaviLinkLayout;
    APP_BANNER_BRANDING: IAppBranding;

    LINKS_REGISTERED: INaviLinksRegister;
    ALL_LINKS: INavigationLinkConfig[] = [];
    ALL_ROUTES: INaviRouteConfig[] = [];

    constructor() {
        this.APP_BANNER_BRANDING = {
            NAME: this.APP_NAME,
            ICON: this.APP_ICON,
            IMAGE: require("../../assets/Icons/informed-round.png"),
            APP_STYLE: {
                backgroundColor: "", // Dont define the colors here, they are configured in sui.css (in ui folder)
                color: ""
            }
        };

        this.LINKS_REGISTERED = this.genereateLinksRegistered();
        this.NAVIGATIONAL_LAYOUT_LINKS = this.generateNavigationEntries(this.LINKS_REGISTERED);
        this.NAVIGATIONAL_LINKS = this.generateNavigationLinks(this.LINKS_REGISTERED);
        this.generateTabIndexes(this.LINKS_REGISTERED);
        // Lets load these off the bat as we will be referencing these later    
        // this.ALL_LINKS = this.loadLinks();
        this.ALL_ROUTES = this.loadStandardRoutes();
    }

    public loadStandardRoutes(): INaviRouteConfig[] {
        let routes = this.process<INaviRouteConfig>(this.NAVIGATIONAL_LAYOUT_LINKS);
        return routes;
    }

    // There is where tabbed routes are configured and loaded
    // We need to manaually control this as there is some decision making involved
    // also defines whihc tab control to render the compoent in
    public loadTabbedRoutes(combined: boolean): INaviRouteGroups {
        let tabbedRoutes = { COMBINED: combined } as INaviRouteGroups;
        tabbedRoutes.GROUPS = [];
        tabbedRoutes.GROUPS.push(this.loadTabGroupItem(this.LINKS_REGISTERED.PROJECT.BASE, this.NAVIGATIONAL_LAYOUT_LINKS.LEFT.PROJECT, SaliTabControl));
        tabbedRoutes.GROUPS.push(this.loadTabGroupItem(this.LINKS_REGISTERED.MANAGE.BASE, this.NAVIGATIONAL_LAYOUT_LINKS.RIGHT.MANAGE, SaliTabControl));
      
        // Manually generating About Link, Route structure. This configuration is defined gnerally in => function generateNavigationEntries (see below at end)
        let ABOUT_LINK = [
            {
                GROUP_NAME: "", // We dont need a group name here
                LINKS: [
                    this.NAVIGATIONAL_LAYOUT_LINKS.LEFT.ABOUT.LINK,
                ]
            }
        ];
        
        let ABOUT_ROUTE = [
            {
                ROUTE: this.NAVIGATIONAL_LAYOUT_LINKS.LEFT.ABOUT.ROUTE
            }
        ];
        
        // For routing we need to add About Route firt specifing base linke route
        tabbedRoutes.GROUPS.push(this.loadTabGroupComponentItem(ABOUT_LINK, ABOUT_ROUTE, this.NAVIGATIONAL_LAYOUT_LINKS.LEFT.ABOUT.LINK)); // Adding base link to address routing issue with similar routes below. Added before following links to address mathing of routes. Also adding base link to itself
        tabbedRoutes.GROUPS.push(this.loadTabGroupComponentItem(this.NAVIGATIONAL_LAYOUT_LINKS.SEARCH_AREA.LINKS,this.NAVIGATIONAL_LAYOUT_LINKS.SEARCH_AREA.ROUTES));
        return tabbedRoutes;
    }

    public loadTabGroupItem(base: INavigationLinkConfig, nl: INaviDropDownConfig, component: React.ComponentType<any> | React.ComponentType<RouteComponentProps<any>> | React.StatelessComponent<any>): INaviGroupRoutes {
        let tg = { GROUPNAME: base.NAME, BASE_PATH: base.GET_ROUTE(), LINKS: [], ROUTES: [], TAB_COMPONENT: component } as INaviGroupRoutes;
        tg.LINKS = nl.LINKS;
        tg.ROUTES = nl.ROUTES;
        tg.EXACT = false;
        return tg;
    }

    // LINKS: Array<INaviLinkGroups>;
    // ROUTES: Array<INaviRoutes>;
    public loadTabGroupComponentItem(links: Array<INaviLinkGroups>, routes: Array<INaviRoutes>,base?: INavigationLinkConfig): INaviGroupRoutes {
        let tg = { GROUPNAME: "", BASE_PATH: base ? base.GET_LINK() : "/", LINKS: [], ROUTES: [], TAB_COMPONENT: SaliTabControl } as INaviGroupRoutes;
        tg.LINKS = links;
        tg.ROUTES = routes;
        return tg;
    }

    // Generic loader for INaviLinkLayout
    public process<T>(data: INaviLinkLayout): T[] {
        let items: T[] = [];

        // Handle null case.
        if (data === null) {
            return items;
        }

        // we are interested in only objects to be searched
        if (data !== Object(data)) {
            return items;
        }

        if (data instanceof Array) {
            for (var i = 0; i < data.length; ++i) {
                items = items.concat(this.process(data[i]));
            }
            return items;
        }

        let keys = Object.keys(data);
        for (let propkey of keys) {
            if (data[propkey] !== undefined && data[propkey] !== null) {
                if (this.isINaviComponentLink(data[propkey])) {
                    let cl: INaviComponentLink = data[propkey];
                    // let route = JSON.parse(JSON.stringify(cl.ROUTE)) as T;
                    let route = <any>cl.ROUTE as T;
                    if (!cl.TAB_LAYOUT) {
                        items.push(route);
                    }
                }
                items = items.concat(this.process(data[propkey]));
            }
        }
        return items;
    }

    // Dynamically locate and load the site maps
    public loadSiteMap(): ISiteMapConfig[] {
        let sitemap = this.processSiteMap(this.NAVIGATIONAL_LINKS);
        return sitemap;
    }

    public processSiteMap(data: INaviLink, basehref?: string | undefined): ISiteMapConfig[] {
        let items: ISiteMapConfig[] = [];

        // Handle null case.
        if (data === null) {
            return items;
        }

        // we are interested in only objects to be searched
        if (data !== Object(data)) {
            return items;
        }

        if (data instanceof Array) {
            for (var i = 0; i < data.length; ++i) {
                items = items.concat(this.processSiteMap(data[i]));
            }
            return items;
        }

        Object.keys(data).forEach(k => {
            if (data[k] !== undefined && data[k] !== null) {
                if (this.isINaviBaseLinks(data[k])) {
                    let ci: INaviBaseLinks = data[k];
                    if (ci.BASE.SITEMAP && ci.BASE.AUTHORISED()) {
                        items.push({
                            NAME: ci.BASE.NAME,
                            HREF: ci.BASE.HREF,
                            DISABLED: ci.BASE.DISABLED_LINK,
                            TREE: this.loadSiteMapItem(ci.BASE.HREF, ci.LINKS)
                        });
                    }
                } else if (this.isINavigationLinkConfig(data[k])) {
                    let ci: INavigationLinkConfig = data[k];
                    let baseLink = basehref ? basehref : "";
                    let link = ci.ENFORCE_BASE_LINK ? baseLink + ci.HREF : ci.HREF;
                    if (ci.SITEMAP && ci.AUTHORISED()) {
                        items.push({
                            NAME: ci.NAME,
                            HREF: link,
                            DISABLED: ci.DISABLED_LINK,
                            TREE: []
                        });
                    }
                } else {
                    items = items.concat(this.processSiteMap(data[k]));
                }
            }
        });
        return items;
    }

    private loadSiteMapItem(baseHref: string, data: any): ISiteMapConfig[] {
        let items: ISiteMapConfig[] = [];
        let keys = Object.keys(data);
        keys.forEach(k => {
            if (data[k] !== undefined && data[k] !== null) {
                if (this.isINavigationLinkConfig(data[k])) {
                    let ci: INavigationLinkConfig = data[k];
                    let link = ci.ENFORCE_BASE_LINK ? baseHref + ci.HREF : ci.HREF;
                    if (ci.SITEMAP && ci.AUTHORISED()) {
                        items.push({
                            NAME: ci.NAME,
                            HREF: link,
                            DISABLED: ci.DISABLED_LINK,
                            TREE: []
                        });
                    }
                } else {
                    items = items.concat(this.processSiteMap(data[k], baseHref)); // dig deeper
                }
            }
        });
        return items;
    }

    // Generate a unique id for the tabbing order
    // Required for WCAG Accessibility reasons
    private generateTabIndexes(data: INaviLinksRegister) {
         // Handle null case.
        if (data === null) {
            return;
        }

        // we are interested in only objects to be searched
        if (data !== Object(data)) {
            return;
        }

        if (data instanceof Array) {
            for (var i = 0; i < data.length; ++i) {
                this.generateTabIndexes(data[i]);
            }
            return;
        }

        Object.keys(data).forEach(k => {
            if (data[k] !== undefined && data[k] !== null) {
                if (data[k].TAB_INDEX !== undefined) {
                    // let ci: INavigationLinkConfig = data[k];
                    data[k].TAB_INDEX = this.tabIndexCounter++;
                } else {
                    this.generateTabIndexes(data[k]);
                }
            }
        });
        return;
    }

    private isINaviBaseLinks(structure: DataShape): structure is INaviBaseLinks | INavigationHomeLinks | INaviLinkModelManage | INaviLinkModelSecurity {
        return (<INaviBaseLinks>structure).BASE !== undefined;
    }

    private isINavigationLinkConfig(structure: DataShape): structure is INavigationLinkConfig {
        return (<INavigationLinkConfig>structure).ROUTER_PATH !== undefined;
    }

    private isINaviComponentLink(structure: DataShape): structure is INaviComponentLink {
        return (<INaviComponentLink>structure).TAB_LAYOUT !== undefined && (<INaviComponentLink>structure).ROUTE !== undefined;
    }

    // Links need to be registered first using this interface and configuration 
    private genereateLinksRegistered(): INaviLinksRegister {

        let LINK_QLD_GOV: INaviQldGov = {
            NAME: "QLD Gov",
            HREF: "https://www.qld.gov.au/",
            TARGET: "_blank",
            LEGAL: false,
            IMAGE: 
                { 
                    SRC: "https://static.qgov.net.au/assets/v2/images/skin/qg-coa.svg", 
                    ALT: "Queensland Government home" 
                }
        };

        let LINK_SEARCH: INaviLinkSearch = {
            DISPLAY: true,    
            ACTION : "//find.search.qld.gov.au/s/search.html",
            TARGET: "_blank",
            INPUTS: [
                {
                    TYPE: "hidden",
                    NAME: "num_ranks",
                    VALUE: "10"
                },
                {
                    TYPE: "hidden",
                    NAME: "tiers",
                    VALUE: "off"
                },
                {
                    TYPE: "hidden",
                    NAME: "collection",
                    VALUE: "qld-gov"
                },
                {
                    TYPE: "hidden",
                    NAME: "profile",
                    VALUE: "qld"
                },
                {
                    TYPE: "hidden",
                    NAME: "scope",
                    VALUE: "cueapp.qld.gov.au"
                }
            ]
        };

        let LINK_FOOTER: INaviLinkFooter = {
            LINK: {
                NAME: "Queensland Government",
                HREF: "https://www.qld.gov.au/",
                TARGET: "_blank",
                ACCESS_KEY: 1,
                LEGAL: true,
                TAB_INDEX: 0,
                REL: "noopener noreferrer"
            },
            FOOTER_BRAND: "© The State of Queensland (" + this.ORGANISATION + ") 2018",
            LINKS: [
                {
                    NAME: "Copyright",
                    HREF: "https://www.qld.gov.au/legal/copyright",
                    TARGET: "_blank",
                    LEGAL: true,
                    TAB_INDEX: 0,
                    REL: "noopener noreferrer"
                },
                {
                    NAME: "Disclaimer",
                    HREF: "https://www.qld.gov.au/legal/disclaimer",
                    TARGET: "_blank",
                    LEGAL: true,
                    TAB_INDEX: 0,
                    REL: "noopener noreferrer"
                },
                {
                    NAME: "Privacy",
                    HREF: "https://www.qld.gov.au/legal/privacy",
                    TARGET: "_blank",
                    LEGAL: true,
                    TAB_INDEX: 0,
                    REL: "noopener noreferrer"
                },
                {
                    NAME: "Accessibility",
                    HREF: "https://www.qld.gov.au/help/accessibility",
                    TARGET: "_blank",
                    LEGAL: false,
                    TAB_INDEX: 0,
                    REL: "noopener noreferrer"
                },
                {
                    NAME: "Jobs in Queensland Government",
                    HREF: "https://smartjobs.qld.gov.au",
                    TARGET: "_blank",
                    LEGAL: false,
                    TAB_INDEX: 0,
                    REL: "noopener noreferrer"
                },
                {
                    NAME: "Other language",
                    HREF: "https://www.qld.gov.au/help/languages",
                    TARGET: "_blank",
                    LEGAL: false,
                    TAB_INDEX: 0,
                    REL: "noopener noreferrer"
                }
            ]
        };
        
        let LINK_HOME: INaviComponentLink = generateComponentLink(false, { NAME: "Home", AUTHORISED: () => checkAuthorised(true), HREF: "/", ICON: "home", SITEMAP: true }, { LASTUPDATED: moment(), COMPONENT: Home, INCLUDE_PRINT_AREA: false, EXACT: true, BREADCRUMS: [], AUTHORISED: () => checkAuthorised(true) });
        LINK_HOME.LINK.BREADCRUMS = generateBreadCrums(LINK_HOME.LINK, []);
        LINK_HOME.ROUTE.BREADCRUMS = LINK_HOME.LINK.BREADCRUMS;

        let LINK_ABOUT: INaviComponentLink = generateComponentLink(true, { NAME: "About", AUTHORISED: () => checkAuthorised(true), HREF: "/about", ICON: "info", SITEMAP: true, SHOW_BREADCRUMS: true }, { DISPLAYLASTUPDATED: true, LASTUPDATED: moment("12/12/18"), COMPONENT: About, INCLUDE_PRINT_AREA: true, EXACT: false, BREADCRUMS: [], AUTHORISED: () => checkAuthorised(true) });
        LINK_ABOUT.LINK.BREADCRUMS = generateBreadCrums(LINK_HOME.LINK, [LINK_ABOUT.LINK]);
        LINK_ABOUT.ROUTE.BREADCRUMS = LINK_ABOUT.LINK.BREADCRUMS;

        let LINK_SITE_MAP: INaviComponentLink = generateComponentLink(true, { NAME: "Site map", AUTHORISED: () => checkAuthorised(true), HREF: "/sitemap", ICON: "map icon", SITEMAP: false, SHOW_BREADCRUMS: true }, { DISPLAYLASTUPDATED: true, LASTUPDATED: moment(), COMPONENT: SiteMap, INCLUDE_PRINT_AREA: true, EXACT: false, BREADCRUMS: [], AUTHORISED: () => checkAuthorised(true) });
        LINK_SITE_MAP.LINK.BREADCRUMS = generateBreadCrums(LINK_HOME.LINK, [LINK_SITE_MAP.LINK]);
        LINK_SITE_MAP.ROUTE.BREADCRUMS = LINK_SITE_MAP.LINK.BREADCRUMS;

        let LINK_CONTACT: INaviComponentLink = generateComponentLink(true, { NAME: "Contact", AUTHORISED: () => checkAuthorised(true), HREF: "/contact", ICON: "user icon", SITEMAP: true, SHOW_BREADCRUMS: true }, { DISPLAYLASTUPDATED: true, LASTUPDATED: moment(), COMPONENT: Contact, INCLUDE_PRINT_AREA: true, EXACT: false, BREADCRUMS: [], AUTHORISED: () => checkAuthorised(true) });
        LINK_CONTACT.LINK.BREADCRUMS = generateBreadCrums(LINK_HOME.LINK, [LINK_CONTACT.LINK]);
        LINK_CONTACT.ROUTE.BREADCRUMS = LINK_CONTACT.LINK.BREADCRUMS;

        let LINK_HELP: INaviComponentLink = generateComponentLink(true, { NAME: "Help", AUTHORISED: () => checkAuthorised(true), HREF: "/help", ICON: "help circle icon", SITEMAP: true, SHOW_BREADCRUMS: true }, { DISPLAYLASTUPDATED: true, LASTUPDATED: moment(), COMPONENT: Help, INCLUDE_PRINT_AREA: true, EXACT: false, BREADCRUMS: [], AUTHORISED: () => checkAuthorised(true) });
        LINK_HELP.LINK.BREADCRUMS = generateBreadCrums(LINK_HOME.LINK, [LINK_HELP.LINK]);
        LINK_HELP.ROUTE.BREADCRUMS = LINK_HELP.LINK.BREADCRUMS;

        let LINK_SIGNIN: INaviComponentLink = generateComponentLink(false, { NAME: "SignIn", AUTHORISED: () => checkAuthorised(true), HREF: "/signin", JS_LINK_TAG: "js-sign-in-page",ICON: "sign in", SITEMAP: false }, { LASTUPDATED: moment(), COMPONENT: SignInComponent, INCLUDE_PRINT_AREA: false, EXACT: true, BREADCRUMS: [], AUTHORISED: () => checkAuthorised(true) });
        LINK_SIGNIN.LINK.BREADCRUMS = generateBreadCrums(LINK_HOME.LINK, [LINK_SIGNIN.LINK]);
        LINK_SIGNIN.ROUTE.BREADCRUMS = LINK_SIGNIN.LINK.BREADCRUMS;

        let LINK_MANAGE = generateLinkModelManage(LINK_HOME);

        let LINK_UNAUTHRORISED_PAGE: INaviComponentLink = generateComponentLink(false, { NAME: "Authorise", AUTHORISED: () => checkAuthorised(true), HREF: "/security/unauthorised", ICON: "sign in", SITEMAP: false }, { LASTUPDATED: moment(), COMPONENT: SaliUnAuthorisedPage, INCLUDE_PRINT_AREA: false, EXACT: false, BREADCRUMS: [], AUTHORISED: () => checkAuthorised(true) });
        LINK_UNAUTHRORISED_PAGE.LINK.BREADCRUMS = generateBreadCrums(LINK_HOME.LINK, [LINK_SIGNIN.LINK]);
        LINK_UNAUTHRORISED_PAGE.ROUTE.BREADCRUMS = LINK_SIGNIN.LINK.BREADCRUMS;

        let LINK_PROJECT = generateLinkProject(LINK_HOME);

        let linksRegistered: INaviLinksRegister = {
            QLDGOV: LINK_QLD_GOV,
            SITE_MAP: LINK_SITE_MAP,
            CONTACT: LINK_CONTACT,
            HELP: LINK_HELP,
            SEARCH: LINK_SEARCH,
            HOME: LINK_HOME,
            ABOUT: LINK_ABOUT,
            PROJECT: LINK_PROJECT,
            MANAGE: LINK_MANAGE,
            SIGNIN: LINK_SIGNIN,
            FOOTER: LINK_FOOTER,
            UNAUTHRORISED_PAGE: LINK_UNAUTHRORISED_PAGE
        };

        return linksRegistered;
    }

    // We manage all navigation links here
    private generateNavigationEntries(registered: INaviLinksRegister): INaviLinkLayout {

        // This is to build the menu structure for layout ect
        let configurationNavigation: INaviLinkLayout = {
            SEARCH_AREA: {
                SITEMAP: registered.SITE_MAP,
                CONTACT: registered.CONTACT,
                HELP: registered.HELP,
                LINKS: [
                    {
                        GROUP_NAME: "",
                        LINKS: [
                            registered.SITE_MAP.LINK,
                            registered.CONTACT.LINK,
                            registered.HELP.LINK
                        ]
                    }
                ],
                ROUTES: [
                    {
                        ROUTE: registered.SITE_MAP.ROUTE
                    },
                    {
                        ROUTE: registered.CONTACT.ROUTE
                    },
                    {
                        ROUTE: registered.HELP.ROUTE
                    }
                ]
            },
            LEFT: {
                HOME: registered.HOME,
                ABOUT: registered.ABOUT,
                PROJECT: {
                    DROP_DOWN_NAME: registered.PROJECT.BASE.NAME,
                    BASE_URL: registered.PROJECT.BASE.HREF,
                    ICON: "search",
                    ROUTES: registered.PROJECT.ROUTES,
                    LINKS: [
                        {
                            GROUP_NAME: registered.PROJECT.BASE.NAME,
                            LINKS: [
                                registered.PROJECT.LINKS.ADD
                            ]
                        },
                    ]
                },
            },
            RIGHT: {
                SIGNIN: registered.SIGNIN,
                MANAGE: {
                    DROP_DOWN_NAME: registered.MANAGE.BASE.NAME,
                    BASE_URL: registered.MANAGE.BASE.HREF,
                    ICON: "settings",
                    ROUTES: registered.MANAGE.ROUTES,
                    LINKS: [
                        {
                            GROUP_NAME: registered.MANAGE.BASE.NAME,
                            LINKS: [
                                registered.MANAGE.LINKS.SECURITY.USERS,
                                registered.MANAGE.LINKS.SECURITY.GROUPS
                            ]
                        }
                    ]
                }
            }
        };
        return configurationNavigation;
    }

    private generateNavigationLinks(registerdLinks: INaviLinksRegister): INaviLink {

        let configurationNavigation: INaviLink = {
            HOME: {
                BASE: registerdLinks.HOME.LINK,
                LINKS: {
                }
            },
            ABOUT: registerdLinks.ABOUT.LINK,
            PROJECT: registerdLinks.PROJECT,
            SITEMAP: registerdLinks.SITE_MAP.LINK,
            CONTACT: registerdLinks.CONTACT.LINK,
            HELP: registerdLinks.HELP.LINK,
            MANAGE: registerdLinks.MANAGE,
            SIGNIN: registerdLinks.SIGNIN
        };

        return configurationNavigation;
    }
}

const saliNavigation = new SaliNavigation();
export default saliNavigation;
