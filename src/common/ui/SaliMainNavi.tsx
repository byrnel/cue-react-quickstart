import * as React from "react";
import * as R from "ramda";
import {
  withRouter,
  Link,
  RouteComponentProps
} from "react-router-dom";
import {
  SemanticSIZES,
  Container,
  Icon,
  Menu,
  Sidebar,
  Responsive,
  MenuItemProps,
  Dropdown,
  DropdownProps,
  DropdownItemProps
} from "semantic-ui-react";
import { observer } from "mobx-react";

// SALI - Menu control and layout
import SignInStore from "../../features/sign-in/SignInStore";
import { UserStore } from "../../stores/";
// SALI - Navigational design, layout, control and configuration (Links , routes are defined within this component )
import SaliNavigation, { INaviLinkLayout, INaviDropDownConfig, INavigationLinkConfig, INaviRightLinks } from "./SaliNavigation";
import * as Environment from "../../constants/Environment";

export interface ImageData {
  size: SemanticSIZES;
  src: string;
}

export interface LinkItemProps {
  visible?: boolean;
  navleftitems: Array<LinkItemData>;
  navrightitems?: Array<LinkItemData>;
  navrightmenuitems?: any;
  image?: ImageData;
}

export interface LinkItemData {
  name: string;
  active: boolean;
  to: string;
  content: string;
  key: string;
  icon: string;
  activeclassname?: string;
  className?: string;
  handleOnClick?(data: any): void; // We can probably remove this at some point
  onClick?(event: React.MouseEvent<HTMLAnchorElement>, data: MenuItemProps): void;
}

export interface IConfigRightNavProps {
  isAuthenticated: boolean;
  userName: string;
  handleSignOut?(to: any): void;
}

export interface INaviMenuEventHandlersBase {
  handleOnClick(e: React.MouseEvent<HTMLAnchorElement>, data: MenuItemProps): void;
  dropDownOnClick?(e: React.KeyboardEvent<HTMLElement>, data: DropdownProps): void;
  dropDownOnItemClick?(e: React.MouseEvent<HTMLDivElement>, data: DropdownItemProps): void;
}

export interface INavBarMobileProps extends INaviMenuEventHandlersBase {
  visible: boolean;
  menuconfig: INaviLinkLayout;
  activeItem: string;
  children: any;
  handlePusher(e: React.MouseEvent<HTMLButtonElement>): void;
  handleToggle(e: React.MouseEvent<HTMLAnchorElement>): void;
}

export interface INavBarDesktopProps extends INaviMenuEventHandlersBase {
  menuconfig: INaviLinkLayout;
  activeItem: string;
}

export const SignOutButton = withRouter(
  ({ history, ...props }) =>
    (
      <Menu.Item
        as={Link}
        name="Sign Out"
        to={SaliNavigation.LINKS_REGISTERED.HOME.LINK.GET_LINK()}
        content="Sign Out"
        icon="sign out"
        onClick={() => {
          SignInStore.signOut(true).then(() => {
            UserStore.isAuthenticated = false; // Setting to false to reflect button statuses
            history.push(SaliNavigation.LINKS_REGISTERED.HOME.LINK.GET_LINK());
          });
        }}
      />
    )
);

function ConfigureRightMenu(menuconfig: INaviRightLinks) {
  return (
    UserStore.isAuthenticated ? (
      <Menu.Menu position="right">
        <Menu.Item>
          Greetings&nbsp;{UserStore.userName()}
        </Menu.Item>
        <ConfigureDropDownMenu {...menuconfig.MANAGE} activeItem={menuconfig.activeItem} />
        {Environment.name !== "govnet" && <SignOutButton {...menuconfig} />}
      </Menu.Menu>
    )
      :
      (
        <Menu.Menu position="right" >
          {Environment.name !== "govnet" && <ConfigureMenuItem {...menuconfig.SIGNIN.LINK} activeItem={menuconfig.activeItem} handleOnClick={menuconfig.handleOnClick} />}
        </Menu.Menu>
      )
  );
}

function ConfigureMenuItem(mi: INavigationLinkConfig) {
  let jsLinkCss = mi.JS_LINK_TAG ? "item " + mi.JS_LINK_TAG : "item";
  return (
    <Menu.Item as={Link} className={jsLinkCss} name={mi.NAME} active={mi.activeItem === mi.NAME} to={mi.GET_LINK()} tabIndex={mi.TAB_INDEX} icon={mi.ICON} onClick={mi.handleOnClick} />
  );
}

// Buil a list of menu structures and groupings if defined
function GenerateMenuItems(ddm: INaviDropDownConfig): JSX.Element[] {
  let mmitems: JSX.Element[] = [];
  var id: number = 1;

  ddm.LINKS.map((group, gid) => {
    var alllinks = R.flatten<INavigationLinkConfig>(group.LINKS.map(x => x));
    var securedLinks = alllinks.filter(f => f.AUTHORISED());
    securedLinks.map((gl, di) => {
      if (group.GROUP_NAME !== undefined && group.GROUP_NAME !== "" && group.GROUP_NAME.toLocaleLowerCase() !== ddm.DROP_DOWN_NAME.toLocaleLowerCase()) {
        mmitems.push(<Menu.Item id="salimenugroupname" link={false} header={true} key={group.GROUP_NAME}> {group.GROUP_NAME}</Menu.Item>);
      }
      id++;
      if (gl.JS_LINK_TAG) {
        let jsLinkCss = gl.JS_LINK_TAG ? "item " + gl.JS_LINK_TAG : "";
        mmitems.push(<Dropdown.Item className={jsLinkCss} key={gl.NAME + id} as={Link} to={gl.GET_LINK()} tabIndex={gl.TAB_INDEX} active={ddm.activeItem === gl.NAME} name={gl.NAME} icon={gl.ICON} text={gl.NAME} onClick={ddm.dropDownOnItemClick} />);
      } else {
        mmitems.push(<Dropdown.Item key={gl.NAME + id} as={Link} to={gl.GET_LINK()} name={gl.NAME} tabIndex={gl.TAB_INDEX} active={ddm.activeItem === gl.NAME} icon={gl.ICON} text={gl.NAME} onClick={ddm.dropDownOnItemClick} />);
      }
    }
    );
  }
  );
  return mmitems;
}

function ConfigureDropDownMenu(ddm: INaviDropDownConfig) {

  let menuitems: JSX.Element[] = GenerateMenuItems(ddm);
  if (menuitems.length === 0) {
    return <div />;
  }

  return (
    <Dropdown
      item={true}
      text={ddm.DROP_DOWN_NAME}
      onClick={ddm.dropDownOnClick}
      active={ddm.activeItem === ddm.DROP_DOWN_NAME}
    >
      <Dropdown.Menu>
        {menuitems}
      </Dropdown.Menu>
    </Dropdown>
  );
}

function RenderGeneralMenus(props: INavBarDesktopProps) {
  return (
      <>
        <ConfigureMenuItem {...props.menuconfig.LEFT.HOME.LINK} activeItem={props.activeItem} handleOnClick={props.handleOnClick} />
        <ConfigureMenuItem {...props.menuconfig.LEFT.ABOUT.LINK} activeItem={props.activeItem} handleOnClick={props.handleOnClick} />
      </>
  );
}

// You need to manually setup additional Menu navigation by adding the entry below
// Add new menu structures after the last entry. 
function RenderCoreMenus(props: INavBarDesktopProps) {
  return (
      <>
        <RenderGeneralMenus {...props} />
        <ConfigureDropDownMenu {...props.menuconfig.LEFT.PROJECT} activeItem={props.activeItem} dropDownOnClick={props.dropDownOnClick} dropDownOnItemClick={props.dropDownOnItemClick} />
      </>
  );
}

function RenderMainMenus(props: INavBarDesktopProps) {
  return (
    UserStore.isAuthenticated ? (
      <>
        <RenderCoreMenus {...props} />
        <ConfigureRightMenu {...props.menuconfig.RIGHT} activeItem={props.activeItem} />
      </>
    )
      :
      (
        <>
          <RenderGeneralMenus {...props} />
          <ConfigureRightMenu {...props.menuconfig.RIGHT} activeItem={props.activeItem} />
        </>
      )
  );
}

function NavBarDesktop(props: INavBarDesktopProps) {
  return (
      <Menu inverted={true}>
        <RenderMainMenus {...props} />
      </Menu>
  );
}

function NavBarMobile({ menuconfig, children, handlePusher, handleToggle, visible, activeItem, handleOnClick, dropDownOnClick, dropDownOnItemClick }: INavBarMobileProps) {
  return (
    <Sidebar.Pushable>
      <Sidebar
        as={Menu}
        animation="push"
        icon="labeled"
        width="thin"
        inverted={true}
        vertical={true}
        visible={visible}
      >
        <RenderCoreMenus menuconfig={menuconfig} activeItem={activeItem} handleOnClick={handleToggle} />
      </Sidebar>
      <Sidebar.Pusher dimmed={visible} onClick={handlePusher} style={{ minHeight: "100vh" }} >
        <Menu inverted={true}>
          <Menu.Item onClick={handleToggle}>
            <Icon name="sidebar" />
          </Menu.Item>
          <ConfigureRightMenu {...menuconfig.RIGHT} activeItem={activeItem} handleOnClick={handleOnClick} />
        </Menu>
        {children}
      </Sidebar.Pusher>
    </Sidebar.Pushable>
  );
}

export const NavBarChildren = ({ children }: any) => (
  <Container fluid={true}>{children}</Container>
);

// function isActiveMenu(activeName: string, thisName: string): boolean {
//   return thisName.localeCompare(activeName) === 0;
// }

export interface ISaliMainNaviState {
  visible: boolean;
  activeItem: string;
}

// The menu structure is passed in from SaliNavigation
// The interface INaviLinkLayout defines the menu configurations
export type ISaliMainNaviProps = RouteComponentProps<any> & {
  menuconfig: INaviLinkLayout;
};

// Main Menu component
export class SaliMainNavi extends React.Component<ISaliMainNaviProps, ISaliMainNaviState> {

  constructor(props: ISaliMainNaviProps) {
    super(props);

    this.state = {
      visible: false,
      activeItem: ""
    };

    this.handleToggle = this.handleToggle.bind(this);
    this.handlePusher = this.handlePusher.bind(this);
  }

  handlePusher = (e: React.MouseEvent<HTMLButtonElement>) => {
    const { visible } = this.state;
    if (visible) {
      this.setState({ visible: false });
    }
  }

  handleMenuClick = (event: React.MouseEvent<HTMLAnchorElement>, data: MenuItemProps) => {
    this.setState({ activeItem: data.name || "" });
  }

  handleToggle = (e: React.MouseEvent<HTMLAnchorElement>) => {
    this.setState({ visible: !this.state.visible });
  }

  dropDownHandleClick = (e: React.KeyboardEvent<HTMLDivElement>, data: DropdownProps) => {
    this.setState({ activeItem: data.text || "" });
  }

  dropDownItemHandleClick = (e: React.MouseEvent<HTMLDivElement>, data: DropdownItemProps) => {
    this.setState({ activeItem: data.name || "" });
  }

  render() {
    const { menuconfig, children } = this.props;
    const { activeItem, visible } = this.state;

    return (
      <div>
        <Responsive {...Responsive.onlyMobile}>
          <NavBarMobile
            menuconfig={menuconfig}
            visible={visible}
            activeItem={activeItem}
            handlePusher={this.handlePusher}
            handleToggle={this.handleToggle}
            handleOnClick={this.handleMenuClick}
            dropDownOnClick={this.dropDownHandleClick}
            dropDownOnItemClick={this.dropDownItemHandleClick}
          >
            <NavBarChildren>{children}</NavBarChildren>
          </NavBarMobile>
        </Responsive>
        <Responsive minWidth={Responsive.onlyTablet.minWidth}>
          <NavBarDesktop
            menuconfig={menuconfig}
            activeItem={activeItem}
            handleOnClick={this.handleMenuClick}
            dropDownOnClick={this.dropDownHandleClick}
            dropDownOnItemClick={this.dropDownItemHandleClick}
          />
          <NavBarChildren>{children}</NavBarChildren>
        </Responsive>
      </div>
    );
  }
}

export default withRouter<ISaliMainNaviProps>(observer(SaliMainNavi));
