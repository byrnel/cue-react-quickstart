import * as React from "react";
import { Container, Grid, Message } from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import SaliNavigation, { ISiteMapConfig } from "./SaliNavigation";

export interface ISiteMapProps {
    siteMaps?: Array<ISiteMapConfig>; // We 
}

interface ISiteMapItemProps {
    index: number;
    siteMap: ISiteMapConfig;
}

const GenerateSiteMapItem = (smp: ISiteMapItemProps) => (
    <li key={smp.index}> {smp.siteMap.DISABLED ? smp.siteMap.NAME : <NavLink key={smp.index} to={smp.siteMap.HREF}> {smp.siteMap.NAME}</NavLink>}
        {smp.siteMap.TREE.length !== 0 && (
            <ul>
                {smp.siteMap.TREE.map((tl, tindex) => {
                    return (
                        <li key={tindex}>
                            <NavLink key={tindex} to={tl.HREF}> {tl.NAME}</NavLink>
                        </li>
                    );
                }
                )}
            </ul>
        )}
    </li>
);

export const GenerateSiteMap = (props: ISiteMapProps) => (
    <ul>
        {props.siteMaps && props.siteMaps.map((nl, nindex) => {
            return (
                <GenerateSiteMapItem key={nindex} siteMap={nl} index={nindex} />
            );
        }
        )}
    </ul>
);

function GatherValidateSiteMaps(siteMaps: Array<ISiteMapConfig>): Array<ISiteMapConfig> {
    let sitemaps = new Array<ISiteMapConfig>();
    // Bug with IE11 not populating siteMaps (added undefined check to prevent page error)
    // Not site maps will be rendered on IE11 until workaround found
    if (siteMaps !== undefined) {
        siteMaps.forEach(nl => {
            if (!nl.DISABLED && nl.TREE.length !== 0 || !nl.DISABLED && nl.TREE.length === 0) {
                sitemaps.push(nl);
            } else if (nl.DISABLED && nl.TREE.length !== 0) {
                sitemaps.push(nl);
            }
        });
    }
    return sitemaps;
}

export class SiteMap extends React.Component<ISiteMapProps, {}> {

    constructor(props: ISiteMapProps) {
        super(props);
    }

    render() {

        let siteMaps  = this.props.siteMaps ? this.props.siteMaps : SaliNavigation.loadSiteMap(); // Bug discovered with IE11 whereby passing values through as props has array as undefined. Loading directly works;
        const sitemaps = GatherValidateSiteMaps(siteMaps);

        return (
            <Container textAlign="left" fluid={true}>
                <Grid stackable={true} stretched={true}>
                    <Grid.Row>
                        <Grid.Column>
                            <Message
                                icon="map"
                                header="Site map"
                                attached={true}
                            />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <p>The site map of SALI Platform. </p>
                            <GenerateSiteMap siteMaps={sitemaps} />
                        </Grid.Column>
                    </Grid.Row>
                </Grid >
            </Container>
        );
    }
}

export default SiteMap;