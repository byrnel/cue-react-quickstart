import * as React from "react";
import { Container, Grid, Table, Message } from "semantic-ui-react";

// TODO: Defaulting email to "salihelp@des.qld.gov.au" . Needs to be created/ verified
interface IContactProps {
    mailTo?: string; // Issue with IE not populate property in configureroots
    // contacts: Array<ISaliContact>; // TODO List can be pulled in through props or Api call
}

interface ISaliContactList {
    contacts: Array<ISaliContact>;
}

interface ISaliContact {
    name: string;
    role: string;
    phone: string;
    email: string;
}

export function RenderContact(contact: ISaliContact) {
    return (
        <Table.Row>
            <Table.Cell>{contact.name}</Table.Cell>
            <Table.Cell>{contact.role}</Table.Cell>
            <Table.Cell>{contact.phone}</Table.Cell>
            <Table.Cell><a href={"mailto:" + contact.email} target="_top">{contact.email}</a></Table.Cell>
        </Table.Row>
    );
}

export function RenderContactList(config: ISaliContactList) {
    return (
        <Table celled={true} striped={true}>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell>Name</Table.HeaderCell>
                    <Table.HeaderCell>Functional Area</Table.HeaderCell>
                    <Table.HeaderCell>Phone</Table.HeaderCell>
                    <Table.HeaderCell>Email</Table.HeaderCell>
                </Table.Row>
            </Table.Header>
            <Table.Body>
                {config.contacts.map((contact, index) => (
                    <RenderContact key={index} {...contact} />
                ))}
            </Table.Body>
            <Table.Footer />
        </Table>
    );
}

export class Contact extends React.Component<IContactProps, {}> {
    contacts: Array<ISaliContact> = new Array<ISaliContact>(); // Build local static contact list for now
    
    constructor(props: IContactProps) {
        super(props);
    }

    render() {

        let emailContact: string = this.props.mailTo ? this.props.mailTo : "soils@qld.gov.au";

        this.contacts.push({ name: "Primary Contact", role: "General Enquiries", phone: "(07) 3170 5623", email: emailContact });
        this.contacts.push({ name: "Dan Brough", role: "Business support", phone: "(07) 3170 5634", email: "daniel.brough@des.qld.gov.au" });
        this.contacts.push({ name: "Evan Thomas", role: "Business support", phone: "(07) 3170 5489", email: "evan.thomas@des.qld.gov.au" });
        // this.contacts.push({ name: "Michael Emery", role: "Technical support", phone: "(07) 3170 5566", email: "michael.emery@des.qld.gov.au" });
        // this.contacts.push({ name: "Jason Shen", role: "Technical support", phone: "(07) 3170 5572", email: "jason.shen@des.qld.gov.au" });

        return (
            <Container fluid={true}>
                <Grid stackable={true} stretched={true}>
                    <Grid.Row>
                        <Grid.Column>
                            <Message
                                icon="mail"
                                header="Contact"
                                attached={true}
                            />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <p>If you have any questions or comments, then the primary contact email for the SALI team is <a href={"mailto:" + emailContact} target="_top">{emailContact}</a>, or if you need to speak with someone
                then please call the relevant contact below:</p>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <RenderContactList contacts={...this.contacts} />
                        </Grid.Column>
                    </Grid.Row>
                </Grid >
            </Container>
        );
    }
}

export default Contact;
