
import * as React from "react";
import * as moment from "moment";
import { Container } from "semantic-ui-react";
import { SaliReactToPrint, SaliConfigureHeadings } from "../../common/ui";
import { RouteComponentProps } from "react-router-dom";
import { IBreadCrumbItem } from  "../../common/ui/SaliHelpers";

export type IComponentPrintConfigProps = RouteComponentProps<any> & {
  Component: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>;
  lastModified: moment.Moment; // TODO: Need to determine if we can automatically get modified date from the component or we manually update this?
  displayLastModifiedDate?: boolean | false; // If a tabluar layout then we need different column layout for Breadcrums
  crums: Array<IBreadCrumbItem> | undefined;
  showprint: boolean | false;
};

export class SaliConfigurePrintArea extends React.Component<IComponentPrintConfigProps, {}> {
  componentRef: any;
  constructor(props: IComponentPrintConfigProps) {
    super(props);
  }

  render() {

    const { crums, Component } = this.props;

    return (
      <Container fluid={true}>
        <SaliReactToPrint
          {...this.props}
          crums={crums}
          content={() => this.componentRef}
        />
        <SaliConfigureHeadings {...this.props} ref={(el: any) => this.componentRef = el}>
            <Component {...this.props} />
        </SaliConfigureHeadings>
      </Container>
    );
  }
}

export default SaliConfigurePrintArea;