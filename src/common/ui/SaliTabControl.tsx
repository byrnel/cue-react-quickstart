import * as React from "react";
import * as R from "ramda";
import { observer } from "mobx-react";
import { Menu, Container, Grid, Segment, MenuItemProps } from "semantic-ui-react";
import { UserStore } from "../../stores/";
import { INavigationLinkConfig, INaviLinkGroups } from "./SaliNavigation";
import { SignOutButton } from "./SaliMainNavi";
import { NavLink, RouteComponentProps } from "react-router-dom";

// *************** SALI Reusable Tab control layout for Pages *********************** //
// All pages are renderd using this control as it defines page layout and menu elements
export interface ISaliTabControlProps extends RouteComponentProps<any> {
    links: INaviLinkGroups[];
    onClick: (event: React.MouseEvent<HTMLAnchorElement>, data: MenuItemProps) => void;
    children?: ((props: RouteComponentProps<any>) => React.ReactNode) | React.ReactNode;
}

export interface ISaliTabControlState {
    activeItem: string;
}

export interface ISaliTabControlBase {
    activeItem: string;
    onClick: (event: React.MouseEvent<HTMLAnchorElement>, data: MenuItemProps) => void;
}

export interface ISaliTabLinkProps extends ISaliTabControlBase {
    links: INaviLinkGroups[];
}

export interface ISaliTabLinkItemProps extends ISaliTabControlBase {
    id: number;
    link: INavigationLinkConfig;
}

export function ConfigureTabGroupMenus(config: ISaliTabLinkProps) {

    let menuitems: JSX.Element[] = [];
    var id: number = 1; // We need to ensure unique id for key otherwise react (current version 15.x) uses only first element. 
 
    config.links.map((group, gid) => {

        var alllinks = R.flatten<INavigationLinkConfig>(group.LINKS.map(x => x));
        var securedLinks = alllinks.filter(f=> f.AUTHORISED());
        if(securedLinks.length !== 0) { // We only want to display the header if we have links to render. Once per link group
            if (group.GROUP_NAME !== undefined && group.GROUP_NAME !== "") {
                menuitems.push(<Menu.Header className="ui header item" >{group.GROUP_NAME}</Menu.Header>);
            }
        }
        securedLinks.map((gl, di) => {
                id++;
                menuitems.push(<ConfigureTabItem key={id} link={gl} id={id} activeItem="" onClick={config.onClick} />);
            }
        );
        }
    );

    if(UserStore.isAuthenticated) {
        id++;
        menuitems.push(<Menu.Item key={id} name="" active={true} />);
        id++;
        menuitems.push(<SignOutButton key={id} />);
    }

    if(menuitems.length === 0) { // If no menus then just return div
        return <div />;
    }

    return (
        <Menu fluid={true} vertical={true}>
            {menuitems}
        </Menu>
    );
}

export function ConfigureTabItem(tab: ISaliTabLinkItemProps) {
    let className: string = tab.link.JS_LINK_TAG ? tab.link.JS_LINK_TAG : "js-" + tab.id + "-tab";
    return (
        <Menu.Item
            key={tab.id}
            as={NavLink}
            to={tab.link.GET_LINK()}
            name={tab.link.NAME}
            active={tab.activeItem === tab.link.NAME}
            tabIndex={tab.link.TAB_INDEX}
            icon={tab.link.ICON}
            text={tab.link.NAME}
            onClick={tab.onClick}
            className={className}
        />
    );
}

@observer
export class SaliTabControl extends React.Component<ISaliTabControlProps, ISaliTabControlState> {

    constructor(props: ISaliTabControlProps) {
        super(props);

        this.state = { activeItem: "" };
        this.handleMenuItemClick = this.handleMenuItemClick.bind(this);
    }

    handleMenuItemClick = (event: React.MouseEvent<HTMLAnchorElement>, data: MenuItemProps) => {
        event.preventDefault();
        this.setState({ activeItem: data.name || "" });
        let target = data.to;
        this.props.history.push(target);
    }

    render() {
        const { activeItem } = this.state;
        const { links } = this.props;

        return (
            <Container fluid={true}>
                <Grid stackable={true} stretched={true}>
                    <Grid.Column width={4} stackable={true}>
                        <Segment container={true} fluid={true} secondary={true}>
                            <ConfigureTabGroupMenus links={links} onClick={this.handleMenuItemClick} activeItem={activeItem} />
                        </Segment>
                    </Grid.Column>

                    <Grid.Column stackable={true} stretched={true} width={12}>
                        <Segment container={true} fluid={true}>
                            {this.props.children}
                        </Segment>
                    </Grid.Column>
                </Grid>
            </Container>
        );
    }
}

export default SaliTabControl;