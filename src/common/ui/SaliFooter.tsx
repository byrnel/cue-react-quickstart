import * as React from "react";
import SaliNavigation, { INaviGeneral } from "./SaliNavigation";

interface ISaliFooterProps {
    link: INaviGeneral;
    id: number;
}

function GeneratLinkItem(props: ISaliFooterProps): JSX.Element {
    return <a className="item" key={props.id} href={props.link.HREF} rel={props.link.REL} target={props.link.TARGET} tabIndex={props.link.TAB_INDEX}> {props.link.NAME}</a>;
}

export const SaliFooter: React.StatelessComponent<{}> = () => {
    return (
        <div id="footer" className="ui darkblue inverted vertical footer segment">
            <div className="ui center aligned container">
                <div className="ui horizontal inverted large divided link list">
                    {SaliNavigation.LINKS_REGISTERED.FOOTER.LINKS.map((link, index) => (
                        <GeneratLinkItem link={link} id={index}/>
                    ))}
                </div>
                <div className="ui inverted section divider" />
                <div className="ui horizontal inverted large divided link list">
                    <p className="legal">{SaliNavigation.LINKS_REGISTERED.FOOTER.FOOTER_BRAND}</p>
                    <p>
                        <GeneratLinkItem link={SaliNavigation.LINKS_REGISTERED.FOOTER.LINK} id={99}/>   
                    </p>
                </div>
            </div>
        </div>
    );
};
