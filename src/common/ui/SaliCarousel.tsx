import * as React from "react";
import { Container, Grid } from "semantic-ui-react";
require("./imageslide.css");

// ImageSlider control
const ImageSlide = ({ url }: any) => {
  const styles = {
    backgroundImage: `url(${url})`
  };

  return (
    <div className="image-slide" style={styles} />
  );
};

const Arrow = ({ direction, clickFunction, glyph }: any) => (
  <div className={`slide-arrow ${direction}`} onClick={clickFunction}>
    {glyph}
  </div>
);

interface ISaliCarouselState {
  currentImageIndex: number;
}

// Reference
// https://dev.to/willamesoares/how-to-build-an-image-carousel-with-react--24na
export class SaliCarousel extends React.Component<{}, ISaliCarouselState> {

  imgUrls = [
    require("../../assets/img/sali/carasoul/image1.jpg"),
    require("../../assets/img/sali/carasoul/image2.jpg"),
    require("../../assets/img/sali/carasoul/image3.JPG"),
    require("../../assets/img/sali/carasoul/image4.JPG"),
  ];

  constructor(props: {}) {
    super(props);

    this.state = {
      currentImageIndex: 0
    };

    this.nextSlide = this.nextSlide.bind(this);
    this.previousSlide = this.previousSlide.bind(this);
  }

  previousSlide() {
    const lastIndex = this.imgUrls.length - 1;
    const { currentImageIndex } = this.state;
    const shouldResetIndex = currentImageIndex === 0;
    const index = shouldResetIndex ? lastIndex : currentImageIndex - 1;

    this.setState({
      currentImageIndex: index
    });
  }

  nextSlide() {
    const lastIndex = this.imgUrls.length - 1;
    const { currentImageIndex } = this.state;
    const shouldResetIndex = currentImageIndex === lastIndex;
    const index = shouldResetIndex ? 0 : currentImageIndex + 1;

    this.setState({
      currentImageIndex: index
    });
  }

  render() {

    return (
      <Container fluid={true} className="carousel">
        <Grid>
          <Grid.Column verticalAlign="middle">
            <Arrow
              direction="left"
              clickFunction={this.previousSlide}
              glyph="&#9664;"
            />
          </Grid.Column>
          <Grid.Column width={14}>
            <ImageSlide url={this.imgUrls[this.state.currentImageIndex]} />
          </Grid.Column>
          <Grid.Column verticalAlign="middle">
            <Arrow
              direction="right"
              clickFunction={this.nextSlide}
              glyph="&#9654;"
            />
          </Grid.Column>
        </Grid>
      </Container>
    );
  }
}

export default SaliCarousel;
