import * as React from "react";
import { Container, Message, Grid } from "semantic-ui-react";

interface IAboutProps {
    productname: string;
}

export class About extends React.Component<IAboutProps, {}> {

    constructor(props: IAboutProps) {
        super(props);
    }

    render() {

        return (
            <Container fluid={true}>
                <Grid stackable={true} stretched={true}>
                    <Grid.Row>
                        <Grid.Column>
                            <Message
                                icon="info"
                                header="About"
                                attached={true}
                            />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={16} stackable={true} >
                            <div className="ui three cards">

                                <div className="ui card">
                                    <div className="image dimmable">
                                        <div className="ui blurring inverted dimmer transition hidden" />
                                        <img src={require("../../assets/img/sali/image3.JPG")} />
                                    </div>
                                    <div className="content">
                                        <div className="header">MODELd</div>
                                        <div className="meta" />
                                        <div className="description">MODELd provides the ability to store, manage and publish soil spatial information. MODELd is used to store vector metadata for the various standard spatial products delivered from SALI. The raster data produced during Digital Soil Mapping and Digital Soil Assessment is also stored in MODELd along with its metadata. The MODELd system allows data to be grouped into sets for publication. The data and metadata from MODELd are able to be published to the QSpatial system for public access and download. </div>
                                    </div>
                                    <div className="extra content">
                                        {/* <i className="eye icon" />Link */}
                                    </div>
                                </div>

                                <div className="ui card">
                                    <div className="image dimmable">
                                        <div className="ui blurring inverted dimmer transition hidden">
                                            <div className="content">
                                                <div className="center">
                                                    {/* <div className="ui teal button">Add Friend</div> */}
                                                </div>
                                            </div>
                                        </div>

                                        <img src={require("../../assets/img/sali/Raindrop2.jpg")} />
                                    </div>
                                    <div className="content">
                                        <div className="header">INFORMd</div>
                                        <div className="meta" />
                                        <div className="description">INFORMd is the repository for knowledge products generate by soil activities. The products include maps, reports, factsheets and scientific articles. INFORMd provides a system to store and manage these products but also publish them to the Queensland Government Publications Portal; which makes them publicly accessible. </div>
                                    </div>
                                    <div className="extra content">
                                        {/* <i className="eye icon" />Link */}
                                    </div>
                                </div>

                                <div className="ui card">
                                    <div className="content">
                                        <div className="header">
                                            SALI Components
                                        </div>
                                        <div className="description">
                                            The SALI Platform is made from a number of components which work together to store and manage the foundational soil data for Queensland. They also provide the mechanisms to make this information available and acecssible.
                                        </div>
                                        <br />
                                        <div className="ui container fluid">
                                            <div className="ui h4">
                                                Some of the components to the SALI Platform include:
                                            </div>
                                            <Container>
                                                <ul>
                                                    <li className="item">SALI Oracle - an Oracle Forms application for the entry and management of soil data</li>
                                                    <li className="item">INFORMd - a system for storing and managing knowledge products</li>
                                                    <li className="item">MODELd - a system for storing and managing spatial data and metadata.</li>
                                                    <li className="item">SERVEd - a system to make the soil data available in SALI accessible to broad audiences through standards based interfaces.</li>
                                                </ul>
                                            </Container>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid >
            </Container >
        );
    }
}

export default About;
