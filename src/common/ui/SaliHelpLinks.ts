
export const SaliHelpLinks = {
    "SEARCHING_NAVIGATION_OTIONS": {
        TITLE: "Searching and navigation options",
        LINKS: [
            {
                index: 1,
                name: "Navigation",
                href: "https://www.qld.gov.au/help/navigation",
                target: "_blank",
                rel: "noopener noreferrer"
            },
            {
                index: 2,
                name: "Search",
                href: "https://www.qld.gov.au/help/search",
                target: "_blank",
                rel: "noopener noreferrer"
            }
        ]
    },
    "SITE_FEATURES": {
        TITLE: "Site features",
        LINKS: [
            {
                index: 1,
                name: "Accessibility",
                href: "https://www.qld.gov.au/help/accessibility",
                target: "_blank",
                rel: "noopener noreferrer"
            },
            {
                index: 2,
                name: "Keyboard Access",
                href: "https://www.qld.gov.au/help/keyboard",
                target: "_blank",
                rel: "noopener noreferrer"
            },
            {
                index: 3,
                name: "Browser support",
                href: "https://www.qld.gov.au/help/browsers",
                target: "_blank",
                rel: "noopener noreferrer"
            },
            {
                index: 4,
                name: "Viewing and downloading files",
                href: "https://www.qld.gov.au/help/downloading",
                target: "_blank",
                rel: "noopener noreferrer"
            },
            {
                index: 5,
                name: "Consistent User Experience",
                href: "https://www.qld.gov.au/help/cue",
                target: "_blank",
                rel: "noopener noreferrer"
            }
        ]
    }
};

export default SaliHelpLinks;