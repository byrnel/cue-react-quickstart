import * as React from "react";
import { NavLink } from "react-router-dom";
import SaliNavigation, { INaviComponentLink } from "./SaliNavigation";

function GenerateNavLink(comp: INaviComponentLink): JSX.Element {
    return <NavLink className="item" key={comp.LINK.NAME} to={comp.LINK.GET_LINK()} tabIndex={comp.LINK.TAB_INDEX}><i className={comp.LINK.ICON} /> {comp.LINK.NAME}</NavLink>;
}

// Lawrence - Search area nav at top of page
export const SaliSearchNavi: React.StatelessComponent<{}> = () => {
    return (
        <div className="ui stackable menu">
            <a className="item" href={SaliNavigation.LINKS_REGISTERED.QLDGOV.HREF} target={SaliNavigation.LINKS_REGISTERED.QLDGOV.TARGET}>
                <img key={SaliNavigation.LINKS_REGISTERED.QLDGOV.IMAGE.SRC} src={SaliNavigation.LINKS_REGISTERED.QLDGOV.IMAGE.SRC} alt={SaliNavigation.LINKS_REGISTERED.QLDGOV.IMAGE.ALT} className="ui medium image" />
            </a>

            <div className="right item">
                <GenerateNavLink {...SaliNavigation.LINKS_REGISTERED.SITE_MAP} />
            </div>
            <GenerateNavLink {...SaliNavigation.LINKS_REGISTERED.CONTACT} />
            <GenerateNavLink {...SaliNavigation.LINKS_REGISTERED.HELP} />

            {SaliNavigation.LINKS_REGISTERED.SEARCH.DISPLAY && (
                <div className="item">
                    <form id="searchform" key="search-form" action={SaliNavigation.LINKS_REGISTERED.SEARCH.ACTION} role="search" target={SaliNavigation.LINKS_REGISTERED.SEARCH.TARGET}>
                        <div>
                            <input id="searchinput" type="hidden" name="form" value="simple-adv" aria-labelledby="searchform searchinput"/>
                            <div className="ui icon input"><input type="search" id="search-query" name="query" aria-labelledby="searchform searchinput"/>
                                <button type="submit" name="login-button" className="ui green submit button">
                                    <i className="search white icon" />
                                </button>
                            </div>
                            {SaliNavigation.LINKS_REGISTERED.SEARCH.INPUTS.map((ip, index) => (
                                <input type={ip.TYPE} key={index} name={ip.NAME} value={ip.VALUE} />
                            ))}
                        </div>
                    </form>
                </div>
            )
            }
        </div>
    );
};
