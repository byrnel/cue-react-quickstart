import * as React from "react";
import { RouteComponentProps } from "react-router-dom";
import * as ReactDOM from "react-dom";
import SaliNavigation from "./SaliNavigation";
import { IBreadCrums } from "./SaliNavigation";
import { GenerateBreadCrumb, IBreadCrumbItem } from "../../common/ui/SaliHelpers";
import { Icon, Button, Grid, Segment } from "semantic-ui-react";

export function RenderLayout(props: any) {
  return (
    <Grid stackable={true}>
      <Grid.Column stackable={true} width={16}>
        {props.children}
      </Grid.Column>
    </Grid>
  );
}

export function RenderBreadCrums(config: IBreadCrums) {
  return (
    <RenderLayout {...config} >
      <Segment secondary={true} attached="bottom" clearing={true}>
        {config.crums && <GenerateBreadCrumb breadcrumbs={config.crums} />}
        {config.showprint && <Button.Group floated="right">
          <Button id="print-btn" size="mini" color="blue" onClick={config.print}>
            <Icon name="print" /> Print</Button>
        </Button.Group>
        }
      </Segment>
    </RenderLayout>
  );
}

// Credits To gregnb => Reference https://www.npmjs.com/package/react-to-print
// Idea of ReactToPrint adopted and expanded / converted to typescript and customised for SALI platform
export interface IComponentPrintConfig {
  content: () => any;
  showprint: boolean | false;
  crums?: IBreadCrumbItem[];
  routeprops?: RouteComponentProps<any>;
  copyStyles?: boolean;
  onBeforePrint?(): void;
  onAfterPrint?(): void;
}

// Polyfill for IE 11 support
if (!Array.from) {
  Array.from = function (object: any) {
    return [].slice.call(object);
  };
}

// NOTE: To provide CUE print layout and elements required on print friendly page, manual adhoc document manipulation was required
// 6.6.18 - Supported on Edge, IE11 and Chrome
export class SaliReactToPrint extends React.Component<IComponentPrintConfig, {}> {

  static defaultProps = {
    copyStyles: true,
    printImage: "http://www.qld.gov.au/assets/v2/images/skin/qg-coa-print.png" // TODO: img does not render in print preview using require("./assets/img/qg-coa-print.png");
  };

  // CSS styles for print layout ect as per CUE guidelines
  private saliPrintStyles: any =
    "@media print, screen {" +
    "body: {" +
    "background-color: #FFFFFF;" +
    "color: #000000;" +
    "-webkit-print-color-adjust: exact;" +
    "}" +
    "#breadcrumbs h2 {" +
    "position: static;" +
    "font-size: 100%;" +
    "display: inline;" +
    "font-weight: normal;" +
    "}" +
    "#breadcrumbs ol, #breadcrumbs li, #breadcrumbs li.last-child, #breadcrumbs a, #breadcrumbs .active.section  {" +
    "text-decoration: none;" +
    "color: black;" +
    "display: inline;" +
    "margin: 0;" +
    "padding: 0;" +
    "float: none;" +
    "}" +
    ".print-version {" +
    "display: inline;" +
    "}" +
    // "#breadcrumbs a:after {" +
    // "content: '>';" +
    // "}" +
    ".print-link-url, a, a:link, a:visited  {" +
    "color: blue;" +
    "}" +
    ".print-ruler {" +
    "size: 50;" +
    "width: 100%;" +
    "noshade;" +
    "}" +
    ".print-heading {" +
    // "display: flex;" +
    "text-align: center;" +
    "padding-bottom: 50px;" +
    "align: right;" +
    "}" +
    ".print-heading-img {" +
    "display: block;" +
    "height: 100px;" +
    "width: 100px;" +
    "margin-left: 155mm;" +
    "}" +
    "#print-btn {" +
    "display: none;" +
    "}" +
    "}";

  private printWindow: any = null;
  private imageTotal: number = 0;
  private imageLoaded = 0;
  private linkTotal: number = 0;
  private linkLoaded: number = 0;

  constructor(props: IComponentPrintConfig) {
    super(props);
    this.handlePrint = this.handlePrint.bind(this);
  }

  triggerPrint(target: any) {
    if (this.props.onBeforePrint) {
      this.props.onBeforePrint();
    }
    setTimeout(() => {
      target.print();
      target.close();
    }, 100);
  }

  handlePrint = () => {

    const {
      content,
      copyStyles,
      onAfterPrint
    } = this.props;

    this.printWindow = window.open("", "Print", "menubar=yes,status=yes, toolbar=yes, scrollbars=yes", false);

    if (onAfterPrint) {
      this.printWindow.onbeforeunload = onAfterPrint;
    }

    const contentNodes = ReactDOM.findDOMNode(this) as Element;
    const imageNodes = Array.from(document.querySelectorAll("img"));
    const linkNodes = document.querySelectorAll("link[rel='stylesheet']");

    this.imageTotal = imageNodes.length;
    this.imageLoaded = 0;

    this.linkTotal = linkNodes.length;
    this.linkLoaded = 0;

    const markLoaded = (type: any) => {

      if (type === "image") {
        this.imageLoaded++;
      } else if (type === "link") {
        this.linkLoaded++;
      }

      if (this.imageLoaded === this.imageTotal && this.linkLoaded === this.linkTotal) {
        this.triggerPrint(this.printWindow);
      }

    };

    imageNodes.forEach((child: any) => {
      /** Workaround for Safari if the image has base64 data as a source */
      if (/^data:/.test(child.src)) {
        child.crossOrigin = "anonymous";
      }
      child.setAttribute("src", child.src);
      child.onload = markLoaded.bind(null, "image");
      child.onerror = markLoaded.bind(null, "image");
    });

    /*
     * IE does not seem to allow appendChild from different window contexts correctly.  They seem to come back
     * as plain objects. In order to get around this each tag is re-created into the printWindow
     * https://stackoverflow.com/questions/38708840/calling-adoptnode-and-importnode-on-a-child-window-fails-in-ie-and-edge
     */
    if (copyStyles !== false) {

      const headEls = Array.from(document.querySelectorAll("style, link[rel='stylesheet']"));

      var meta = this.printWindow.document.createElement("meta");
      meta.setAttribute("http-equiv", "Content-Type");
      meta.setAttribute("charset", "UTF-8");
      meta.setAttribute("accept-charset", "UTF-8");
      this.printWindow.document.head.appendChild(meta);

      [...headEls].forEach((node: any) => {

        let newHeadEl = this.printWindow.document.createElement(node.tagName);
        if (node.textContent) {
          newHeadEl.textContent = node.textContent;
        } else if (node.textContent) {
          newHeadEl.innerText = node.textContent;
        }

        let attributes = Array.from(node.attributes);
        attributes.forEach((attr: any) => {

          let nodeValue = attr.nodeValue;

          if (attr.nodeName === "href" && /^https?:\/\//.test(attr.nodeValue ? attr.nodeValue : "") === false) {
            nodeValue = document.location.protocol + "//" + document.location.host + nodeValue;
          }

          newHeadEl.setAttribute(attr.nodeName, nodeValue);
        });

        if (node.tagName === "LINK") {
          newHeadEl.onload = markLoaded.bind(null, "link");
          newHeadEl.onerror = markLoaded.bind(null, "link");
        }

        this.printWindow.document.head.appendChild(newHeadEl);

      });

    }

    if (document.body.className) {
      const bodyClasses = document.body.className.split(" ");
      bodyClasses.map(item => this.printWindow.document.body.classList.add(item));
    }

    let styleEl = this.printWindow.document.createElement("style");
    styleEl.appendChild(this.printWindow.document.createTextNode(this.saliPrintStyles));
    this.printWindow.document.head.appendChild(styleEl);

    let imgDiv: Element = this.printWindow.document.createElement("div");
    let printImg: Element = this.printWindow.document.createElement("img");
    printImg.setAttribute("src", SaliReactToPrint.defaultProps.printImage);
    printImg.setAttribute("class", "print-heading-img");
    printImg.setAttribute("id", "qg-coa");
    imgDiv.appendChild(printImg);

    let mainHeader: Element = this.printWindow.document.createElement("div");
    mainHeader.appendChild(imgDiv);

    let siteBrandingName: Element = this.printWindow.document.createElement("h1");
    siteBrandingName.appendChild(this.printWindow.document.createTextNode(SaliNavigation.APP_BANNER_BRANDING.NAME));

    let ruler: Element = this.printWindow.document.createElement("hr");
    ruler.setAttribute("class", "print-ruler");

    mainHeader.appendChild(siteBrandingName);
    mainHeader.appendChild(ruler);

    this.printWindow.document.body.innerHTML = contentNodes.outerHTML;
    var component = this.printWindow.document.createElement("div"); // Lets add the component in for printing layout
    component.innerHTML = (ReactDOM.findDOMNode(content()) as Element).outerHTML;
    this.printWindow.document.body.appendChild(component);
    this.printWindow.document.body.insertBefore(mainHeader, this.printWindow.document.body.firstChild);

    let rulerFooter: Element = this.printWindow.document.createElement("hr");
    rulerFooter.setAttribute("class", "print-ruler");
    this.printWindow.document.body.appendChild(rulerFooter);
    this.printWindow.document.body.appendChild(this.printWindow.document.createElement("br"));

    let printlegalLinks = SaliNavigation.LINKS_REGISTERED.FOOTER.LINKS.filter(f => f.LEGAL === true);
    printlegalLinks.map((fl, index) => {
      let hl = this.printWindow.document.createElement("a");
      var desc = this.printWindow.document.createTextNode(fl.NAME + " ( " + fl.HREF + " )");
      hl.setAttribute("href", fl.HREF);
      hl.setAttribute("class", "print-link-url");
      hl.appendChild(desc);
      this.printWindow.document.body.appendChild(hl);
      this.printWindow.document.body.appendChild(this.printWindow.document.createElement("br"));
    });

    this.printWindow.document.body.appendChild(this.printWindow.document.createElement("br"));

    var legalTitle = this.printWindow.document.createElement("P");
    legalTitle.appendChild(this.printWindow.document.createTextNode(SaliNavigation.LINKS_REGISTERED.FOOTER.FOOTER_BRAND));
    this.printWindow.document.body.appendChild(legalTitle);

    this.printWindow.document.body.appendChild(this.printWindow.document.createElement("br"));

    let linkMain = SaliNavigation.LINKS_REGISTERED.FOOTER.LINK;
    let hmain = this.printWindow.document.createElement("a");
    var descmain = this.printWindow.document.createTextNode(linkMain.NAME + " ( " + linkMain.HREF + " )");
    hmain.setAttribute("href", linkMain.HREF);
    hmain.setAttribute("class", "print-link-url");
    hmain.appendChild(descmain);
    this.printWindow.document.body.appendChild(hmain);
    this.printWindow.document.body.appendChild(this.printWindow.document.createElement("br"));

    this.triggerPrint(this.printWindow);
  }

  render() {

    const { crums, showprint } = this.props;
    const breadcrums = <RenderBreadCrums crums={crums} print={this.handlePrint} showprint={showprint} />;
    return React.cloneElement(breadcrums, {
    });
  }
}

export default SaliReactToPrint;
