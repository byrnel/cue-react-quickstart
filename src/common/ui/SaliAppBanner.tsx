import * as React from "react";
import { Link } from "react-router-dom";
import { IAppBranding } from "./SaliNavigation";

export const SaliAppBanner: React.StatelessComponent<IAppBranding> = (props) => {
    let brandName: string = props.NAME ? props.NAME : "SALI (Branding not specified)";
    let showImage: boolean = false;

    var imageStyle = {
        height: "70"
    };

    const BannerName = () => {
        return (
            <div className="app-banner item">
                <Link to="/" ><strong>{brandName}</strong></Link>
            </div>
        );
    };

    return (
        <div className="ui fluid compact menu salibanner">
            {showImage ? <Link to="/"><img src={props.IMAGE} style={imageStyle} alt={brandName} /></Link> : <BannerName />}
        </div>
    );
};