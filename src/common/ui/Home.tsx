import * as React from "react";
import SaliCarousel from "./SaliCarousel";
import SaliBypassBlock from "./SaliBypassBlock";
import { Container, Grid } from "semantic-ui-react";
import SaliNavigation from "./SaliNavigation";

export const Home: React.StatelessComponent<{}> = () => {

    return (
        <Container fluid={true} className="js-home-component">
            <SaliBypassBlock>
                <div className="ui three column centered grid">
                    <div className="column" />
                    <div className="four column centered row">
                        {SaliNavigation.PRODUCTS.map((product, index) => (
                            <div className="column ">
                                <div className="ui segment">
                                    {product.DISABLED ? (
                                        <a className="ui centered large image" target={product.TARGET}>
                                            <img src={product.IMAGE.SRC} alt={product.IMAGE.ALT} />
                                        </a>
                                    )
                                        : (
                                            <a className="ui centered large image" href={product.HREF} target={product.TARGET}>
                                                <img src={product.IMAGE.SRC} alt={product.IMAGE.ALT} />
                                            </a>
                                        )
                                    }
                                </div>
                                <p>{product.DESC}</p>
                            </div>
                        ))}
                    </div>
                </div>
            </SaliBypassBlock>
            <Grid stackable={true} >
                <Grid.Row>
                    <Grid.Column width={1} />
                    <Grid.Column width={14} stackable={true} >
                        <div role="heading" aria-level={1}>
                            <h2 >SALI</h2>
                            <p>
                                The Soil and Land Information (SALI) Platform is Queensland Government’s corporate repository for the input, management and security of soils data and information. It ensures that important foundational datasets are stored securely in accessible formats to support decision-making, planning and modelling
                                by the government and others now and into the future. SALI is used to distribute soil data to client agencies, industry and the community. A secure collection of Queensland soils information ensures the State meets national obligations and policies for reporting and management of natural assets.
                        </p>
                            <p>
                                SALI contains a number of different modules and components that allow the management of foundational soil information and provision of this data in accessible formats to end users.
                        </p>
                        </div>
                    </Grid.Column>
                    <Grid.Column width={1} />
                </Grid.Row>
            </Grid >
            <SaliCarousel />
        </Container >
    );
};

export default Home;