
import * as React from "react";
import { IBreadCrumbItem } from "../../common/ui/SaliHelpers";
import * as moment from "moment";
import { Container, Segment, Grid, Label } from "semantic-ui-react";
import { RouteComponentProps } from "react-router-dom";

// Your component own properties
export type IConfigureHeadingProps = RouteComponentProps<any> & {
  Component: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>;
  lastModified: moment.Moment;
  displayLastModifiedDate?: boolean | false; // Display lastModified Date for this component (only required for static pages, or any pages deemed worthy :)
  crums: Array<IBreadCrumbItem> | undefined;
};

function RenderLayout(props: any) {
  return (
    <Grid stackable={true}>
      <Grid.Row>
        <Grid.Column width={16} stackable={true}>
          {props.children}
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
}

export class SaliConfigureHeadings extends React.Component<IConfigureHeadingProps, {}> {

  constructor(props: IConfigureHeadingProps) {
    super(props);
  }

  render() {

    let lastModified = moment(this.props.lastModified).format("DD/MM/YYYY");
    const { displayLastModifiedDate, Component } = this.props;

    return (
      <Segment>
        <Container fluid={true}>
            <Component {...this.props} />
        </Container>
        <Container fluid={true}>
          <br />
          {displayLastModifiedDate && (
            <Segment attached="bottom">
              <RenderLayout>
                <Label color="blue" tag={true}>Last updated: {lastModified}</Label>
              </RenderLayout>
            </Segment>
          )
          }
        </Container>
      </Segment>
    );
  }
}

export default SaliConfigureHeadings;