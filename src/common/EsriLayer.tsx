import * as Leaflet from "leaflet";
import { GridLayer, GridLayerProps } from "react-leaflet";
// import { basemapLayer } from "esri-leaflet"; // Using our minimal custom type definitions
import { basemapLayer  } from "esri-leaflet";
import * as Basemap from "esri-leaflet";

interface IEsriLayerProps extends GridLayerProps {
    layerKey: string;
}

export default class EsriLayer extends GridLayer<IEsriLayerProps, Leaflet.GridLayer> {
    createLeafletElement(props: IEsriLayerProps) {
        const { layerKey } = props;

        // Supports one of:
        // "Streets", "Topographic", "Oceans", "OceansLabels", "NationalGeographic",
        // "Gray", "GrayLabels", "DarkGray", "DarkGrayLabels", "Imagery", "ImageryLabels",
        // "ImageryTransportation", "ShadedRelief", "ShadedReliefLabels", "Terrain" or "TerrainLabels"
        // see: https://github.com/Esri/esri-leaflet/blob/master/src/Layers/BasemapLayer.js
        return (basemapLayer(layerKey as Basemap.Basemaps) as any);
    }
}
