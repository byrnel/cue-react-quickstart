import * as React from "react";
import { Form } from "semantic-ui-react";
import * as moment from "moment";
import DatePicker from "react-datepicker";
require("react-datepicker/dist/react-datepicker.css");

interface IDatePickerProps {
    label: string;
    value: moment.Moment;
    onChange: (e: any, data: any) => void;
    error?: boolean;
    required?: boolean;
    id?: string;
    className?: string;
    disabled?: boolean;
}

// See: https://hacker0x01.github.io/react-datepicker/
export default class DateInput extends React.Component<IDatePickerProps, {}> {
    constructor(props: IDatePickerProps) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
    }

    convertToUtcDate(localDate: moment.Moment) {
        if (!moment.isMoment(localDate)) { return null; }
        return localDate.isUTC() ? localDate : moment(localDate).utc().add(localDate.utcOffset(), "m");
    }

    convertToLocalDate(utcDate: moment.Moment) {
        if (!moment.isMoment(utcDate)) { return null; }
        return utcDate.isLocal() ? utcDate : moment(utcDate).local().add(-(moment().utcOffset()), "m");
    }

    handleChange(e: moment.Moment) {
        // We need to convert our DateTime output moment object to be in UTC timezone so
        // that the timezone is consistent across the frontend and backend when saving
        // created and updated dates for raster and vectors.

        this.props.onChange(null, {
            value: this.convertToUtcDate(e)
        });
    }
    
    render() {
        return (
            <Form.Field
                required={this.props.required}
                className={(this.props.error ? "error" : "" ) + " " +(this.props.className || "")}
                id={this.props.id}
            >
                <label>{this.props.label}</label>
                <DatePicker
                       disabled={this.props.disabled}
                       dateFormat="DD/MM/YYYY"
                       selected={this.convertToLocalDate(this.props.value)}
                       onChange={this.handleChange}
                />
            </Form.Field>
        );
    }
}