import * as React from "react";
import * as moment from "moment";
import { Table } from "semantic-ui-react";

interface ITableCellValueProps {
    value?: string | number | moment.Moment;
    children?: any;
    href?: string;
}

/**
 * Adopted for table cell value with any (can be text/date or another component,specified as child elements).
 */
export default class TableCellValue extends React.Component<ITableCellValueProps, {}> {
    render() {
        if (this.props.href) {
            return (
                <Table.Cell>
                   <a href={this.props.href}>{this.getValue()}</a>
                </Table.Cell>
            );
        } else {
            return (
                <Table.Cell>
                    {this.getValue()}
                </Table.Cell>
            );
        }
    }

    getValue() {
        const { value, children } = this.props;

        if(value == null) {
            return children;
        }

        if(this.isMoment(value)) {
            return value.format("MMMM Do YYYY");
        }

        return value;
    }

    isMoment(value: any): value is moment.Moment {
        return (value as moment.Moment).year != null;
    }
}