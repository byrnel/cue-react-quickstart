import * as React from "react";
import { Form } from "semantic-ui-react";

interface ITagInputProps {
    label: string;
    value: string[];
    options: string[];
    placeholder?: string;
    noResultsMessage?: string;
    onCreateTag: (newTag: string) => void;
    onChange: (e: any, data: any) => void;
    error?: boolean;
    className?: string;
    defaultSelected?: string[];
    immutableSelected: string[];
}

export default class TagInput extends React.Component<ITagInputProps, {}> {
    
    isNewTag: boolean;
    isBadTag: boolean;

    /**
     * Creates a new tag when the user manually enters a new tag name and
     * chooses to add it.
     */
    handleOnAddItem = (e: any, { value }: any) => {
        const { options, onCreateTag } = this.props;
        
        this.isNewTag = true;
        this.isBadTag = options.map(x => x.toLowerCase()).indexOf(value.toLowerCase()) !== -1;
        if(!this.isBadTag) {
            onCreateTag(value);
        }
    }

    /** 
     * Acts as a guard to catch a 'bad' tag addition and prevent the
     * cascade to onChange
     */
    handleChange = (e: any, data: any) => {
        if (!this.isBadTag) {
            this.props.onChange(e, data);
        }
        this.isBadTag = false;
        this.isNewTag = false;
    }

    /**
     * Removes the tag when its body is clicked rather than only its little 'x'
     * icon
     */
    handleLabelClick = (e: any, data: any) => {
        const { immutableSelected } = this.props;

        if (data.onRemove != null) {
            if (immutableSelected.indexOf(data.content) === -1) {
                data.onRemove(e, data);
            }
        }
    }

    renderLabel = (item: any, index: number, defaultProps: any) => {
        const { immutableSelected } = this.props;

        if (immutableSelected.indexOf(item.value) !== -1) {
            return {
                content: item.value,
                removeIcon: null
            };
        } else {
            return {
                content: item.value
            };
        }
        
    }

    render() {
        const { label, options, placeholder, noResultsMessage, error,
                className, defaultSelected } = this.props;

        return (
            <Form.Select
                error={error}
                label={label}
                selectOnBlur={false}
                closeOnChange={true}
                closeOnBlur={true}
                multiple={true}
                search={true}
                selection={true}
                scrolling={true}
                allowAdditions={true}
                options={options.map(tag => ({key: tag, value: tag, text: tag}))}
                onChange={this.handleChange}
                onAddItem={this.handleOnAddItem}
                onLabelClick={this.handleLabelClick}
                onRemove={this.handleLabelClick}
                placeholder={placeholder}
                noResultsMessage={noResultsMessage}
                className={className}
                defaultValue={defaultSelected}
                renderLabel={this.renderLabel}
            />
        );
    }
}