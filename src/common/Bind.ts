import * as R from "ramda";
import {isObservableArray} from "mobx";

/**
 * Provides data-binding like functionality to any component which implements
 * value and onChange. Example usage:
 * 
 * <SomeInput {...Bind(person, "name")} />
 * 
 * @param context The object whose property should be shown.
 * @param property The property of the object to show.
 */
function Bind(
    context: any,
    property: string,
    onChange?: (e: any, data: any) => void,
    errors?: IErrors,
    errorProperty?: string) {
    if(!(property in context)) {
        throw "Bound property does not exist: " + property;
    }

    // Handle the case that value is an ObservableArray (from mobx) and
    // convert it into it's more primative representation (an Array<string> or Array<number>)
    const handleObservableArray = (value: any) => {
        if (isObservableArray(value)) {
            var convertedObservableArray = new Array();
            value.map((item: any, index: number) => {convertedObservableArray.push(item);});
            return convertedObservableArray;
        }
        return value;
    };

    // If the context has a guid and we have a dictionary of
    // guids -> properties -> errors then check whether this property is in error
    // and provide that information to the bound component.
    //
    // Consumers can default to checking the bound property name or override
    // and use a different name for error checking. This is useful when the model
    // submitted to the API doesn't exactly match the model being presented (e.g.
    // a property might be called "thing" in a component but submitted to the API
    // as "thingCode", in such cases the errorProperty should be set to "thingCode").
    const checkErrorProperty = (errorProperty != null ? errorProperty : property).toLowerCase();
    let hasErrors =
        context.guid != null &&
        R.path([context.guid, checkErrorProperty], errors) != null;

    // todo
    // This will clear the error if any value is present
    // Obviously this is not ideal for any error other than a 'must not be empty'
    // Consider either removing this if more intricate validation is introduced
    // OR implement a validation code standard so we can check that the error IS
    // in fact a 'must not be empty'
    if (context[property]) {
        hasErrors = false;
    }

    return {
        value: handleObservableArray(context[property]),
        onChange: (e: any, data: any) =>  {
            context[property] = data.value;
            context.changesMade = true;
            if(onChange != null) {
                onChange(e, data);
            }
        },
        error: hasErrors
    };
}

export default Bind;