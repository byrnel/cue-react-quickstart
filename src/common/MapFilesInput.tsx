import * as React from "react";
import { MapFile } from "../domain";
import { FilesInput } from "./";

interface IMapFilesInput {
    // A collection of map files
    value: Array<MapFile>;
    onChange: (e: any, data: { value: Array<MapFile> }) => void;
    errors?: IErrors;
    error?: boolean;
}

export default class MapFilesInput extends React.Component<IMapFilesInput, {}> {
    constructor(props: IMapFilesInput) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e: any, data: any) {
        this.props.onChange(e, {
            value: data.value
        });
    }

    handleCreateMapFile(file: File): IFileModel {
        return new MapFile(file);
    }

    render() {
        const { value, errors } = this.props;

        return (
            <FilesInput
                onChange={this.handleChange}
                fileModelConstructor={this.handleCreateMapFile}
                errors={errors}
                label="Map Files"
                value={value}
            />
        );
    }
}