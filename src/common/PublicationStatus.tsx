import * as React from "react";
import { List, Icon } from "semantic-ui-react";

interface PublicationStatusProps { status: string; }

export default class PublicationStatus extends React.Component<PublicationStatusProps, {}> {
    renderPublishedStatus = () => {
        return (
            <List.Content verticalAlign="middle">
                <List.Icon circular={true} color="green" name="check" /> Published
            </List.Content>
        );
    }

    renderCurrentVersionUnpublishedStatus = () => {
        return (
            <List.Content verticalAlign="middle">
                <Icon circular={true} color="orange" name="warning" /> Current version unpublished
            </List.Content>
        );
    }

    renderUnpublishedStatus = () => {
        return (
            <List.Content verticalAlign="middle">
                <Icon circular={true} color="red" name="x" /> Unpublished
            </List.Content>
        );
    }

    renderPendingPublicationStatus = () => {
        return (
            <List.Content verticalAlign="middle">
                <Icon circular={true} inverted={true} color="orange" name="warning" /> Pending Publication
            </List.Content>
        );
    }

    renderUnknownStatus = () => {
        return (
            <List.Content verticalAlign="middle">
                <Icon circular={true} color="red" name="dont" /> Unknown status
            </List.Content>
        );
    }

    render() {
        switch(this.props.status) {
            case "currentInstancePublished":
                return this.renderPublishedStatus();
            case "currentVersionUnpublished":
                return this.renderCurrentVersionUnpublishedStatus();
            case "unpublished":
                return this.renderUnpublishedStatus();
            case "pendingPublication":
                return this.renderPendingPublicationStatus();
            default:
                return this.renderUnknownStatus();
        }
    }
}