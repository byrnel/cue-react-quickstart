import UrlComboBind from "./UrlComboBind";

interface IBindConfigOptions {
    context: any;
    errors?: IErrors;
}

/**
 * Specifically for the UrlCombo Component (see BindConfig for original)
 */
function UrlComboBindConfig(options: IBindConfigOptions) {
    return (mainProperty: string, 
            urlProperty: string,
            onMainChange?: (e: any, data: any) => void, 
            onUrlChange?: (e: any, data: any) => void, 
            errorProperty?: string) =>
                    UrlComboBind(options.context, 
                        mainProperty,
                        urlProperty,
                        onMainChange,
                        onUrlChange, 
                        options.errors, 
                        errorProperty);
}

export default UrlComboBindConfig;