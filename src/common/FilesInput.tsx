import * as React from "react";
import * as R from "ramda";
import { Form, Button } from "semantic-ui-react";
import BindConfig from "./BindConfig";

interface IFilesInputProps {
    value: Array<IFileModel>;
    onChange: (e: any, data: { value: Array<IFileModel> }) => void;
    fileModelConstructor: (file: File) => IFileModel;
    errors?: IErrors;
    label: string;
    required?: boolean;
}

export default class FilesInput extends React.Component<IFilesInputProps, {}> {
    private fileInput: HTMLInputElement | null;

    constructor(props: IFilesInputProps) {
        super(props);

        this.handleBeginSelectFile = this.handleBeginSelectFile.bind(this);
        this.handleFilesSelected = this.handleFilesSelected.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.renderFileModel = this.renderFileModel.bind(this);
        this.renderAddFileButton = this.renderAddFileButton.bind(this);
    }

    handleBeginSelectFile(e: React.MouseEvent<HTMLButtonElement>, data: any) {
        e.preventDefault();

        if (this.fileInput != null) {
            this.fileInput.click(); // launch the file selector
        }
    }

    handleFilesSelected(e: React.ChangeEvent<HTMLInputElement>) {
        e.preventDefault();
        const { fileModelConstructor } = this.props;
        const files = this.fileInput != null? this.fileInput.files : null;

        if(files == null || files.length === 0) {
            return;
        }

        // Copy out all the files into a new list then clear the
        // input so the user could re-select the same file if desired.
        // Clearing the input clears this.fileInput.files which is
        // why we need to copy it.
        const newFileModels = new Array<IFileModel>();
        for(let i = 0; i < files.length; i++) {
            newFileModels.push(fileModelConstructor(files[i]));
        }

        if (this.fileInput != null) {
            this.fileInput.value = "";
        }

        const value = this.props.value.concat(newFileModels);
        this.props.onChange(e, { value: value });
    }

    handleDelete(fileModel: IFileModel, e: any) {
        e.preventDefault();
        const value = R.without([fileModel], this.props.value);
        this.props.onChange(e, { value: value });
    }

    renderFileModel(fileModel: IFileModel) {
        const bind = BindConfig({
            context: fileModel,
            errors: this.props.errors
        });

        return (
            <Form.Group key={fileModel.guid}>
                <Form.Input disabled={true} {...bind("name")} />
                <Button color="red" icon="delete" onClick={this.handleDelete.bind(null, fileModel)} />
            </Form.Group>
        );
    }

    renderAddFileButton() {
        const { label } = this.props;

        return (
            <div>
                <Button floated="right" icon="file" content={"Add " + label} onClick={this.handleBeginSelectFile} />
                <input
                    type="file"
                    multiple={true}
                    style={{display:"none"}}
                    ref={x => this.fileInput = x}
                    onChange={this.handleFilesSelected}
                />
            </div>
        );
    }

    render() {
        const { label, value } = this.props;

        return (
            <Form.Field required={this.props.required}>
                <label>{label}</label>
                {value.map(this.renderFileModel)}
                {this.renderAddFileButton()}
            </Form.Field>
        );
    }
}