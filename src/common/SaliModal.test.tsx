import * as React from "react";
import { configure, mount } from "enzyme";
import * as Adapter from "enzyme-adapter-react-15";
import SaliModal from "./SaliModal";

configure ({ adapter: new Adapter() });

let header: string = "hello this ip dog";
const content: string = "BOTTOM TEXT";
const list: Array<string> = ["some", "body", "once"];
const icon: string = "home";
const positiveText: string = "yap";
const negativeText: string = "nap";
const customContentClass = "yeetClass";
const customContent: JSX.Element = (
    <div className={customContentClass}>
        <h1>Hello, World!</h1>
        <p>It's ya boy, skinny pete</p>
    </div>
);
const deadClick: () => void = () => { ""; };

const clickEvent = () => {
    const event: any = document.createEvent("Events");
    return event.initEvent("click", true, false);
};

const clickPositive = () => {
    document.getElementsByClassName("actions")[0].children[1].dispatchEvent(clickEvent());
    // document.getElementsByClassName("actions")[0].children[1].fireEvent("onclick");
};

const clickNegative = () => {
    document.getElementsByClassName("actions")[0].children[0].dispatchEvent(clickEvent());
    // document.getElementsByClassName("actions")[0].children[0].fireEvent("onclick");
};

beforeEach(() => {
    document.body.innerHTML = "";
});

describe("SaliModal", () => {
    it ("renders without crashing", () => {
        mount(
            <SaliModal
                onClose={deadClick}
            />
        );
    });
});

describe("SaliModal", () => {    
    it ("renders header", () => {
        mount(
            <SaliModal
                header={header}
                onClose={deadClick}
            />
        );

        const value = document.getElementsByClassName("ui header")[0].textContent;
        expect(value).toEqual(header);
    });
});

describe("SaliModal", () => {
    it ("renders icon", () => {
        mount(
            <SaliModal
                icon={icon}
                onClose={deadClick}
            />
        );

        let headericon = document.getElementsByClassName("icon " + icon);
        expect(headericon).toHaveLength(1);
    });
});

describe("SaliModal", () => {
    it ("renders content", () => {
        mount(
            <SaliModal
                content={content}
                onClose={deadClick}
            />
        );
        const value: any = document.getElementsByClassName("description")[0].textContent;
        expect(value).toEqual(content);
    });
});

describe("SaliModal", () => {
    it ("renders list", () => {
        mount(
            <SaliModal
                list={list}
                onClose={deadClick}
            />
        );
        const value: any = document.getElementsByClassName("list")[0].children;
        expect(value.length).toEqual(list.length);
        expect(value[0].textContent).toEqual(list[0]);
        expect(value[1].textContent).toEqual(list[1]);
        expect(value[2].textContent).toEqual(list[2]);
    });
});

describe("SaliModal", () => {
    it ("renders custom content", () => {
        mount(
            <SaliModal
                customContent={customContent}
                onClose={deadClick}
            />
        );
        const value: any = document.getElementsByClassName(customContentClass);
        expect(value).toHaveLength(1);
    });
});

describe("SaliModal", () => {
    it ("renders default button", () => {
        mount(
            <SaliModal
                onClose={deadClick}
            />
        );
        const buttons: any = document.getElementsByClassName("actions")[0].children;
        expect(buttons).toHaveLength(1);
    });
});

describe("SaliModal", () => {
    it ("renders button text", () => {
        mount(
            <SaliModal
                positiveText={positiveText}
                negativeText={negativeText}
                onClose={deadClick}
            />
        );
        const buttons: any = document.getElementsByClassName("actions")[0].children;
        expect(buttons).toHaveLength(2);
        const backText: string = buttons[0].textContent;
        const okayText: string = buttons[1].textContent;
        expect(backText).toEqual(negativeText);
        expect(okayText).toEqual(positiveText);
    });
});

describe("SaliModal", () => {
    xit ("handles default positive click", () => {
        var count: number = 0;
        const onClose: () => void = () => {
            count += 1;
        };

        mount(
            <SaliModal
                positiveText={"yep"}
                negativeText={"nap"}
                onClose={onClose}
            />
        );

        expect(count).toEqual(0);
        clickNegative();
        expect(count).toEqual(1);
        clickPositive();
        expect(count).toEqual(2);
    });
});

describe("SaliModal", () => {
    xit ("handles custom positive click", () => {
        var count: number = 0;
        const positiveClick: () => void = () => {
            count += 1;
        };

        mount(
            <SaliModal
                positiveText={"yep"}
                negativeText={"nap"}
                onPositiveClick={positiveClick}
                onClose={deadClick}
            />
        );

        expect(count).toEqual(0);
        clickNegative();
        expect(count).toEqual(0);
        clickPositive();
        expect(count).toEqual(1);
    });
});

describe("SaliModal", () => {
    xit ("handles custom negative and negative click", () => {
        var count: number = 0;
        const positiveClick: () => void = () => {
            count += 2;
        };

        const negativeClick: () => void = () => {
            count -= 1;
        };

        mount(
            <SaliModal
                positiveText={"yep"}
                negativeText={"nap"}
                onPositiveClick={positiveClick}
                onNegativeClick={negativeClick}
                onClose={deadClick}
            />
        );

        expect(count).toEqual(0);
        clickNegative();
        expect(count).toEqual(-1);
        clickPositive();
        expect(count).toEqual(1);
    });
});