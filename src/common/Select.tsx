import * as React from "react";
import { observer } from "mobx-react";
import { Form, SemanticWIDTHS } from "semantic-ui-react";

export interface ISelectOption {
    text: string;
    value: string;
    key?: string;
}

export interface ISelectProps {
    /**
     * The currently selected entity. This should be a class instance, not a
     * string.
     */
    value?: object;

    /**
     * A collection of entities from which the user may choose.
     */
    options: any[];

    /**
     * A mapping function that converts an option entity to an ISelectOption
     * which  is used internally by this component to display each option.
     */
    optionsFunc: (option: object) => ISelectOption;

    /**
     * This is invoked whenever the user selects a different entity
     */
    onChange:(e: any, data: any) => void;

    /**
     * Whether to include a blank entry at the start of the select list. This
     * allows the user to deselect.
     */
    withBlankOption?: boolean;

    label?: string;
    error?: boolean;
    disabled?: boolean;
    width?: SemanticWIDTHS;
    className?: string;
    required?: boolean;
    style?: any;
    selectOnBlur?: boolean;
    placeholder?: string;
    noResultsMessage?: string;
    onFocus?:(e: any, data: any) => void;

    /**
     * This is only used to work around testing issues
     */
    blurOnClick?: boolean;
}

interface ISelectState {
    /**
     * Whether the select input is currently focused (clicked on or tabbed into)
     * by the user. If this is true the select options should be shown.
     */
    isFocused: boolean;
}

/**
 * Like Semantic-UI's Form.Select but only populates the select list when the selector is clicked.
 * This reduces loading time when displaying many lists.
 * 
 * Also works with entities rather than strings (i.e. if you provide a list of
 * Person objects it will return a Person in the onChange event when a user
 * selects something).
 */
@observer
export default class Select extends React.PureComponent<ISelectProps, ISelectState> {  
    /**
     * A map of value strings to option objects. This is used to map a value
     * back to the corresponding object after the user makes a selection.
     */
    valueToOptionMap: Map<string, object> = new Map<string, object>();
    previousValue?: object;

    constructor(props: ISelectProps) {
        super(props);

        this.state = { isFocused: false };
    }

    handleFocus = (e: React.SyntheticEvent<HTMLElement>, data: any) => {
        this.setState({ isFocused: true });

        if (this.props.onFocus) {
            this.props.onFocus(e, data);
        }
    }
    handleBlur = () => this.setState({ isFocused: false });

    handleChange = (e: React.SyntheticEvent<HTMLElement>, data: any) => {
        const { value } = data;
        const { onChange, blurOnClick } = this.props;
        const { valueToOptionMap, handleBlur } = this;

        const valueObject  = valueToOptionMap.get(value);
        if(valueObject !== this.props.value) {
            this.previousValue = this.props.value;
        }
        // Get the option object that corresponds to the given value string then
        // notify the consumer.
        onChange(e, {
            value: valueObject,
            previousValue: this.previousValue
        });

        // The selector will stay focused by default until a user clicks another
        // selector. This will hurt performance because it will re-render the
        // full list if a user types into a text box (for example). So we'll
        // pretend we've become unfocused if the user has clicked it.
        // Keyboard events (such as arrow keys) shouldn't trigger this because
        // the selector stays open when the arrow keys are used.
        // blurOnClick is used to keep unit tests happy
        if(e.type === "click" && (blurOnClick == null || blurOnClick === true)) {
            handleBlur();
        }
    }

    /**
     * Gets the currently selected value in a form that can be presented in
     * a select input.
     */
    getSelectValue = () => {
        const { value, optionsFunc } = this.props;

        return value != null
            ? optionsFunc(value)
            : undefined;
    }

    /**
     * Gets all options in a form that can be presented in a select input.
     */
    getSelectOptions = () => {
        const { withBlankOption, options, optionsFunc } = this.props;
        const { valueToOptionMap } = this;

        valueToOptionMap.clear();
        const selectOptions: ISelectOption[] = [];

        if(withBlankOption) {
            selectOptions.push({ value: "", text: "" });
        }

        // Iterate over every option entity, convert it to a select option and
        // add it to the list of options. Also cache the value -> option
        // mapping so we can find the option that corresponds to a value when
        // the user picks a select option.
        for(let i = 0; i < options.length; i++) {
            let option = options[i];
            let selectOption = optionsFunc(option);
            selectOptions.push(selectOption);
            valueToOptionMap.set(selectOption.value, option);
        }

        return selectOptions;
    }

    maybeClearSelectionByDeleteKey = (e: any, value: string) => {
        const { onChange } = this.props;

        if (e.key === "Delete") {
            onChange(e, { value: undefined });
        }
    }

    render() {
        const selectValue = this.getSelectValue();

        // If the input is focused show all options (slow). If the input is
        // not focused, show only the selected option (fast)
        // Never give the input an undefined options value because it will
        // emit warnings.
        const options = this.state.isFocused
            ? this.getSelectOptions()
            : [selectValue || { value: "", text: ""}];

        return (
            <Form.Select
                value={selectValue != null ? selectValue.value : undefined}
                options={options}
                onFocus={this.handleFocus}
                onBlur={this.handleBlur}
                onChange={this.handleChange}
                onKeyDown={this.maybeClearSelectionByDeleteKey}
                search={true}
                label={this.props.label}
                error={this.props.error}
                disabled={this.props.disabled}
                width={this.props.width}
                className={this.props.className + " js-select"}
                required={this.props.required}
                style={this.props.style}
                selectOnBlur={this.props.selectOnBlur}
                placeholder={this.props.placeholder}
                noResultsMessage={this.props.noResultsMessage}
            />
        );
    }
}