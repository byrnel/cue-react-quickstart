import * as React from "react";
import * as R from "ramda";
import { Menu, Icon } from "semantic-ui-react";

interface IPaginatorProps {
    pages: number;
    currentPage: number;
    onNavigate: (pageNumber: number) => void;
}

export default class Paginator extends React.Component<IPaginatorProps, {}> {
    getShownPageNumbers(pages: number, currentPage: number) {
        const groupSize = 4;
        
        // Generate pagination numbers like [1, 2, 3, 4,   10, 11, 12, 13, 14,    17, 18, 19, 20]
        // Show up to three groups of page numbers: The first, middle, and last groups.
        const first = this.range(1, groupSize);
        const middle = this.range(currentPage - (groupSize / 2), currentPage + (groupSize / 2));
        const last = this.range(pages - groupSize + 1, pages);

        const all = R
            .uniq(first.concat(middle).concat(last))
            .filter(x => x > 0 && x <= pages);
        return all;
    }

    range(min: number, max: number) {
        const range = [];
        for(var i = min; i <= max; i++) {
            range.push(i);
        }
        return range;
    }

    render() {
        const { pages, currentPage, onNavigate } = this.props;
        const isAtStart = currentPage === 1;
        const isAtEnd = currentPage === pages;

        const pageNumbers = this.getShownPageNumbers(pages, currentPage);
        const paginationItems = [];
        for(var i = 0; i < pageNumbers.length; i++) {
            var prev = (i === 0 ? 0 : pageNumbers[i - 1]);
            var curr = pageNumbers[i];
            var isEllipses = curr - prev > 1;
            var isCurrentPage = this.props.currentPage === curr;
            
            paginationItems.push(isEllipses
                ? <Menu.Item as="a" key={"ellipses_" + prev} disabled={true}>...</Menu.Item>
                : <Menu.Item as="a" key={"page_" + curr} disabled={isCurrentPage} onClick={onNavigate.bind(null, curr)}>{curr}</Menu.Item>);
        }

        if(!isAtStart) {
            paginationItems.unshift(
                <Menu.Item as="a" icon={true} key="left" onClick={onNavigate.bind(null, currentPage - 1)}>
                    <Icon name="chevron left" />
                </Menu.Item>
            );
        }

        if(!isAtEnd) {
            paginationItems.push(
                <Menu.Item as="a" icon={true} key="right" onClick={onNavigate.bind(null, currentPage + 1)}>
                    <Icon name="chevron right" />
                </Menu.Item>
            );
        }

        return (
            <Menu floated="right" pagination={true}>
                {paginationItems}
            </Menu>
        );
    }
}