import * as React from "react";
import * as moment from "moment";
import { List } from "semantic-ui-react";

interface ILabelValuePairProps {
    className?: string;
    label?: string;
    value?: string | number | moment.Moment;
    children?: any;
}

/**
 * Shows a label with any value (can be text or another component,
 * specified as child elements).
 */
export default class LabelValuePair extends React.Component<ILabelValuePairProps, {}> {
    render() {
        return (
            <List>
                <List.Item>
                    <List.Content>
                        <List.Header>{this.props.label}</List.Header>
                        <List.Description className={this.props.className}>{this.getValue()}</List.Description>
                    </List.Content>
                </List.Item>
            </List>
        );
    }

    getValue() {
        const { value, children } = this.props;

        if(value == null) {
            return children;
        }

        if(this.isMoment(value)) {
            return value.format("MMMM Do YYYY");
        }

        return value;
    }

    isMoment(value: any): value is moment.Moment {
        return (value as moment.Moment).year != null;
    }
}