interface IFileModel {
    guid: string;
    name: string;
    file?: File;
}