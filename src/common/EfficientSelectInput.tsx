import * as React from "react";
import * as R from "ramda";
import { Form } from "semantic-ui-react";

interface ISelectOption {
    text: string;
    value: string;
    "data-entity"?: object;
}

interface IEfficientSelectInputProps {
    value: Array<object>; // The currently selected entity (a rich object - not a value string)
    label: string;
    onChange: (e: any, data: any) => void;
    entities: Array<object>; // An array of rich entities (they are converted to select options using optionsFunc)
    optionsFunc: (entities: Array<object>) => Array<ISelectOption>;
    error?: boolean;
    disabled?: boolean;
    width?: any;
    className?: string;
    withBlankOption?: boolean;
    multipleSelect?: boolean;
    required?: boolean;
    placeholder?: string;
}

interface IEfficientSelectInputState {
    isActive: boolean;
}

/**
 * Like Semantic-UI's Form.Select but only populates the select list when the selector is clicked.
 * This reduces loading time when displaying many lists.
 */
export default class EfficientSelect extends React.Component<IEfficientSelectInputProps, IEfficientSelectInputState> {
    constructor(props: IEfficientSelectInputProps) {
        super(props);

        this.state = {
            isActive: false
        };

        this.handleFocus = this.handleFocus.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.getSelectedOptions = this.getSelectedOptions.bind(this);
        this.deleteSelection = this.deleteSelection.bind(this);
    }

    handleFocus(e: any) {
        this.setState({ isActive: true });
    }

    handleBlur(e: any) {
        this.setState({ isActive: false });
    }

    handleChange(e: any, data: any) {
        const { value, options } = data;

        if (this.props.multipleSelect) {
            let selectedOptions: Array<Object> = [];

            value.forEach((selectedItem: string) => {
                var selectedOption = R.find(((option: any) => option.value === selectedItem), options);
                selectedOptions.push(selectedOption["data-entity"]);
            });
            this.props.onChange(e, {
                value: selectedOptions
            });
        } else {
            // Find the rich entity in the options list using the value as the key
            const selectedOption = R.find((option: any) => option.value === value, options);
            const entity = selectedOption["data-entity"];
            this.props.onChange(e, {
                value: entity
            });

            // Force close the input because onblur doesn't seem to fire normally.
            // this removes all the select options from the DOM which would otherwise
            // kill performance if we left them in. If you remove this line,
            // performance will be bad when typing in text inputs if a user has
            // opened a selector with a lot of options.
            this.handleBlur(null);
        }
    }

    getSelectedOptions() {
        let convertedSelectedOptions: Array<{value: string, text: string}> = [];
        if (this.props.multipleSelect) {

            var selectedOptions = this.props.value !== undefined? this.props.optionsFunc([this.props.value][0]) : undefined;
            if (selectedOptions != null) {
                selectedOptions.forEach((option: any) => {
                    convertedSelectedOptions.push({ value: option.value, text: option.text });
                });
            }

            return selectedOptions != null? convertedSelectedOptions : undefined;
        }

        if (this.props.value != null) {
            const selectedOption = this.props.optionsFunc([this.props.value])[0];
            convertedSelectedOptions.push({ value: selectedOption.value, text: selectedOption.text });
        }
        convertedSelectedOptions.push({ value: "", text: ""});
        return convertedSelectedOptions;
    }

    deleteSelection(e: any, value: string) {
        var spinnerOptions = this.props.optionsFunc(this.props.entities);
        spinnerOptions.push({ value: "", text: ""});

        if (e.key === "Delete") { // delete key
            this.handleChange(e, {value: "", options: spinnerOptions});
        }
    }

    /**
     * The inactive selector is a dummy selector which
     * just shows the currently selected value.
     */
    renderInactiveSelect() {
        const selectedOptions = this.getSelectedOptions();
        const selectedOptionsValues = selectedOptions !=null? selectedOptions.map(x => x.value) : undefined;
        const selectOptions = this.props.optionsFunc(this.props.entities);
        const placeholder = this.props.placeholder;

        if (this.props.multipleSelect) {
            return (
                <Form.Select
                    multiple={true}
                    search={true}
                    selection={true}
                    value={selectedOptionsValues || []}
                    onBlur={this.handleBlur}
                    onChange={this.handleChange}
                    options={selectOptions}
                    label={this.props.label}
                    error={this.props.error}
                    disabled={this.props.disabled}
                    width={this.props.width}
                    className={this.props.className}
                    required={this.props.required}
                    onKeyDown={this.deleteSelection}
                    placeholder={placeholder}
                />
            );
        } else {
            return (
                <Form.Select
                    value={selectedOptions != null? selectedOptions[0].value : undefined}
                    search={true}
                    onFocus={this.handleFocus}
                    options={[{value: selectedOptions != null? selectedOptions[0].value: undefined, text: selectedOptions != null? selectedOptions[0].text : undefined}]}
                    label={this.props.label}
                    error={this.props.error}
                    disabled={this.props.disabled}
                    width={this.props.width}
                    className={this.props.className}
                    required={this.props.required}
                    onKeyDown={this.deleteSelection}
                    placeholder={placeholder}
                />
            ); 
        }
    }

    renderActiveSelect() {
        const selectedOptions = this.getSelectedOptions();
        const selectedOptionsValues = selectedOptions != null? selectedOptions.map(x => x.value) : undefined;
        const selectOptions = this.props.optionsFunc(this.props.entities);
        const placeholder = this.props.placeholder;

        if(this.props.withBlankOption) {
            selectOptions.unshift({ value: "", text: "" });
        }

        if (this.props.multipleSelect) {
            return (
            <Form.Select
                multiple={true}
                search={true}
                selection={true}
                value={selectedOptionsValues || []}
                onBlur={this.handleBlur}
                onChange={this.handleChange}
                options={selectOptions}
                label={this.props.label}
                error={this.props.error}
                disabled={this.props.disabled}
                width={this.props.width}
                className={this.props.className}
                required={this.props.required}
                onKeyDown={this.deleteSelection}
                placeholder={placeholder}
            />
            );
        } else {
           return (
            <Form.Select
                value={selectedOptions != null? selectedOptions[0].value : undefined}
                search={true}
                onBlur={this.handleBlur}
                onChange={this.handleChange}
                options={selectOptions}
                label={this.props.label}
                error={this.props.error}
                disabled={this.props.disabled}
                width={this.props.width}
                className={this.props.className}
                required={this.props.required}
                onKeyDown={this.deleteSelection}
                placeholder={placeholder}
            />
            );
        }
    }

    render() {
        if(!this.state.isActive) {
            return this.renderInactiveSelect();
        }

        return this.renderActiveSelect();
    }
}