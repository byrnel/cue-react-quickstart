import * as moment from "moment";

export default class DateFactory {
    static getCurrentDateAsUtc() {
        return moment.utc().add(moment().utcOffset(), "m").startOf("day");
    }
}