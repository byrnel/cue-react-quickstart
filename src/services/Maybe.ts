export function maybe<T>(val: T): T | null {
    if (val) { return val; }
    return null;
}