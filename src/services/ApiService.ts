import * as Api from "../constants/Api";
import * as moment from "moment";

const defaultFetchOptions: RequestInit = {
    credentials: "include" // Allow cookies to be sent and received cross-origin
};

const defaultXhrOptions = (xhr: XMLHttpRequest) => {
    xhr.withCredentials = true; // Allow cookies to be sent and received cross-origin
};

class DeserializedResponse extends Response {
    /**
     * Deserialised json response data (or undefined)
     */
    data?: any;
}

class ApiService {
    xhr? = new XMLHttpRequest();

    get(url: string, data?: object) {
        // Maybe add data to the url string as encoded parameters
        if(data != null) {
            url += "?" + this.parameterize(data);
        }

        return fetch(Api.baseUrl + url, defaultFetchOptions)
            .then(this.maybeParseJson);
    }

    post(url: string, data?: object) {
        return this.requestThunk("post", url, data);
    }

    /**
     * Like post, but passes the Response data back to the client and injects
     * a data property which may include deserialised json.
     */
    postIncludeResponse(url: string, data?: object): Promise<DeserializedResponse> {
        return fetch(Api.baseUrl + url, {
            ...defaultFetchOptions,
            method: "post",
            headers: new Headers({
                "Content-Type": "application/json"
            }),
            body: data != null ? JSON.stringify(data) : null
        })
        .then(response => {
            return this.maybeParseJson(response)
                .then((responseData: Response): DeserializedResponse => {
                    // Can't spread for some reason
                    (<any>response).data = responseData;
                    return response;
                });
        });
    }

    put(url: string, data?: object) {
        return this.requestThunk("put", url, data);
    }

    delete(url: string, data?: object) {
        return this.requestThunk("delete", url, data);
    }

    requestThunk(method: string, url: string, data?: object) {
        return fetch(Api.baseUrl + url, {
            ...defaultFetchOptions,
            method: method,
            headers: new Headers({
                "Content-Type": "application/json"
            }),
            body: data != null ? JSON.stringify(data) : null
        })
        .then(this.maybeParseJson);
    }

    abort() {
        if(this.xhr != null) {
            this.xhr.abort();
            this.xhr = undefined;
        }
    }
    
    /**
     * Creates a collection of promise constructors for staging a collection of
     * files. This differs from staging files individually because the progress
     * reports will be an overall progress report, not just a report for one
     * file.
     * 
     * Since this returns a collection of promise constructors, the consumer
     * must invoke each one to begin the staging process. It is expected that
     * each promise is run sequentially.
     */
    stageFiles(files: IStageableFile[], onProgress: (progress: number, file: IStageableFile) => void) {
        return this.runTasksSequentially(this.stageFilesThunk(files, onProgress));
    }
    
    stageFilesThunk(files: IStageableFile[], onProgress: (progress: number, file: IStageableFile) => void) {
        if(files.length === 0) {
            return [];
        }
        
        const bytesTotal = files.map(f => f.file.size).reduce((prev, current) => prev + current);
        let bytesLoaded = 0;
        const shimOnProgress = (_: number, loaded?: number, total?: number, loadedDelta?: number, file?: IStageableFile) => {
            if(loadedDelta != null) {
                bytesLoaded += loadedDelta;
            }

            // This should always be true.
            if(file != null) {
                onProgress(bytesLoaded * 100 / bytesTotal, file);
            }
        };

        return files.map(f => () => {
            return this.stageFile(f, shimOnProgress);
        });
    }

    /**
     * Submits arbitrary files for temporary storage on the server. This
     * allows us to separate file uploads from JSON uploads. The client
     * should provide a "client guid" for each file. The server will send
     * back the client guid plus a "server guid" for each file. The client
     * can then submit the server guid in subsequent requests so the data
     * can be accessed on the server. The concept of client and server
     * guids prevents scenarios where a client with a poor RNG generates
     * clashing guids which would require changing and re-attempting the
     * submission with new guids.
     *
     * This is useful because: Combining files uploads with JSON uploads
     * is messy. It's also wasteful if the user has to upload 30MB raster
     * files every time something in the JSON fails validation.
     *
     * We could just submit the JSON data first then follow with the file
     * data when the JSON is valid. However this way we don't have to litter
     * our backend domain models with guids that allow us to hook up files
     * with their parent entities after those parent entities have been
     * persisted.
     */
    stageFile(file: IStageableFile, onProgress: (progress: number, loaded?: number, total?: number, loadedDelta?: number, file?: IStageableFile) => void) {
        // See "futch": https://github.com/github/fetch/issues/89
        const url = Api.baseUrl + "/files/stage";

        // See: https://developer.mozilla.org/en-US/docs/Web/API/FormData/Using_FormData_Objects
        const formData = new FormData();
        formData.append(`files[0].file`, file.file);
        formData.append(`files[0].clientGuid`, file.guid);

        this.xhr = new XMLHttpRequest();

        return new Promise((resolve: any, reject: any) => {
            if(this.xhr == null) {
                reject();
                return;
            }

            defaultXhrOptions(this.xhr);
            this.xhr.onload = (e: any) => {
                this.xhr = undefined;
                if(e.target.responseText == null) {
                    reject();
                } else {
                    // Map server guids back to client guids. Response will
                    // be a dictionary of client guid -> server guid.
                    var response =  JSON.parse(e.target.responseText);
                    file.serverGuid = response[file.guid];
                    resolve(file);
                }
            };
            this.xhr.onerror = (ev: ErrorEvent) => {
                this.xhr = undefined;
                reject(ev);
            };
            let loadedPrev = 0;
            if(this.xhr.upload != null) { // This conditional is only relevant under test; should never be null at runtime.
                this.xhr.upload.onprogress = (e: ProgressEvent) => {
                    if(e.lengthComputable) {
                        const loadedDelta = e.loaded - loadedPrev;
                        loadedPrev = e.loaded;
                        onProgress(e.loaded * 100/ e.total, e.loaded, e.total, loadedDelta, file);
                    }
                };
            }
            this.xhr.open("post", url);
            this.xhr.send(formData);
        });
    }

    private runTasksSequentially(tasks: any) {
        var result = Promise.resolve();
        tasks.forEach((task: any) => {
            result = result.then(() => task());
        });
        return result;
    }

    /**
     * Deserialize the response as JSON if it has a JSON content-type, otherwise return null.
     * @param response 
     */
    private maybeParseJson(response: Response) {
        const contentType = response.headers.get("content-type");
        return (contentType != null && contentType.indexOf("application/json") > -1)
            ? response.json()
            : Promise.resolve(undefined);
    }

    /**
     * Converts an object to a URL query string like ?thing=blah&anotherThing=Yep
     * See: http://stackoverflow.com/questions/22678346/convert-javascript-object-to-url-parameters
     */ 
    private parameterize(data: object) {
        return Object
            .keys(data)
            .map(k => encodeURIComponent(k) + "=" + this.encode(data[k]))
            .join("&");
    }

    private encode(value: any) {
        if (value == null) {
            return "";
        } else if (value instanceof moment) {
            return encodeURIComponent(value.format());
        } else {
            return encodeURIComponent(value);
        }
    }
    
}

export default new ApiService();