import { CrossStorageClient } from "cross-storage";
import * as A from "./A";
import { ApiService, Logger } from "./index";

class CacheService {
    getCacheData(key:string) {
        return this.getUrl()
            .then((url:string) => this.createCacheClient(url))
            .then((storage:CrossStorageClient) => {
                return storage.onConnect()
                .then(() => {
                    storage.get(key);
                })
                .then((res) => {
                    return JSON.parse(A.string(res)); })
                .catch((err) => {
                    Logger.error(err);
                });
            });
    }

    setCacheData(key:string, data:any) {
        return this.getUrl()
            .then((url:string) => this.createCacheClient(url))
            .then((storage:CrossStorageClient) => {
                storage.onConnect()
                .then(() => {
                    storage.set(key, JSON.stringify(data));
                });
            });
    }

    clearCacheData(key:string) {
        return this.getUrl()
            .then((url:string) => this.createCacheClient(url))
            .then((storage:CrossStorageClient) => {
                storage.onConnect().then(() => {
                    return storage.del(key); });
            });
    }

    createCacheClient(url:string) {
        return new CrossStorageClient(
            url,
            {
                timeout: 2000,
                frameId: "modeld-frame"
            }
        );
    }

    getUrl() {
        return ApiService
            .get("/series/lookupdata")
            .then((data:any) => {
                return data.informdUri;
            });
    }
}

export default new CacheService();