import { ApiService } from "./";

class Logger {
    error = (error: any) => {
        this.log(error.message, "error", error.stack);
        console.error(error);
    }
    private log = (message: string, level: string, stacktrace: string) => {
        ApiService.post("/log", { 
            "Message": message, 
            "Level": level,
            "StackTrace": stacktrace
        });
    }
}

export default new Logger();