import ApiService from "./ApiService";

// Simplest possible implementation of IStageableFile
class TestFile implements IStageableFile {
    file: File;
    guid: string;
    serverGuid?: string;

    constructor(guid: string) {
        this.guid = guid;
        this.file = new File(["Blah"], "testfile");
    }
}

it("can submit files for storing (staging) temporarily on the server", (done) => {
    // Configure our mocked "server" to return these server guids for
    // these client guids
    mockFileStagingResponse({
        clientguid1: "serverguid1",
        clientguid2: "serverguid2"
    });

    // Set up some test files that we want to stage on the server
    let file1 = new TestFile("clientguid1");
    let file2 = new TestFile("clientguid2");

    // Attempt to stage the files on the server and confirm the files
    // were updates with the server guids we configured earlier.
    const onProgressStub = (progress: number, file: IStageableFile) => { ""; }; 
    ApiService.stageFiles([file1, file2], onProgressStub)
        .then(() => {
            // Assert files were given server guids
            expect(file1.serverGuid).toBe("serverguid1");
            expect(file2.serverGuid).toBe("serverguid2");
            done();
        })
        .catch((error: any) => {
            // This catch statement will eat any exceptions thrown
            // from the "expect" calls above, so we just log out
            // to the console so we have some idea why this test
            // failed.
            // console.log(error);
            fail();
            done();
        });
});

// Mock XMLHttpRequest to immediately return the specified data.
function mockFileStagingResponse(responseData: object) {
    const responseText = JSON.stringify(responseData);
    const open = jest.fn();
    const send = jest.fn().mockImplementation(<any>function(this: any, formData: any) {
        this.onload({
            target: {
                responseText: responseText
            }
        });
    });
    const xmlHttpRequestMocked = function() {
        return {
            open,
            send
        };
    };

    (<any>window).XMLHttpRequest = jest.fn().mockImplementation(xmlHttpRequestMocked);
}
