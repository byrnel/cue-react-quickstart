
CN for Semantic UI : https://cdnjs.com/libraries/semantic-ui

https://levelup.gitconnected.com/typescript-and-react-using-create-react-app-a-step-by-step-guide-to-setting-up-your-first-app-6deda70843a4
npm install -g create-react-app
create-react-app sali-cue-template --scripts-version=react-scripts-ts


npm install react-router
npm install react-router-dom --save
npm install @types/react-router-dom --save
npm install semantic-ui-react

npm install semantic-ui-react --save
npm install ramda --save
npm install @types/ramda --save

npm install mobx --save
npm install mobx-react --save
npm install moment
npm install react-datepicker --save
npm install @types/react-datepicker --save

npm install enzyme --save
npm install @types/enzyme --save
npm install enzyme-adapter-react-15 --save
npm install @types/enzyme-adapter-react-15 --save
npm install leaflet --save
npm install @types/leaflet --save
npm install react-leaflet --save
npm install @types/react-leaflet --save
npm install esri-leaflet --save
npm install @types/esri-leaflet --save
npm install react-toastify --save
npm install cross-storage --save
npm install @types/cross-storage --save


npm audit

# Run  npm update postcss-filter-plugins --depth 4  to resolve 1 vulnerability

npm WARN ajv-keywords@3.2.0 requires a peer of ajv@^6.0.0 but none is installed. You must install peer dependencies yourself.
npm WARN enzyme-adapter-react-15@1.0.5 requires a peer of react@^15.5.0 but none is installed. You must install peer dependencies yourself.
npm WARN enzyme-adapter-react-15@1.0.5 requires a peer of react-dom@^15.5.0 but none is installed. You must install peer dependencies yourself.
npm WARN enzyme-adapter-react-15@1.0.5 requires a peer of react-test-renderer@^15.5.0 but none is installed. You must install peer dependencies yourself.
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.4 (node_modules\fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.4: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"})


npm update postcss-filter-plugins --depth 4


npm WARN ajv-keywords@3.2.0 requires a peer of ajv@^6.0.0 but none is installed. You must install peer dependencies yourself.
npm WARN enzyme-adapter-react-15@1.0.5 requires a peer of react@^15.5.0 but none is installed. You must install peer dependencies yourself.
npm WARN enzyme-adapter-react-15@1.0.5 requires a peer of react-dom@^15.5.0 but none is installed. You must install peer dependencies yourself.
npm WARN enzyme-adapter-react-15@1.0.5 requires a peer of react-test-renderer@^15.5.0 but none is installed. You must install peer dependencies yourself.
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.4 (node_modules\fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.4: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"})

+ postcss-filter-plugins@2.0.3
removed 2 packages and updated 1 package in 12.309s
[+] no known vulnerabilities found [31252 packages audited]


C:/Data/SALI/Sali-CUE-Template/src/common/Toasts.ts
(1,17): Module '"C:/Data/SALI/Sali-CUE-Template/node_modules/react-toastify/index"' has no exported member 'style'.